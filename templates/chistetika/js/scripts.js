'use strict';

function getCatalogueLang(){ 
    var lang_id = parseInt($('.js_catalogue_lang_id').attr('data-id')); 
     
    return lang_id; 
}

var jsonLangConsts = JSON.parse($('.js_lang_consts').attr('data-json'));
var lang_id = getCatalogueLang(); 


var fancyOpt = {
    focus: false,
    slideShow: false,
    fullScreen: false,
    thumbs: false,
    closeBtn: true,
    touch: false,
    closeClickOutside: true,
    buttons: false,
    animationDuration : 600,
    smallBtn: true,
    btnTpl : {
        smallBtn: '<button data-fancybox-close class="fancybox-close-small">&#xe915;</button>'
    },
    afterShow : function( instance, current ) {
        var $currentSlider = $(current.$content[0]).find('.modal-gallery-slider');

        if($(instance.$lastFocus).hasClass('zoom-ico') || $(instance.$lastFocus).hasClass('js-gallery-link') || instance.slides[0].contentType == "html"){
            initModalGallery($currentSlider);
            $('.js-equal-height').matchHeight();
        }
    },
	ajax: {
		cache: false,
		settings: {
			cache: false,
		}
	}
};

var isTouchDev = 'ontouchstart' in document.documentElement;
var windWidth = viewport().width;
var windHeight = viewport().height;
var windTop = 0;


function players(func, args) {
    var iframes = document.getElementsByTagName('iframe');
    for (var i=0; i<iframes.length; ++i) {
        if (iframes[i]) {
            var src = iframes[i].getAttribute('src');
            if (src) {
                if (src.indexOf('youtube.com/embed') != -1) {
                    iframes[i].contentWindow.postMessage(JSON.stringify({'event': 'command','func': func,'args': args || []}), "*");
                }
            }
        }
    }
}


function recalcSticky() {
	$('.page-up-btn-col-scroll').trigger("sticky_kit:recalc");
}



function setIconItem(currentSlider){
	if(!currentSlider.find('.slick-current .item-tile-slider__link').attr('data-json')){
		return false;
	}
	var json_item = JSON.parse(currentSlider.find('.slick-current .item-tile-slider__link').attr('data-json'));
	var is_hit = parseInt(json_item.is_hit);
	var is_new = parseInt(json_item.is_new);
	var is_new_colour = parseInt(json_item.is_new_colour);
	var is_sale = parseInt(json_item.is_sale);
	var is_call = parseInt(json_item.is_call);
	
	//Zoom and Favorite
	var id = parseInt(json_item.id);
	var class_id = parseInt(json_item.class_id);
	var url_zoom = json_item.url_zoom;
	var is_favorite = json_item.is_favorite;
	var is_zoom = json_item.is_zoom;
	
	currentSlider.parents('.catalog-list__item').find('.zoom-ico.js-modal-link').attr('data-src', url_zoom).hide();
	if(is_zoom){
		currentSlider.parents('.catalog-list__item').find('.zoom-ico.js-modal-link').show();
	}
	
	currentSlider.parents('.catalog-list__item').find('.zoom-ico.js_open_modal_no_cache').attr('data-src', url_zoom).hide();
	if(is_zoom){
		currentSlider.parents('.catalog-list__item').find('.zoom-ico.js_open_modal_no_cache').show();
	}
	
	currentSlider.parents('.catalog-list__item').find('.js-add-favorites').removeClass('active').attr('data-class-id', class_id).attr('data-id', id);
	if(is_favorite){
		currentSlider.parents('.catalog-list__item').find('.js-add-favorites').addClass('active');
	}
	
	var arrLabelsActive = [];
	
	currentSlider.parents('.catalog-list__item').find('.item-tile__label').hide().addClass('item-tile__label_hidden').removeClass('item-tile__label');
	if(is_hit){
		currentSlider.parents('.catalog-list__item').find('.item-tile__label_hit').removeClass('item-tile__label_hidden').addClass('item-tile__label').show();
		arrLabelsActive.push(currentSlider.parents('.catalog-list__item').find('.item-tile__label_hit'));
	}
	if(is_new){
		currentSlider.parents('.catalog-list__item').find('.item-tile__label_new').removeClass('item-tile__label_hidden').addClass('item-tile__label').show();
		arrLabelsActive.push(currentSlider.parents('.catalog-list__item').find('.item-tile__label_new'));
	}
	if(is_new_colour){
		currentSlider.parents('.catalog-list__item').find('.item-tile__label_new_colour').removeClass('item-tile__label_hidden').addClass('item-tile__label').show();
		arrLabelsActive.push(currentSlider.parents('.catalog-list__item').find('.item-tile__label_new_colour'));
	}
	if(is_sale){
		currentSlider.parents('.catalog-list__item').find('.item-tile__label_sale').removeClass('item-tile__label_hidden').addClass('item-tile__label').show();
		arrLabelsActive.push(currentSlider.parents('.catalog-list__item').find('.item-tile__label_sale'));
	}
	if(is_call){
		currentSlider.parents('.catalog-list__item').find('.item-tile__label_call').removeClass('item-tile__label_hidden').addClass('item-tile__label').show();
		arrLabelsActive.push(currentSlider.parents('.catalog-list__item').find('.item-tile__label_call'));
	}
	if(currentSlider.find('.slick-current').hasClass('item-tile-slider__link_no-pic')){
		currentSlider.find('.slick-current .zoom-ico').hide();
		currentSlider.find('.slick-current .zoom-ico').show();
	}else{
	}
	
	if(arrLabelsActive.length > 0){
		var arrLabelsTop = [0,27,54,81,108];
		for(var i = 0; i < arrLabelsActive.length; i++){
			var label_obj = arrLabelsActive[i];
			label_obj.css({'top':arrLabelsTop[i]+'px'});
		}
	}
	
}


function imageZoom($currentImg) {

	$currentImg.removeData('elevateZoom');
	$.removeData($currentImg, 'elevateZoom');
	$currentImg.removeData('zoomImage');

	$('.zoomContainer').remove();

	if(windWidth>1023 && $currentImg.hasClass('js-zoom-img')) {

		var zoomContainerWidth = $('.card-row__right').outerWidth() - 15;
		if(windWidth<1366){
			zoomContainerWidth += 15;
		}

		var zoomContainerHeight = $('.card-detail-pic').height();

		var zoomOptions = {
			cursor: "pointer",
			borderSize: 0,
			lensOpacity: 1,
			responsive: true,
			lensBorder: 2,
			lensColour: 'transparent',
			zoomWindowHeight: zoomContainerHeight,
			zoomWindowWidth: zoomContainerWidth,
			zoomWindowFadeIn: 500,
			zoomWindowFadeOut: 500,
			lensFadeIn: 500,
			lensFadeOut: 500
		};

		$currentImg.elevateZoom(zoomOptions);
	}
}




function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}


if(lang_id != 1){ 
    $.extend($.validator.messages, { 
            required: "This field is required.", 
            remote: "Please fix this field.", 
            email: "Please enter a valid email address.", 
            url: "Please enter a valid URL.", 
            date: "Please enter a valid date.", 
            dateISO: "Please enter a valid date (ISO).", 
            number: "Please enter a valid number.", 
            digits: "Please enter only digits.", 
            creditcard: "Please enter a valid credit card number.", 
            equalTo: "Please enter the same value again.", 
            accept: "Please enter a value with a valid extension.", 
            maxlength: $.validator.format("Please enter no more than {0} characters."), 
            minlength: $.validator.format("Please enter at least {0} characters."), 
            rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."), 
            range: $.validator.format("Please enter a value between {0} and {1}."), 
            max: $.validator.format("Please enter a value less than or equal to {0}."), 
            min: $.validator.format("Please enter a value greater than or equal to {0}.") 
    }); 
}else{ 
    $.extend($.validator.messages, { 
            required: "Заполните поле", 
            remote: "Пожалуйста, введите правильное значение.", 
            email: "Неверный формат", 
            url: "Пожалуйста, введите корректный URL.", 
            date: "Пожалуйста, введите корректную дату.", 
            dateISO: "Пожалуйста, введите корректную дату в формате ISO.", 
            number: "Пожалуйста, введите число.", 
            digits: "Пожалуйста, вводите только цифры.", 
            creditcard: "Пожалуйста, введите правильный номер кредитной карты.", 
            equalTo: "Пожалуйста, введите такое же значение ещё раз.", 
            extension: "Пожалуйста, выберите файл с правильным расширением.", 
            maxlength: $.validator.format("Пожалуйста, введите не больше {0} символов."), 
            minlength: $.validator.format("Пожалуйста, введите не меньше {0} символов."), 
            rangelength: $.validator.format("Пожалуйста, введите значение длиной от {0} до {1} символов."), 
            range: $.validator.format("Пожалуйста, введите число от {0} до {1}."), 
            max: $.validator.format("Пожалуйста, введите число, меньшее или равное {0}."), 
            min: $.validator.format("Пожалуйста, введите число, большее или равное {0}.") 
    }); 
} 

function checkInView($animation_elements, coeff) {
    var window_height = $(window).height();
    var window_top_position = $(window).scrollTop();
    var window_bottom_position = (window_top_position + window_height);

    $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top + element_height / coeff;

        if ((element_top_position <= window_bottom_position)) {
            $element.addClass('animate-in');
        }
    });
}


$.fn.isInViewport = function() {
    var elementTop = $(this).offset().top - 100;
    var viewportBottom = $(window).scrollTop() + $(window).height()/2;

    return elementTop < viewportBottom;
};

var isTabActive = true;

window.onfocus = function () {
    isTabActive = true;
};

window.onblur = function () {
    isTabActive = false;
};


function parallaxSlider(options) {
    var $ = jQuery
    this.setting = {
        displaymode: {
            type: 'auto',
            pause: 8000,
            stoponclick: false,
            cycles: 2,
            pauseonmouseover: true
        },
        activeslideclass: 'active',
        orientation: 'h',
        persist: true,
        slideduration: 600,
        autoplaydelay: 0
    };

    jQuery.extend(this.setting, options)
    this.setting.displaymode.pause += 400 + this.setting.slideduration
    this.curslide = 0
    this.curstep = 0
    this.curzindex = 0
    this.animation_isrunning = false
    this.posprop = "left"
    options = null
    var slideshow = this, setting = this.setting;

    slideshow.$wrapperdiv = $('#' + setting.wrapperid);

    slideshow.init($)

}

parallaxSlider.prototype = {

    slide: function (nextslide, dir) {

        var navdir = /(left|top)/.test(dir) ? 'back' : 'forth'
        if (this.curslide == nextslide)
            return
        var slider = this, setting = this.setting
        var createobj = parallaxSlider.routines.createobj
        var nextslide_initialpos = setting.dimensions[(dir == "right" || dir == "left") ? 0 : 1] * ((dir == "right" || dir == "down") ? -1 : 1)
        var curslide_finalpos = -nextslide_initialpos
        var posprop = this.posprop
        if (this.animation_isrunning != null)
            this.animation_isrunning = true //indicate animation is running
        var $nextslide = this.$imageslides.eq(nextslide).show().css(createobj([posprop, nextslide_initialpos], ['zIndex', 1]))
        var $curslide = this.$imageslides.eq(this.curslide)

        $nextslide //show upcoming slide: [.75,.03,0,.97] is easing "function". See http://cubic-bezier.com
            .stop().velocity(createobj([posprop, 0]), setting.slideduration, [.75, .03, 0, .97], function () {
            var $this = jQuery(this)
            $this.addClass(setting.activeslideclass)
            slider.animation_isrunning = false
        });

        $curslide //unshow current slide
            .css({zIndex: 0})
            .removeClass(setting.activeslideclass)
            .stop().velocity(createobj([posprop, dir == 'left' || dir == 'up' ? -300 : 300]), setting.slideduration, "ease-in-out", function () {
            var $this = jQuery(this)
            $this.hide()
        }); //hide outgoing slide


        this.curslide = nextslide
    },

    navigate: function (keyword) { //keyword: "back" or "forth"

        var slideshow = this, setting = this.setting
        clearTimeout(this.rotatetimer)
        if (!setting.displaymode.stoponclick && setting.displaymode.type != "manual") { //if slider should continue auto rotating after nav buttons are clicked on
            this.curstep = (keyword == "back") ? this.curstep - 1 : this.curstep + 1 //move curstep counter explicitly back or forth depending on direction of slide
            this.rotatetimer = setTimeout(function () {
                slideshow.rotate()
            }, setting.displaymode.pause)
        }
        var dir = (keyword == "back") ? (setting.orientation == "h" ? "right" : "down") : (setting.orientation == "h" ? "left" : "up")
        var targetslide = (keyword == "back") ? this.curslide - 1 : this.curslide + 1
        targetslide = (targetslide < 0) ? this.$imageslides.length - 1 : (targetslide > this.$imageslides.length - 1) ? 0 : targetslide //wrap around
        if (this.animation_isrunning == false)
            this.slide(targetslide, dir)
    },

    rotate: function () {
        var slideshow = this, setting = this.setting
        if (this.ismouseover || !isTabActive) { //pause slideshow onmouseover
            this.rotatetimer = setTimeout(function () {
                slideshow.rotate()
            }, setting.displaymode.pause)
            return
        }
        var nextslide = (this.curslide < this.$imageslides.length - 1) ? this.curslide + 1 : 0
        this.slide(nextslide, this.posprop) //go to next slide, either to the left or upwards depending on setting.orientation setting
        if (setting.displaymode.cycles == 0 || this.curstep < this.maxsteps - 1) {
            this.rotatetimer = setTimeout(function () {
                slideshow.rotate()
            }, setting.displaymode.pause)
            this.curstep++
        }
    },

    init: function ($) {

        var slideshow = this, setting = this.setting
        setting.dimensions = [this.$wrapperdiv.width(), this.$wrapperdiv.height()]

        this.curslide = 0 //make sure curslide index is within bounds

        this.$imageslides = this.$wrapperdiv.find('.category-slider__item').hide();

        this.$imageslides.eq(this.curslide).show().css(parallaxSlider.routines.createobj([this.posprop, 0])) //set current slide's CSS position (either "left" or "top") to 0
            .addClass(setting.activeslideclass)
            .stop()

        var orientation = setting.orientation;

        var $controls = $('<span class="nav-button" data-dir="forth"></span><span class="nav-button nav-button_next" data-dir="back"></span>')
            .click(function () {
                var keyword = this.getAttribute('data-dir')
                setting.curslide = (keyword == "right") ? (setting.curslide == setting.content.length - 1 ? 0 : setting.curslide + 1)
                    : (setting.curslide == 0 ? setting.content.length - 1 : setting.curslide - 1)
                slideshow.navigate(keyword)
            });
        this.$controls = $controls.appendTo(this.$wrapperdiv)

        if (setting.displaymode.type == "auto") { //auto slide mode?
            setting.displaymode.pause += setting.slideduration
            this.maxsteps = setting.displaymode.cycles * this.$imageslides.length
            if (setting.displaymode.pauseonmouseover) {
                this.$wrapperdiv.mouseenter(function () {
                    slideshow.ismouseover = true
                })
                this.$wrapperdiv.mouseleave(function () {
                    slideshow.ismouseover = false
                })
            }
            this.rotatetimer = setTimeout(function () {
                slideshow.rotate()
            }, setting.displaymode.pause + setting.autoplaydelay)
        }

        var swipeOptions = { // swipe object variables
            triggerOnTouchEnd: true,
            triggerOnTouchLeave: true,
            fallbackToMouseEvents: true, // enable mouse emulation of swipe navigation in non touch devices?
            allowPageScroll: "vertical",
            excludedElements: []
        }

        swipeOptions.swipeStatus = function (event, phase, direction, distance) {
            if (phase == 'start' && event.target.tagName == 'A') { // cancel A action when finger makes contact with element
                evtparent.onclick = function () {
                    return false
                }
            }
            if (phase == 'cancel' && event.target.tagName == 'A') { // if swipe action canceled (so no proper swipe), enable A action
                evtparent.onclick = function () {
                    return true
                }
            }
            if (phase == 'end') {
                var navkeyword = /(right)|(down)/i.test(direction) ? 'back' : 'forth'
                if ((setting.orientation == 'h' && /(left)|(right)/i.test(direction)) || (setting.orientation == 'v' && /(up)|(down)/i.test(direction)))
                    slideshow.navigate(navkeyword)
            }
        }

        if (this.$wrapperdiv.swipe) {
            this.$wrapperdiv.swipe(swipeOptions)
        }
    }

}

parallaxSlider.routines={

    createobj:function(){
        var obj={}
        for (var i=0; i<arguments.length; i++){
            obj[arguments[i][0]]=arguments[i][1]
        }
        return obj
    }
}


function numberFormat(str) {
    return str.replace(/(\s)+/g, '').replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ');
}

function initModalGallery($galleryObj) {

    $galleryObj.on('init', function(event, slick){
        $.fn.matchHeight._update();
    });

    $galleryObj.slick({
        lazyLoad: 'ondemand',
        dots: false,
        infinite: true,
        arrows: true,
        speed: 600,
        slidesToShow: 1,
        fade: false,
        prevArrow: '<button type="button" class="slick-prev"><span></span></button>',
        nextArrow: '<button type="button" class="slick-next"><span></span></button>'
    });
	
	/*
	var active_index = $galleryObj.attr('data-active-index');
	$galleryObj.slick('slickGoTo', active_index, true);
	*/
	
	$galleryObj.on('afterChange', function(event, slick, currentSlide){
		var json_str = $galleryObj.find('.slick-current .item-tile-slider__link').attr('data-json');
		if(!json_str){
			json_str = $galleryObj.find('.slick-current .modal-gallery-slider__item').attr('data-json');
		}
		if(json_str){
			var json_item = JSON.parse(json_str);
			$galleryObj.parents('.modal-gallery').find('.modal-gallery__title').text(json_item.name);
			$galleryObj.parents('.modal-gallery').find('.js-add-favorites').attr('data-id',json_item.id);
			$galleryObj.parents('.modal-gallery').find('.js-add-favorites').attr('data-class-id',json_item.class_id);
			
			$galleryObj.parents('.modal-gallery').find('.js-add-favorites').removeClass('active');
			if(json_item.is_favorite){
				$galleryObj.parents('.modal-gallery').find('.js-add-favorites').addClass('active');
			}
		}
	});
}

function formsInit(){
    $('.js-validate-form').each(function () {
        $(this).validate();
    });

    var telInput = document.getElementsByClassName('js-phone-input');
    var telMaskOptions = {
        lazy: true,
        placeholderChar: ' ',
        mask: '+0 000 000 00 00 00 00 00'
    };

    if(telInput.length> 0) {
        for (var i = 0; i < telInput.length; i++) {
            new IMask(telInput[i], telMaskOptions);
        }
    }

    var dateInput = document.getElementsByClassName('js-date-input');

    var dateMaskOptions = {
        mask: Date,
        min: new Date(1900, 0, 1),
        max: new Date()
    };

    if(dateInput.length> 0) {
        for (var i = 0; i < dateInput.length; i++) {
            new IMask(dateInput[i], dateMaskOptions);
        }
    }

    var numberInputMask = document.getElementsByClassName('js_number_mask');
    var numberMaskOptions = {
        lazy: true,
        placeholderChar: ' ',
        mask: Number,

    };

    if(numberInputMask.length> 0) {
        for (var i = 0; i < numberInputMask.length; i++) {
            var min_input = $(numberInputMask[i]).attr('min');
            var max_input = $(numberInputMask[i]).attr('max');
            numberMaskOptions.min = min_input;
            numberMaskOptions.max = max_input;
            new IMask(numberInputMask[i], numberMaskOptions);
        }
    }

    $('select, input[type="number"], input[type="file"]').styler({
        selectSearch: true
    });

    $('input[type="number"]').each(function () {
        numberInput($(this))
    });
    $('body').on('change', 'input[type="number"]', function () {
        numberInput($(this))
    });
}

function numberInput(input) {
    var $currentInput = input;
    var currentVal = parseInt($currentInput.val());
    var currentMin = parseInt($currentInput.attr('min'));
    var currentMax = parseInt($currentInput.attr('max'));
    $currentInput.closest('.jq-number').find('.jq-number__spin').removeClass('disabled');
    $currentInput.addClass('empty');
    if(currentVal>0) {
        $currentInput.removeClass('empty');
    }
    if(currentVal>currentMax) {
        currentVal = currentMax;
        $currentInput.val(currentMax);
    }
    if(currentVal<currentMin) {
        currentVal = currentMin;
        $currentInput.val(currentMin);
    }

    if(currentVal == currentMin){
        $currentInput.closest('.jq-number').find('.minus').addClass('disabled');
    }
    if(currentVal == currentMax){
        $currentInput.closest('.jq-number').find('.plus').addClass('disabled');
    }
}

function count404() {
    var siteLeft = $('.page-width').css('margin-left');

    $('.error-page').css('padding-left', siteLeft);
}




function initItemsSmallScroll(){
	$('.items-small-scroll').each(function () {
        var $currentSlider = $(this);

        $currentSlider.slick({
            dots: false,
            infinite: false,
            arrows: true,
            speed: 600,
            slidesToShow: 4,
            fade: false,
            prevArrow: '<button type="button" class="slick-prev"><span></span></button>',
            nextArrow: '<button type="button" class="slick-next"><span></span></button>',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        arrows: false,
                        slidesToShow: 2
                    }
                }
            ]
        });
    });
}

function initSmallGalleryScroll(){
	$('.small-gallery-scroll').each(function () {
        var $currentSlider = $(this);

        $currentSlider.slick({
            dots: false,
            infinite: false,
            arrows: true,
            speed: 600,
            slidesToShow: 3,
            fade: false,
            prevArrow: '<button type="button" class="slick-prev"><span></span></button>',
            nextArrow: '<button type="button" class="slick-next"><span></span></button>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        arrows: false,
                        slidesToShow: 1
                    }
                }
            ]
        });
    });
}

function setShowButtonMoreArticle(){
	var total = $('.card-detail-thumbs .card-detail-thumbs__item').length;
	var window_width = viewport().width;
	var is_hide = false;
	if(window_width > 1023){
		if(total <= 10){
			is_hide = true;
		}
	}else if(window_width > 576){
		if(total <= 6){
			is_hide = true;
		}
	}else{
		if(total <= 3){
			is_hide = true;
		}
	}
	
	if(is_hide){
		$('.js-toggle-thumbs').hide();
	}
}

function countCardHeight() {
	if(windWidth>1023) {
		var $leftCols = $('.card-row__left');
		var $rightCols = $('.card-row__right');

		$($rightCols[1]).css('min-height', 0);
		var rightHeight = 0;

		$rightCols.each(function () {
			rightHeight += $(this).height();
		});

		var leftHeight = 0;

		$leftCols.each(function () {
			leftHeight += $(this).height();
		});

		if (leftHeight > rightHeight) {
			var left2Height = leftHeight - $($rightCols[0]).height() - 60;
			//$($rightCols[1]).css('min-height', left2Height);
		}
	}
}

function setFavoriteSlider(){
	$('.favorites-scroll').each(function () {
        var $currentSlider = $(this);

        $currentSlider.slick({
            dots: false,
            infinite: false,
            arrows: true,
            speed: 600,
            slidesToShow: 8,
            fade: false,
            prevArrow: '<button type="button" class="slick-arrow slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-next"></button>',
            responsive: [
                {
                    breakpoint: 1366,
                    settings: {
                        slidesToShow: 6
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]
        });

    });
}


$(document).ready(function(){
    if(isTouchDev){
        $('body').addClass('touch-dev');
    }
    else{
        $('body').addClass('no-touch-dev');
    }

    windWidth = viewport().width;
    windHeight = viewport().height;
    windTop = $(window).scrollTop();


    $(window).on('resize', function () {
        windWidth = viewport().width;
        windHeight = viewport().height;
		setShowButtonMoreArticle();
    });


    $('.js-equal-height').matchHeight();

    var $animationElements = $('.js-check-in-view');

    $(window).on('scroll resize', function(){
        if(windWidth<1000){
            checkInView($animationElements, 10);
        }
        else{
            checkInView($animationElements, 2);
        }
    });


    $(window).on('load', function(){
        setTimeout(function () {
            if(windWidth<1000){
                checkInView($animationElements, 10);
            }
            else{
                checkInView($animationElements, 2);
            }
        }, 600);
    });

    $('.js-city-select .city-select__current').on('click', function (e) {
        $(this).next('.city-select__drop').toggleClass('opened');
    });

    $(document).on("click", function (event) {
        if(!$(event.target).closest(".js-city-select, .header-search, .js-toggle-search, .js-checkbox-select, .js-show-share, .favorites-popup, .js-toggle-favorites").length) {
            $('.city-select__drop, .header-search, .js-checkbox-select, .share, .favorites-popup').removeClass('opened');
        }
        event.stopPropagation();
    });

    $('.js-toggle-search').on('click', function (e) {
        $('.header-search').toggleClass('opened');
        if($('.header-search').hasClass('opened')){
            $('.header-search__input').focus();
        }
        e.preventDefault();
    });

    $('.js-main-slider').slick({
        dots: true,
        infinite: true,
        arrows: false,
        speed: 600,
        slidesToShow: 1,
        fade: true,
        autoplay: true,
        autoplaySpeed: 8000
    });

    if(!isTouchDev) {
        $('.main-benefits__item:first-child').addClass('hover');

        $('.main-benefits__item').hover(function () {
            $('.main-benefits__item').removeClass('hover');
            $(this).addClass('hover')
        });
    }


    $('.js-scroll-top').on('click', function (e) {
        $("html,body").stop().animate({scrollTop: 0}, 800);
    });


    $('.js-category-hover').hover(function () {
        $('.js-category-hover').addClass('no-hover');
        $(this).addClass('is-hover').removeClass('no-hover');
    }, function () {
        $('.js-category-hover').removeClass('is-hover no-hover');
    });

    var galleryOpenedFl = false;

    $('.js-category-hover').on('click', function (e) {
        if(!galleryOpenedFl && !e.target.closest('.btn-type2')){
            $('.js-category-hover').addClass('closed');
            $(this).addClass('opened').removeClass('closed');
			$('body').addClass('gallery-opened');
            galleryOpenedFl = true;
        }
    });

    $('.js-close-category-gallery').on('click', function () {
        $('.js-category-hover').removeClass('closed opened');
		$('body').removeClass('gallery-opened');
        setTimeout(function () {
            galleryOpenedFl = false;
        }, 400);
    });


    if($('.js-category-slider').length>0) {
        var firstParallaxSlider = new parallaxSlider({
            wrapperid: 'slider0'
        });

        var secondParallaxSlider = new parallaxSlider({
            wrapperid: 'slider1',
            autoplaydelay: 2000
        });
    }


    $('.js-scroll-down').on('click', function (e) {
        $("html,body").stop().animate({scrollTop: windHeight}, 800);
    });


    var $fixHeader = $('.js-fix-header');

    var menuOpenFl = false;

    var winTopMax = $("body").height()*10;
    var windTopLast = 0;

    $('.js-toggle-menu').on('click', function () {
        if(!$('html').hasClass('menu-opened') && !$('html').hasClass('menu-opened-top')) {
            windTop = $(window).scrollTop();

            if(windTop == 0 || ($fixHeader.hasClass('fixed') && !$fixHeader.hasClass('out'))){
                $('html').addClass('menu-opened-top').css('top', -windTop);
            }
            else{
                $('html').addClass('menu-opened').css('top', -windTop);
            }
            $('.page-left-col').slideDown();
            menuOpenFl = true;
        }
        else{
            $('.page-left-col').slideUp();
            $('html').removeClass('menu-opened menu-opened-top').css('top', 0);
            $('html, body').scrollTop(windTop);
            setTimeout(function (args) { menuOpenFl = false; }, 50);
        }
    });

    var difference = function (a, b) { return Math.abs(a - b); }

    $(window).on('load', function () {
        var windTop = $(window).scrollTop();

        winTopMax = $("body").height()*10;

        if(windWidth > 1023) {
            if (windTop > 50) {
                $fixHeader.addClass('fixed');
            }
            else {
                $fixHeader.removeClass('fixed');
            }
        }
    });


    $(window).on('scroll', function () {
        var windTop = $(window).scrollTop();

        if(!menuOpenFl) {
            if (windWidth > 1023) {
                if (windTop > 50) {
                    $fixHeader.addClass('fixed');
                }
                else {
                    $fixHeader.removeClass('fixed');
                }
            }
            else {
                if (windTop > 60) {
                    $fixHeader.addClass('fixed');
                }
                else {
                    $fixHeader.removeClass('fixed');
                }

                if(difference(windTopLast,windTop) > 300) {
                    if (windTop > winTopMax && windTop > 400) {
                        $fixHeader.addClass("out");
                    }
                    else {
                        $fixHeader.removeClass("out");
                    }
                    windTopLast = windTop;
                }
                if (windTop == 0) {
                    $fixHeader.removeClass("out");
                }

                winTopMax = windTop;
            }
        }
    });


    $('.main-promo-params').slick({
        responsive: [{
            breakpoint: 99999,
            settings: "unslick"
        },{
            breakpoint: 576,
            settings: {
                dots: false,
                arrows: false,
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true
            }
        }]
    });

    $('.js-toggle-footer-menu').on('click', function () {
        if(windWidth<576){
            $(this).next('.footer-menu').slideToggle();
            $(this).toggleClass('opened');
        }
    });


    $('.js-checkbox-select .checkbox-select__current').on('click', function () {
        var parentOpenFl = $(this).parent().hasClass('opened');
        $('.js-checkbox-select').removeClass('opened');
        if(!parentOpenFl){
            $(this).parent().addClass('opened');
        }
    });

    $('.js-toggle-filter').on('click', function () {
        $('.catalog-filter__form').slideToggle();
        $(this).toggleClass('opened');
        var hideText = $(this).attr('data-show');
        if($(this).hasClass('opened')){
            hideText = $(this).attr('data-hide');
        }
        $(this).text(hideText);
    });

    if(windWidth<768){
        $('.catalog-filter__form').hide();
        $('.js-toggle-filter').removeClass('opened').text(jsonLangConsts.TEXT_SHOW_ALL_FILTERS);
    }


    $('.js-item-tile-slider').each(function () {
        var $currentSlider = $(this);
		
		$currentSlider.on('init', function(event, slick){
			setIconItem($currentSlider);
        });

        $currentSlider.slick({
            lazyLoad: 'ondemand',
            dots: false,
            infinite: true,
            arrows: true,
            speed: 600,
            slidesToShow: 1,
            fade: false,
            prevArrow: '<button type="button" class="slick-prev"><span></span></button>',
            nextArrow: '<button type="button" class="slick-next"><span></span></button>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        arrows: false,
                        touchMove: false,
                        draggable: false,
                        swipe: false
                    }
                }
            ]
        });

        $currentSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            if(nextSlide != 0){
                $(this).closest('.item-tile').addClass('show-info');
            }
            else{
                $(this).closest('.item-tile').removeClass('show-info');
            }
        });

        $currentSlider.on('afterChange', function(event, slick, currentSlide){
            var json_item = JSON.parse($currentSlider.find('.slick-current .item-tile-slider__link').attr('data-json'));
			$currentSlider.parents('.catalog-list__item').find('.item-tile__name').text(json_item.name);
			setIconItem($currentSlider);
        });
    });


    $(document).on('click', '.js-modal-link', function () {
        if($('body').hasClass('fancybox-active')){
            return false;
        }
        if(menuOpenFl) {
            $('.page-left-col').slideUp();
            $('html').removeClass('menu-opened menu-opened-top').css('top', 0);
            $('html, body').scrollTop(windTop);
            setTimeout(function (args) {
                menuOpenFl = false;
            }, 50);
        }
    });

    $(".js-modal-link").fancybox(fancyOpt);



    var $zoomImages = $('.card-detail-scroll__item img');

    var $currentActiveZoomImg = $($zoomImages[0]);

	/*
    $('body').on({
        mouseenter: function(){
            $('.card-detail-pic').addClass('zoom-active');
        },
        mouseleave: function(){
            $('.card-detail-pic').removeClass('zoom-active');
        }
    }, '.zoomContainer');*/


    if($zoomImages.length>0){
        setTimeout(function () {
            imageZoom($($zoomImages[0]));
        }, 100);
    }

    $('.card-detail-scroll').on('afterChange', function(event, slick, currentSlide){
        imageZoom($($zoomImages[currentSlide]));
        $currentActiveZoomImg = $($zoomImages[currentSlide]);
    });

    $(window).on('resize', function () {
        if($zoomImages.length>0) {
            imageZoom($currentActiveZoomImg);
        }
    });

    $(window).on('load', function () {
        if($zoomImages.length>0) {
            setTimeout(function () {
                imageZoom($currentActiveZoomImg);
            }, 100);
        }
    });


    $('.card-detail-scroll').each(function () {
        var $currentSlider = $(this);

        $currentSlider.slick({
            lazyLoad: 'ondemand',
            dots: false,
            infinite: true,
            arrows: false,
            speed: 400,
            slidesToShow: 1,
            fade: true,
            zIndex: 10
        });

		/*
        $currentSlider.on('afterChange', function(event, slick, currentSlide){
            var $zoomItem = $currentSlider.closest('.card-detail-pic').find('.js-modal-link');
            var url = $zoomItem.attr('data-src');
            var newUrl = url.split('?')[0] + "?id=" + currentSlide;
            $zoomItem.attr('data-src', newUrl)
        });*/

        $('.card-detail-thumbs__link').on('click', function (e) {
            var clickId = $(this).parent().index();
            $('.card-detail-thumbs__item').removeClass('active');
            $(this).parent().addClass('active');
            $currentSlider.slick('slickGoTo', clickId);
        });

    });

    $(document).on('click', '.js-toggle-thumbs', function () {
        var $cardThumbs = $('.card-detail-thumbs__item');
		$(this).toggleClass('opened');
        var hideText = $(this).attr('data-show');
        if($(this).hasClass('opened')){
			countCardHeight();
            $cardThumbs.slideDown(countCardHeight);
			countCardHeight();
            hideText = $(this).attr('data-hide');
			Cookies.set('show_articles', 1);
        }
        else{
			Cookies.remove('show_articles');
            if(windWidth>1023){
                $cardThumbs.slice(10, $cardThumbs.length).slideUp(countCardHeight);
            }
            else{
                if(windWidth>767){
                    $cardThumbs.slice(6, $cardThumbs.length).slideUp(countCardHeight);
                }
                else{
                    if(windWidth>576){
                        $cardThumbs.slice(5, $cardThumbs.length).slideUp(countCardHeight);
                    }
                    else{
                        $cardThumbs.slice(3, $cardThumbs.length).slideUp(countCardHeight);
                    }
                }
            }
        }
        $(this).text(hideText);
    });
	
	setShowButtonMoreArticle();


    initItemsSmallScroll();


    $(document).on('click', '.js-cloth-more', function () {
       $(this).next('.card-cloth-info').slideToggle();
       $(this).toggleClass('opened');
    });


    $('body').on('click', '.js-tabs .tabs__item',function () {
        var currentTabId = $(this).index();
        var $currentTabs = $(this).parent().find('.tabs__item');
        var $nextPanes = $(this).parent().next('.tabs-panes').find('.tabs-panes__item');
        $nextPanes.removeClass('active');
        $currentTabs.removeClass('active');
        $(this).addClass('active');
        $($nextPanes[currentTabId]).addClass('active');

        countCardHeight();
    });

    $(document).on('click', '.card-technology__row', function () {
        $(this).next('.card-technology__hidden').slideToggle();
        $(this).parent().toggleClass('opened');
    });


    initSmallGalleryScroll();

    countCardHeight();

    $(window).on('laod resize', countCardHeight);

    formsInit();

    $('.order-list__header').on('click', function () {
        $(this).toggleClass('opened');
        $(this).next('.order-list__info').slideToggle();
    });


    $('.js-sticky-col').stick_in_parent({
        offset_top: 80
    });


    $('.media-gallery').each(function () {
        var $currentSlider = $(this);

        $currentSlider.on('init', function(event, slick){
            $.fn.matchHeight._update();
        });

        $currentSlider.slick({
            dots: false,
            infinite: true,
            arrows: true,
            speed: 600,
            slidesToShow: 1,
            fade: false,
            prevArrow: '<button type="button" class="slick-prev"><span></span></button>',
            nextArrow: '<button type="button" class="slick-next"><span></span></button>',
        });

    });


    $('body').on('click', '.js-care-tabs .care-tabs__item',function () {
        var currentTabId = $(this).index();
        var $currentTabs = $(this).parent().find('.care-tabs__item');
        var $nextPanes = $(this).parent().next('.care-panes').find('.care-panes__item');
        $nextPanes.removeClass('active');
        $currentTabs.removeClass('active');
        $(this).addClass('active');
        $($nextPanes[currentTabId]).addClass('active');
		
		var arrSpotIds = [];
		$('.care-panes .js-care-checkbox:eq('+currentTabId+') input:checkbox:checked').each(function(){
			arrSpotIds.push($(this).val());
		});
		
		$('.js-care-tabs .care-tabs__item:not(.active)').each(function(){
			var index = $(this).index();
			$('.care-panes .js-care-checkbox:eq('+index+') input:checkbox:checked').removeAttr('checked').prop('checked', false);
			$('.care-panes .js-care-checkbox:eq('+index+') .care-pics__item').removeClass('active');
		});
		
		if(arrSpotIds.length > 0){
			$('.care-cloth').slideDown();
			$('.care-not-selected').text(jsonLangConsts.TEXT_CHOOSE_CLOTH_TYPE);
		}else{
			$('.care-cloth').slideUp();
			$('.care-not-selected').text(jsonLangConsts.TEXT_SELECT_TYPE_SPOT);

			$('.care-not-selected').show();
			$('.care-result-cont').slideUp();
			$('.js-care-checkbox2 input').prop('checked', false);
		}
    });


    $('.js-anchor-link').on('click', function (e) {
        var target = $(this).attr('href');
        if($(target).length>0) {
            var targetPos = $(target).offset().top - 80;

            $("html,body").stop().animate({scrollTop: targetPos}, 800);
        }
        e.preventDefault();
    });

    if($('.innovation-list').length>0){

        var menuItems = $(".innovation-dots").find(".js-anchor-link"),
            scrollItems = menuItems.map(function () {
                var item = $($(this).attr("href"));
                if (item.length) {
                    return item;
                }
            });


        $(window).on('scroll', function(){
            var windTop = $(window).scrollTop();

            var lastItem = $('.dots-list__item:last a');

            var scrollBottom = $(window).scrollTop() + $(window).height();
            var docHeight = $(document).height();
            var cur = scrollItems.map(function () {
                if ($(this).offset().top < windTop + 81)
                    return this;
            });

            cur = cur[cur.length - 1];
            var id = cur && cur.length ? cur[0].id : "";

            menuItems.parent().removeClass("active");
            menuItems.filter('[href="#' + id + '"]').parent().addClass("active");
            if (docHeight - scrollBottom < 100) {
                menuItems.parent().removeClass("active");
                lastItem.parent().addClass("active");
            }

        });
    }

    count404();

    $(window).on('resize', count404);

    $('.page-text-top__more-btn').on('click', function () {
		var current_text = $(this).text();
		var attr_text = $(this).attr('data-text');
		$(this).text(attr_text);
		$(this).attr('data-text', current_text);
		
        $('.page-text-top__hidden').slideToggle();
		$(this).toggleClass('opened');
        //$('.page-text-top__more').slideUp();
    });


    $(document).on('click', '.js-show-share', function (e) {
       $(this).parents('.share').toggleClass('opened');
    });



    $(document).on("click", "a", function (e) {
        var linkClicked = $(this).attr('href');
		
        if(
			linkClicked != '#' && 
			!$(this).hasClass('js-modal-link') && 
			!$(this).hasClass('js-anchor-link') && 
			!$(this).hasClass('js-inline-video') && 
			!$(this).hasClass('js-catalog-load-more') && 
			!$(e.target).hasClass('js-remove-favorites') && 
			!$(e.target).hasClass('js-add-favorites') && 
			!$(this).hasClass('file-link') && 
			!$(this).hasClass('page-header__tel') && 
			!$(this).hasClass('page-footer__tel') && 
			!$(this).hasClass('page-footer__mail') && 
			!$(this).hasClass('js_tel') && 
			!$(this).hasClass('print-link') && 
			!$(this).hasClass('js_link_outer') && 
			!$(this).hasClass('js-filter-media') && 
			!$(this).hasClass('social-list__link') && 
			!$(this).hasClass('js_variant_item') && 
			!$(this).hasClass('js_item_autocomplete') && 
			!$(this).hasClass('js_bracing_ymaps_select_item') && 
			!$(this).hasClass('js_edit_address') && 
			!$(this).hasClass('js_link_no_clicked') &&
			!$(this).hasClass('js_show_company_list')
		) {

            $('body').removeClass('page-loaded');
            e.preventDefault();

            window.setTimeout(function () {
                window.location = linkClicked;
            }, 650);
        }
    });


    $('.page-up-btn-col-scroll').stick_in_parent();


    $(document).on('click', '.js-inline-video', function (e) {
        e.preventDefault();
        var frameSrc = $(this).attr('href');

        if(frameSrc.indexOf('?') == -1){
            frameSrc += '?autoplay=1&version=3&enablejsapi=1';
        }
        else{
            frameSrc += '&autoplay=1&version=3&enablejsapi=1';
        }
        $(this).hide();
        $(this).next('.video-iframe').show();
        $(this).next('.video-iframe').find('iframe').attr({'src': frameSrc});
    });



    setFavoriteSlider();


    $('.js-toggle-favorites').on('click', function (e) {
        $('.favorites-popup').toggleClass('opened');
        e.preventDefault();
    });

	$('.main-benefits__item').each(function () {
        if ($(this).isInViewport()) {
            $(this).addClass('in-view');
        } else {
            $(this).removeClass('in-view');
        }
    });

    $(window).on('resize scroll', function() {
        $('.main-benefits__item').each(function () {
            if ($(this).isInViewport()) {
                $(this).addClass('in-view');
            } else {
                $(this).removeClass('in-view');
            }
        })
    });

});
/*Doc ready end*/


$(window).on('load', function () {
    setTimeout(function () {
        $('body').addClass('page-loaded');
		
		$('.page-up-btn-col-scroll').trigger("sticky_kit:recalc");
    }, 200);

    setTimeout(function () {
        if(windWidth<1000){
            checkInView($('.js-check-in-view'), 10);
        }
        else{
            checkInView($('.js-check-in-view'), 2);
        }
    }, 600);
});