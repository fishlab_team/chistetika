var jsonLangConsts = JSON.parse($('.js_lang_consts').attr('data-json'));



/*! 
 * JavaScript Cookie v2.2.0 
 * https://github.com/js-cookie/js-cookie 
 * 
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack 
 * Released under the MIT license 
 */ 
;(function (factory) { 
    var registeredInModuleLoader; 
    if (typeof define === 'function' && define.amd) { 
        define(factory); 
        registeredInModuleLoader = true; 
    } 
    if (typeof exports === 'object') { 
        module.exports = factory(); 
        registeredInModuleLoader = true; 
    } 
    if (!registeredInModuleLoader) { 
        var OldCookies = window.Cookies; 
        var api = window.Cookies = factory(); 
        api.noConflict = function () { 
            window.Cookies = OldCookies; 
            return api; 
        }; 
    } 
}(function () { 
    function extend () { 
        var i = 0; 
        var result = {}; 
        for (; i < arguments.length; i++) { 
            var attributes = arguments[ i ]; 
            for (var key in attributes) { 
                result[key] = attributes[key]; 
            } 
        } 
        return result; 
    } 

    function init (converter) { 
        function api (key, value, attributes) { 
            if (typeof document === 'undefined') { 
                return; 
            } 

            // Write 

            if (arguments.length > 1) { 
                attributes = extend({ 
                    path: '/' 
                }, api.defaults, attributes); 

                if (typeof attributes.expires === 'number') { 
                    attributes.expires = new Date(new Date() * 1 + attributes.expires * 864e+5); 
                } 

                // We're using "expires" because "max-age" is not supported by IE 
                attributes.expires = attributes.expires ? attributes.expires.toUTCString() : ''; 

                try { 
                    var result = JSON.stringify(value); 
                    if (/^[\{\[]/.test(result)) { 
                        value = result; 
                    } 
                } catch (e) {} 

                value = converter.write ? 
                    converter.write(value, key) : 
                    encodeURIComponent(String(value)) 
                        .replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent); 

                key = encodeURIComponent(String(key)) 
                    .replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent) 
                    .replace(/[\(\)]/g, escape); 

                var stringifiedAttributes = ''; 
                for (var attributeName in attributes) { 
                    if (!attributes[attributeName]) { 
                        continue; 
                    } 
                    stringifiedAttributes += '; ' + attributeName; 
                    if (attributes[attributeName] === true) { 
                        continue; 
                    } 

                    // Considers RFC 6265 section 5.2: 
                    // ... 
                    // 3.  If the remaining unparsed-attributes contains a %x3B (";") 
                    //     character: 
                    // Consume the characters of the unparsed-attributes up to, 
                    // not including, the first %x3B (";") character. 
                    // ... 
                    stringifiedAttributes += '=' + attributes[attributeName].split(';')[0]; 
                } 

                return (document.cookie = key + '=' + value + stringifiedAttributes); 
            } 

            // Read 

            var jar = {}; 
            var decode = function (s) { 
                return s.replace(/(%[0-9A-Z]{2})+/g, decodeURIComponent); 
            }; 
            // To prevent the for loop in the first place assign an empty array 
            // in case there are no cookies at all. 
            var cookies = document.cookie ? document.cookie.split('; ') : []; 
            var i = 0; 

            for (; i < cookies.length; i++) { 
                var parts = cookies[i].split('='); 
                var cookie = parts.slice(1).join('='); 

                if (!this.json && cookie.charAt(0) === '"') { 
                    cookie = cookie.slice(1, -1); 
                } 

                try { 
                    var name = decode(parts[0]); 
                    cookie = (converter.read || converter)(cookie, name) || 
                        decode(cookie); 

                    if (this.json) { 
                        try { 
                            cookie = JSON.parse(cookie); 
                        } catch (e) {} 
                    } 

                    jar[name] = cookie; 

                    if (key === name) { 
                        break; 
                    } 
                } catch (e) {} 
            } 

            return key ? jar[key] : jar; 
        } 

        api.set = api; 
        api.get = function (key) { 
            return api.call(api, key); 
        }; 
        api.getJSON = function () { 
            return api.apply({ 
                json: true 
            }, arguments); 
        }; 
        api.remove = function (key, attributes) { 
            api(key, '', extend(attributes, { 
                expires: -1 
            })); 
        }; 

        api.defaults = {}; 

        api.withConverter = init; 

        return api; 
    } 

    return init(function () {}); 
}));

/*  
    Функция форматирование цены  
*/  
function php_number_format( number, decimals, dec_point, thousands_sep ) {    // Format a number with grouped thousands  
    //   
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)  
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)  
    // +     bugfix by: Michael White (http://crestidg.com)  

    var i, j, kw, kd, km;  

    // input sanitation & defaults  
    if( isNaN(decimals = Math.abs(decimals)) ){  
        decimals = 2;  
    }  
    if( dec_point == undefined ){  
        dec_point = ",";  
    }  
    if( thousands_sep == undefined ){  
        thousands_sep = ".";  
    }  

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";  

    if( (j = i.length) > 3 ){  
        j = j % 3;  
    } else{  
        j = 0;  
    }  

    km = (j ? i.substr(0, j) + thousands_sep : "");  
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);  
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");  
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");  


    return km + kw + kd;  
}  
/*  
    Получение формата цены  
*/  
function formatPrice(price, no_convert){  
    if(!price || price == undefined){  
        return 0;  
    }  
      
    var re = /\./gi; 
    var re2 = /\,/gi; 
     
    var currency_json = $('.js_currency_convert').attr('data-json'); 
    if(currency_json && !no_convert){ 
        var json = JSON.parse(currency_json); 
        price = price * (json[1] / json[3]); 
    } 
      
    if(re.test(price) || re2.test(price)){  
        price = php_number_format(price, 2, ',', ' ');  
    }else{  
        price = php_number_format(price, 0, '', ' ');  
    }  
      
    return price;  
}

function setValidateFormContact(){
	$('#form_send_app_contact').validate({  
        messages: { 
            'agreement': { 
                required: jsonLangConsts["TEXT_TO_ACCEPT_CONDITION"], 
            } 
        }, 
        submitHandler: function(form) {  
            delay(function(){  
                var form_obj = $(form);  
                var action = form_obj.attr('action');  
                var params = form_obj.serialize();  
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){  
                        openFancyboxModal(data.header, data.text, data.status);
						if(data.status){
							form_obj.trigger('reset');
						}
						setTimeout(function() { 
							$.fancybox.close(); 
						}, 7000);
                    }
                });  
            },250);  
            return false;  
        },  
    });
}

function setValidateFormReviewCompany(){
	$('#send_form_review_company').validate({  
        messages: { 
            'agreement': { 
                required: jsonLangConsts["TEXT_TO_ACCEPT_CONDITION"], 
            } 
        }, 
        submitHandler: function(form) {  
            delay(function(){  
                var form_obj = $(form);  
                var action = form_obj.attr('action');
				
				//Проставляем кол-во рейтинга
				form_obj.find('.rating-vote-line').each(function(){
					var class_name = $(this).attr('data-class-input');
					var total = $(this).find('.rating-vote__input.active').length;
					form_obj.find('.'+class_name).val(total);
				});
				
                var params = form_obj.serialize();  
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){  
                        openFancyboxModal(data.header, data.text, data.status);
						if(data.status){
							form_obj.trigger('reset');
						}
						setTimeout(function() { 
							$.fancybox.close(); 
						}, 7000);
                    }
                });  
            },250);  
            return false;  
        },  
    });
}



var delay = (function(){
var timer = 0;
	return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	};
})();

function generateFilter(clear){
	$('.catalog-wrap').addClass('pre-loading');
	delay(function(){
		var arrFilter = {};
		
		if(!clear){
			//Checkbox-ы
			$('.checkbox-list.checkbox-list_line .checkbox-list__item input:checkbox').each(function(){
				if($(this).is(':checked')){
					arrFilter[$(this).attr('data-param-name')] = $(this).val();
				}
			});
			//Цена
			if($('.js-range-slider__input_min').length && $('.js-range-slider__input_max').length){
				var set_price_min = parseFloat($('.js-range-slider__input_min').val().replace(/\s/gi,''));
				if(isNaN(set_price_min)){
					set_price_min = 0;
				}
				var set_price_max = parseFloat($('.js-range-slider__input_max').val().replace(/\s/gi,''));
				if(isNaN(set_price_max)){
					set_price_max = 0;
				}
				var default_price_min = parseFloat($('.js_price_range_slider').attr('data-min'));
				if(isNaN(default_price_min)){
					default_price_min = 0;
				}
				var default_price_max = parseFloat($('.js_price_range_slider').attr('data-max'));
				if(isNaN(default_price_max)){
					default_price_max = 0;
				}
				if(set_price_min !== default_price_min){
					arrFilter['min_price'] = set_price_min;
				}
				if(set_price_max !== default_price_max){
					arrFilter['max_price'] = set_price_max;
				}
			}
			//Группировка
			var is_group_cloth = $('.js_group_cloth').is(':checked');
			if(is_group_cloth){
				arrFilter['group_cloth'] = 1;
			}
			//Количество выводимых объектов
			var recnum = parseInt($('select.js_select_recnum').val());
			if(recnum !== 12 && !isNaN(recnum)){
				arrFilter['recNum'] = recnum;
			}
			//Цвет
			var arrActiveColors = [];
			$('.js_filter_colors input:checkbox').each(function(){
				if($(this).is(':checked')){
					arrActiveColors.push($(this).val());
				}
			});
			if(arrActiveColors.length > 0){
				arrFilter['colors'] = arrActiveColors.join(',');
			}
			//Фильтры выпадающие списки с чекбоксами
			var arrFiltersSelectCheckbox = {};
			var arrFiltersSelectCheckboxForTextSelect = {};
			$('.catalog-filter__selects input:checkbox').each(function(){
				var param_name = $(this).attr('data-param-name');
				var value = $(this).val();
				if($(this).is(':checked')){
					if(!arrFiltersSelectCheckbox[param_name]){
						arrFiltersSelectCheckbox[param_name] = [];
					}
					if(!arrFiltersSelectCheckboxForTextSelect[param_name]){
						arrFiltersSelectCheckboxForTextSelect[param_name] = [];
					}
					arrFiltersSelectCheckbox[param_name].push(value);
					arrFiltersSelectCheckboxForTextSelect[param_name].push($.trim($(this).siblings('.checkbox__text').text()));
				}
			});
			//Ставим в выпадающем списке выбранные данные
			$('.catalog-filter__selects input:checkbox').each(function(){
				var param_name = $(this).attr('data-param-name');
				var arrDataText = arrFiltersSelectCheckboxForTextSelect[param_name];
				if(!arrDataText){
					$(this).parents('.js-checkbox-select').find('.checkbox-select__current').text($(this).parents('.js-checkbox-select').find('.checkbox-select__current').attr('data-text-default'));
				}else{
					$(this).parents('.js-checkbox-select').find('.checkbox-select__current').text(arrDataText.join(','));
				}
			});
			
			var size_filter_select_checkbox = Object.keys(arrFiltersSelectCheckbox).length;
			if(size_filter_select_checkbox){
				for(p_name in arrFiltersSelectCheckbox){
					arrFilter[p_name] = arrFiltersSelectCheckbox[p_name].join(',');
				}
			}
			//Поиск по артикулу
			var search_article = $.trim($('.js_search_article').val());
			if(search_article){
				arrFilter['search_article'] = search_article;
			}
			
		}else{
			clearFilter();
		}
		
		applicationFilter(arrFilter);
	}, 250);
}

function clearFilter(){
	//Checkbox-ы
	$('.checkbox-list.checkbox-list_line .checkbox-list__item input:checkbox').each(function(){
		if($(this).is(':checked')){
			$(this).removeAttr('checked').prop('checked',false).trigger('refresh');
		}
	});
	//Цена
	var default_price_min = parseFloat($('.js_price_range_slider').attr('data-min'));
	if(isNaN(default_price_min)){
		default_price_min = 0;
	}
	var default_price_max = parseFloat($('.js_price_range_slider').attr('data-max'));
	if(isNaN(default_price_max)){
		default_price_max = 0;
	}
	$('.js-range-slider__input_min').val(default_price_min);
	$('.js-range-slider__input_max').val(default_price_max);
	var current_id = $('.js-range-slider__input_min').attr('data-id');
	var filterSliders = document.querySelectorAll('.number-range');
	if(filterSliders && filterSliders[current_id]){
		if(filterSliders[current_id].noUiSlider){
			filterSliders[current_id].noUiSlider.set([default_price_min, default_price_max]);
		}
	}
	//Группировка
	if($('.js_group_cloth').is(':checked')){
		$('.js_group_cloth').removeAttr('checked').prop('checked',false).trigger('refresh');
	}
	//Количество выводимых объектов
	$('select.js_select_recnum').val($('select.js_select_recnum option:first').val()).trigger('refresh');
	//Цвет
	$('.js_filter_colors input:checkbox').each(function(){
		$(this).removeAttr('checked').prop('checked',false).trigger('refresh');
	});
	//Фильтры выпадающие списки с чекбоксами
	$('.catalog-filter__selects input:checkbox').each(function(){
		$(this).removeAttr('checked').prop('checked',false).trigger('refresh');
	});
	//Поиск по артикулу
	$('.js_search_article').val('');
}

function applicationFilter(data_filter){
	var button_more_obj = $('.js-catalog-load-more');
	
	var size_filter = Object.keys(data_filter).length;
	if(size_filter){
		var arrParams = [];
		var params = '';
		for(filter_key in data_filter){
			arrParams.push(filter_key+'='+data_filter[filter_key]);
		}
		params = arrParams.join('&');
		history.pushState(null, null, "?"+params);
	}else{
		history.pushState(null, null, location.pathname);
	}
	
	$.ajax({
		'url': location.pathname+'?no_check_easy_and_cloth=1',
		'type': 'GET',
		'dataType':'HTML',
		'data': data_filter,
		'success': function(data){
			$('.catalog-list').html($(data).find('.catalog-list').html());
			
			if($(data).find('.catalog-sort').css('display') === 'none'){
				$('.catalog-sort').hide();
				$('.catalog-empty').show();
			}else{
				$('.catalog-sort').show();
				$('.catalog-empty').hide();
			}
			$('.catalog-list').css('padding-top', $(data).find('.catalog-list').css('padding-top'));
			
			var href = $(data).find('.js-catalog-load-more').attr('href');
			var text = $(data).find('.js-catalog-load-more').text();
			if(button_more_obj.length <= 0){
				if($('.page-nav .page-nav-list').length <= 0){
					$('.catalog-list').after($(data).find('.page-nav')[0]);
				}
				$('.catalog-list').after($(data).find('.js-catalog-load-more')[0]);
			}
			if(href){
				button_more_obj.attr('href', href);
				button_more_obj.text(text);
			}else{
				button_more_obj.remove();
				$('.page-nav').remove();
			}
			
			if($(data).find('.page-nav').length > 0){
				$('.page-nav').html($(data).find('.page-nav').html());
			}
			
			$('.catalog-filter-result').html($(data).find('.catalog-filter-result').html());
			
			//Меняем данные в шапке левой колонки, чтобы при переключении с русского на английский и обратно, был новый URL
			$('.lang-select').html($(data).find('.lang-select').html());
			
			catalogLoad();
		}  
	});
}

function openFancyboxModal(header,text,status){
	if(!status && !header){
		header = jsonLangConsts['TEXT_ERROR'];
	}
	
	if(status && !header){
		header = 'OK';
	}
	
	var modal_obj = $('#success-modal');
	modal_obj.find('.js_text').hide();
	
	if(text){
		modal_obj.find('.js_text').show().html(text);
	}
	modal_obj.find('.js_header').text(header);
	
	$('.js_open_modal').trigger('click');
}

function setDataBeforeFilterBracing(data){
	$('.js_filter_content').html($(data).find('.js_filter_content').html());
	//$('.js_content_company').hide();
	//$('.js-restoration-show-map').trigger('click');
	
	$('.js_bracing_empty').hide();
	$('.js_filter_content').show();
	if($(data).find('.restoration-list__item').length <= 0){
		$('.js_bracing_empty').show();
		$('.js_filter_content').hide();
	}
	
	//Меняем данные в шапке левой колонки, чтобы при переключении с русского на английский и обратно, был новый URL
	$('.lang-select').html($(data).find('.lang-select').html());
	
	if(viewport().width > 768){
		$('.contacts-map-caption').hide();
	}
}

function filterBracing(){
	var arrParams = [];
	$('.checkbox-list input:checkbox').each(function(){
		if($(this).is(':checked')){
			arrParams.push($(this).attr('data-param-name')+'='+$(this).val());
		}
	});
	
	var url = location.pathname;
	if(arrParams.length > 0){
		url += '?'+arrParams.join('&');
		history.pushState(null, null, "?"+arrParams.join('&'));
	}else{
		history.pushState(null, null, location.pathname);
	}
	
	var search_value = $.trim($('.js_search_map_bracing').val());
	var data_params = {};
	if(search_value){
		data_params[search_value] = search_value;
	}
	var active_block = $('.filter-type-btn.active').attr('data-id');
	data_params['active_block'] = active_block;
	
	var by_list_obj = $('.js-restoration-show-list');
	var no_ids = by_list_obj.attr('data-no-ids');
	var ids = by_list_obj.attr('data-ids');
	if(!no_ids && ids){
		ids = ids.split(',');
		data_params['ids'] = ids;
	}
	var country_id = $('select.js_change_country').find('option:selected').val();
	data_params['country_id'] = country_id;
	
	$.ajax({
		'url': url,
		'type': 'GET',
		'data': data_params,
		'dataType':'HTML',
		'success': function(data){
			setDataBeforeFilterBracing(data);
			var search_address = $.trim($('.js_search_map_bracing').val());
			if(search_address){
				var country_name = $.trim($('select.js_change_country').find('option:selected').text());
				var search_value_map = country_name+', '+search_address;
				$('#restoration-map').attr('data-city', search_value_map);
			}else{
				var current_city = $.trim($('.city-select__current').text());
				$('#restoration-map').attr('data-city', current_city);
			}
			var current_zoom = restorationMap.getZoom();
			$('#restoration-map').attr('data-zoom', current_zoom);
			restorationMapInit();
		}  
	});
}

function updateCart(){
	$.ajax({  
		'url': location.pathname+location.search,
		'type': 'POST',
		'dataType':'html',
		'success': function(html){
			$('.js_header_cart').html($(html).find('.js_header_cart').html());
			$('.js_wrap_cart').html($(html).find('.js_wrap_cart').html());
			if($(html).find('.js_header_cart .js_item_row').length <= 0){
				$('.js_header_cart').addClass('no_cart_items');
			}else{
				$('.js_header_cart').removeClass('no_cart_items');
			}
			formsInit();
		}
	});
}

function setAutoCompleteSearchArticlesReviewBracing(){
	var $searchResults = $('.js_articles_list');
	if($searchResults.length > 0){
		$('.js_auto_search_articles').autocomplete({
			appendTo: ".js_articles_list",
			delay:500,
			minLength:2,
			focus: function (event, ui)
			{
				var currentValue = ui.item.value.replace(/<{1}[^<>]{1,}>{1}/g," ");
				$('.js_auto_search_articles').val(currentValue);
				return false;
			},
			select: function (event, ui)
			{
				var currentValue = ui.item.value.replace(/<{1}[^<>]{1,}>{1}/g," ");
				$('.js_auto_search_articles').val(currentValue);
				$searchResults.removeClass('show');
				return false;
			},
			source: function (request, response) {
				var url = $(this.element).attr('data-url');
				$.ajax({
					url: url,
					data: {query:request.term},
					dataType:'JSON',
					success: function (data) {
						var size = Object.keys(data.items).length;
						$searchResults.addClass('show');
						if (size > 0) {
							$searchResults.removeClass('no_result');
							$searchResults.find('.empty-message').hide();

							response($.map(data.items, function (item) {
								return {
									value: item.name,
									link: item.link
								};
							}));
						}
						else {
							$searchResults.addClass('no_result');
							$searchResults.find('.empty-message').show();
						}
					}
				})
			}
		}).data("ui-autocomplete")._renderItem = function (ul, item) {

			var inner_html = '<a href="' + item.link + '" onclick="event.preventDefault(); return false;" class="js_item_autocomplete">' + item.value + '</a>';

			return $("<li></li>")
				.data("item.autocomplete", item)
				.append(inner_html)
				.appendTo(ul);
		};
	}
}

function setImageCardHeight(){
	var image_card = $('.card-detail-pic');
	if(image_card.length > 0){
		var height_image = image_card.outerHeight();
		image_card.css({'minHeight':height_image+'px'});
	}
}

function setSimilarItemsHeight(){
	var similar_block = $('.js_similar_cloth_item');
	if(similar_block.length > 0){
		var height_block = similar_block.outerHeight();
		similar_block.css({'minHeight':height_block+'px'});
	}
}

function setGalleryItemsHeight(){
	var gallery_block = $('.js_gallery_for_item');
	if(gallery_block.length > 0){
		var height_block = gallery_block.outerHeight();
		gallery_block.css({'minHeight':height_block+'px'});
	}
}

$(document).ready(function(){
	$(document).on('change', '.checkbox-list.checkbox-list_line', function(){
		generateFilter();
	});
	
	$(document).on('change', 'select.js_select_recnum', function(){
		generateFilter();
	});
	
	$(document).on('click', '.js-catalog-view', function(e){
		generateFilter();
	});
	
	$(document).on('change', '.js_group_cloth', function(){
		generateFilter();
	});
	
	$(document).on('change', '.js_filter_colors input:checkbox', function(){
		generateFilter();
	});
	
	$(document).on('change', '.catalog-filter__selects input:checkbox', function(){
		generateFilter();
	});
	
	$(document).on('click', '.js_clear_filter', function(e){
		e.preventDefault();
		generateFilter(true);
	});
	
	$(document).on('keyup', '.js_search_article', function(e){
		var par = $(this);
		var value = $.trim(par.val());
		if(value.length >= 2 || value.length <= 0){
			delay(function(){
				generateFilter();
			}, 500);
		}
	});
	
	$(document).on('click', '.js_remove_active_tag', function(e){
		e.preventDefault();
		var param = $(this).attr('data-param');
		var type = $(this).attr('data-type');
		var value = $(this).attr('data-value');
		
		if(type === 'checkbox'){
			$('.checkbox-list input:checkbox, .catalog-sort input:checkbox').each(function(){
				var get_param = $(this).attr('data-param-name');
				var get_value = $(this).val();
				if(param == get_param && value == get_value){
					$(this).removeAttr('checked').prop('checked',false).trigger('refresh');
				}
			});
		}
		if(type === 'colors'){
			$('.js_filter_colors input:checkbox').each(function(){
				var get_value = $(this).val();
				if(value == get_value){
					$(this).removeAttr('checked').prop('checked',false).trigger('refresh');
				}
			});
		}
		if(type === 'select_checkbox'){
			var class_name = $(this).attr('data-class-name');
			if(class_name){
				$('.'+class_name+' input:checkbox').each(function(){
					var get_param = $(this).attr('data-param-name');
					var get_value = $(this).val();
					if(param == get_param && value == get_value){
						$(this).removeAttr('checked').prop('checked',false).trigger('refresh');
					}
				});
			}
		}
		if(type === 'input_one_class'){
			var class_name = $(this).attr('data-class-name');
			$('.'+class_name).val('');
		}
		
		$(this).remove();
		generateFilter();
	});
	
	var cookie_city_id = parseInt(Cookies.get('city_id'));
	if(!cookie_city_id){
		ymaps.ready(function(){
			var geolocation = ymaps.geolocation.get({
				provider: 'yandex',
				timeout: 2000
			}).then(function (result) {
				var city_name = $.trim(result.geoObjects.get(0).properties.get('name'));
				//Дубль кода
				var url_check = $('.js_city_check_url').attr('data-url');
				$.ajax({
					'url': url_check,
					'type': 'POST',
					'dataType':'JSON',
					'data': {city:city_name},
					'success': function(data){
						if(data.status){
							$('.city-select__current').text(data.city);
							$('.city-select-list__item.city-select-list__item_active').text(data.city);
							$('.city-select-list__item.city-select-list__item_active').attr('data-id', data.city_id);
							Cookies.set('city_id', data.city_id);
							
							$('.page-header__tel').attr('href', 'tel:+'+data.phone_clear).text(data.phone);
						}
					}  
				});
				//end - Дубль кода
			});
		});
	}
	
	$('.js_change_city_header').on('keyup', function(e){
		var par = $(this);
		delay(function(){
			var value = $.trim(par.val().toLowerCase());
			$('.city-select-list .city-select-list__item:not(.js_no_remove_li)').each(function(){
				var city_name = $(this).text().toLowerCase();
				if(city_name.indexOf(value) !== -1){
					$(this).show();
				}else{
					$(this).hide();
				}
			});
			
			if($('.city-select-list .city-select-list__item:not(:hidden)').length <= 0){
				$('.js_city_not_found').show();
			}else{
				$('.js_city_not_found').hide();
			}
			
			if(value === ''){
				$('.js_city_not_found').hide();
				$('.city-select-list .city-select-list__item:not(.js_no_remove_li)').show();
			}
		}, 250);
		
		/*
		var url = par.attr('data-url');
		delay(function(){
			var value = $.trim(par.val());
			if(value && value.length >= 3){
				$.ajax({
					'url': url,
					'type': 'POST',
					'dataType':'JSON',
					'data': {city:value},
					'success': function(data){
						$('.js_city_min_length').hide();
						$('.js_city_not_found').hide();
						if(data.html){
							$('.city-select-list .city-select-list__item:not(.js_no_remove_li)').remove();
							$('.city-select-list').append(data.html);
						}else{
							$('.js_city_not_found').show();
							$('.city-select-list .city-select-list__item:not(.js_no_remove_li)').hide();
						}
					}  
				});
			}else{
				$('.js_city_not_found').hide();
				if(value === ''){
					$('.js_city_min_length').hide();
					$('.city-select-list .city-select-list__item:not(.js_no_remove_li)').show();
				}else{
					$('.js_city_min_length').show();
				}
			}
		}, 500);
		*/
	});
	
	$(document).on('click', '.city-select-list__item', function(e){
		e.preventDefault();
		
		if($(this).hasClass('js_no_remove_li')){
			return false;
		}
		
		var city_id = parseInt($(this).attr('data-id'));
		var city_name = $.trim($(this).text());
		Cookies.set('city_id', city_id);
		$('.city-select__current').text(city_name);
		$('.city-select-list__item').removeClass('city-select-list__item_active');
		$(this).addClass('city-select-list__item_active');
		$('.city-select__current').trigger('click');
		$('.js_change_city_header').val('').trigger('keyup');
		
		//Дубль кода
		/*
		var url_check = $('.js_city_check_url').attr('data-url');
		$.ajax({
			'url': url_check,
			'type': 'POST',
			'dataType':'JSON',
			'data': {city:city_name},
			'success': function(data){
				if(data.status){
					$('.page-header__tel').attr('href', 'tel:+'+data.phone_clear).text(data.phone);
				}
			}  
		});
		*/
		//end - Дубль кода
		
		var phone = $(this).attr('data-phone');
		var phone_clear = $(this).attr('data-phone-clear');
		$('.page-header__tel').attr('href', 'tel:+'+phone_clear).text(phone);
		
		//Если находимся в разделах контакты, меняем город, если такой есть
		var select_contact_city_obj = $('select.js_city_contact');
		if(select_contact_city_obj.length > 0){
			select_contact_city_obj.find('option').each(function(){
				var get_city_id = parseInt($(this).val());
				if(get_city_id === city_id){
					select_contact_city_obj.removeAttr('selected').prop('selected',false);
					$(this).attr('selected', 'selected').prop('selected', true).trigger('change');
				}
			});
		}
	});
	
	setValidateFormContact();
	setValidateFormReviewCompany();
	
	$('select.js_city_contact').on('change', function(){
		var city_id = $(this).val();
		$.ajax({
			'url': location.pathname+location.search,
			'type': 'GET',
			'dataType':'HTML',
			'data': {set_city_id:city_id, isNaked:1},
			'success': function(data){
				$('.js_list_contact_info').html($(data).find('.js_list_contact_info').html());
				$('#form_send_app_contact').html($(data).find('#form_send_app_contact').html());
				$('.contacts-map-wrap').html($(data).find('.contacts-map-wrap').html());
				
				formsInit();
				setValidateFormContact();
				ymaps.ready(contactsMapInit);
			}  
		});
	});
	
	$(document).on('click', '.js_open_print', function(e){
		e.preventDefault();
		window.print();
	});
	
	$('.js_filter_checkbox_bracing').on('change', function(){
		filterBracing();
	});
	
	var arrCountriesZoom = {
		1:4,
		3:7,
		4:6
	};
	$('select.js_change_country').on('change', function(){
		var country_id = $(this).val();
		var country_name = $.trim($(this).find('option:selected').text());
		var myGeocoder = ymaps.geocode(country_name);
		myGeocoder.then(
			function (res){
				restorationMap.setCenter(res.geoObjects.get(0).geometry.getCoordinates());
				restorationMap.setZoom(arrCountriesZoom[country_id]);
			}
				
		);
	});
	
	/*
	$('.js_search_map_bracing').on('keyup', function(e){
		var par = $(this);
		delay(function(){
			var value = par.val();
			if(value.length >= 3){
				var country_name = $.trim($('select.js_change_country').find('option:selected').text());
				var search_value = country_name+', '+value;
				var myGeocoder = ymaps.geocode(search_value, {json:true});
				myGeocoder.then(
					function (res){
						console.log(res);
						//restorationMap.setCenter(res.geoObjects.get(0).geometry.getCoordinates());
						//restorationMap.setZoom(13);
					}
				);
			}
		}, 250);
	});
	*/
	
	
	
	if($('.js_search_ymaps').length > 0){
		$('.js_search_map_bracing').autocomplete({
			appendTo: ".js_search_ymaps",
			delay:500,
			minLength:2,
			focus: function (event, ui)
			{
				var currentValue = ui.item.value.replace(/<{1}[^<>]{1,}>{1}/g," ");
				$('.js_search_map_bracing').val(currentValue);
				return false;
			},
			change: function (event, ui)
			{
				//var value = $.trim($('.js_search_map_bracing').val());
				//if(!value || value.length < 2){
                    $('.js_search_ymaps').removeClass('show');
				//}
			},
			select: function (event, ui)
			{
				var currentValue = ui.item.value.replace(/<{1}[^<>]{1,}>{1}/g," ");
				$('.js_search_map_bracing').val(currentValue);
				$('.js_search_ymaps').removeClass('show');
				
				var country_name = $.trim($('select.js_change_country').find('option:selected').text());
				var search_value = country_name+', '+currentValue;
				var myGeocoder = ymaps.geocode(search_value);
				myGeocoder.then(
					function (res){
						restorationMap.setCenter(res.geoObjects.get(0).geometry.getCoordinates());
						restorationMap.setZoom(13);
					}
				);
				
				return false;
			},
			source: function (request, response) {
				
				var country_name = $.trim($('select.js_change_country').find('option:selected').text());
				var search_value = country_name+', '+request.term;
				var myGeocoder = ymaps.geocode(search_value, {json:true});
				myGeocoder.then(
					function (res){
						var objects = res.GeoObjectCollection.featureMember;
						if(objects.length > 0){
							var data = {
								items:[]
							}
							for(item in objects){
								//console.log(objects[item].GeoObject);
								data.items.push({
									name:objects[item].GeoObject.name,
									link:objects[item].GeoObject.uriMetaData.URI.uri
								});
							}
							$('.js_search_ymaps').addClass('show');
							response($.map(data.items, function (item) {
								return {
									value: item.name,
									link: item.link
								};
							}));
						}
						//restorationMap.setCenter(res.geoObjects.get(0).geometry.getCoordinates());
						//restorationMap.setZoom(13);
					}
				);
				
				/*
				var url = $(this.element).attr('data-url');
				$.ajax({
					url: url,
					data: {query:request.term},
					dataType:'JSON',
					success: function (data) {
						var size = Object.keys(data.items).length;
						$searchResults.addClass('show');
						if (size > 0) {
							$searchResults.removeClass('no_result');
							$searchResults.find('.empty-message').hide();

							response($.map(data.items, function (item) {
								return {
									value: item.name,
									link: item.link
								};
							}));
						}
						else {
							$searchResults.addClass('no_result');
							$searchResults.find('.empty-message').show();
						}
					}
				});
				*/
			}
		}).data("ui-autocomplete")._renderItem = function (ul, item) {

			var inner_html = '<a href="' + item.link + '" class="js_bracing_ymaps_select_item">' + item.value + '</a>';

			return $("<li></li>")
				.data("item.autocomplete", item)
				.append(inner_html)
				.appendTo(ul);
		};
		
		$(document).on('click', '.js_bracing_ymaps_select_item', function(e){
			e.preventDefault();
		});
	}
	
	
	
	
	
	
	
	
	
	
	$(document).on('click', '.js_send_mail_favorites_pdf', function(e){
		e.preventDefault();
	});
	
	$('#form_send_email_favorites_pdf').validate({ 
        messages: { 
            'agreement': { 
                required: jsonLangConsts["TEXT_TO_ACCEPT_CONDITION"], 
            } 
        }, 
        submitHandler: function(form) {
            delay(function(){  
                var form_obj = $(form);  
                var action = form_obj.attr('action');  
                var params = form_obj.serialize();  
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){  
                        openFancyboxModal(data.header, data.text, data.status);
						if(data.status){
							form_obj.trigger('reset');
						}
						setTimeout(function() { 
							$.fancybox.close(); 
						}, 7000);
                    }
                });  
            },250);  
            return false;  
        },
		invalidHandler: function(event, validator) {
			return false;
		},
		errorPlacement: function(error, element) {
			return false;
		}
    });
	
	$(document).on('click', '.js_clear_care', function(e){
		e.preventDefault();
		
		$('.js-care-checkbox2 input:checkbox').removeAttr('checked').prop('checked', false);
		$('.js-care-checkbox2 input:checkbox:first').trigger('change');
	});
	
	
	
	$('#send_form_subscription').validate({
        messages: { 
            'agreement': { 
                required: jsonLangConsts["TEXT_TO_ACCEPT_CONDITION"], 
            } 
        }, 
        submitHandler: function(form) {
            delay(function(){  
                var form_obj = $(form);  
                var action = form_obj.attr('action');  
                var params = form_obj.serialize();  
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'HTML',  
                    'success': function(response){
						var data = {
							header:jsonLangConsts["TEXT_SUBSCRIPTION"],
							text:jsonLangConsts["TEXT_ERROR"],
							status:0
						}
						if($.trim(response) === 'OK'){
							data.status = 1;
							data.text = jsonLangConsts["TEXT_THANK_SUBSCRIBING"];
						}else{
							data.text = response;
						}
                        openFancyboxModal(data.header, data.text, data.status);
						if(data.status){
							form_obj.trigger('reset');
						}
						setTimeout(function() {
							$('#fancybox-container-3 .fancybox-close-small,#fancybox-container-2 .fancybox-close-small').trigger('click'); 
						}, 7000);
                    }
                });  
            },250);  
            return false;  
        }
    });
	
	
	
	setAutoCompleteSearchArticlesReviewBracing();
	
	
	
	
	$('#send_form_auth').validate({
        submitHandler: function(form) {
            delay(function(){  
                var form_obj = $(form);  
                var action = form_obj.attr('action');  
                var params = form_obj.serialize();  
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){
						if(data.status){
							location.href = data.url;
						}else{
							openFancyboxModal(data.header, data.text, data.status);
							setTimeout(function() { 
								$.fancybox.close(); 
							}, 7000);
						}
                    }
                });  
            },250);  
            return false;  
        },
		invalidHandler: function(event, validator) {
			return false;
		},
    });
	
	$('#send_form_reg').validate({
        submitHandler: function(form) {
            delay(function(){  
                var form_obj = $(form);  
                var action = form_obj.attr('action');  
                var params = form_obj.serialize();  
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){
						if(data.status){
							location.href = data.url;
						}else{
							openFancyboxModal(data.header, data.text, data.status);
							setTimeout(function() { 
								$.fancybox.close(); 
							}, 7000);
						}
                    }
                });  
            },250);  
            return false;  
        },
		invalidHandler: function(event, validator) {
			return false;
		},
		errorPlacement: function(error, element) {
			return false;
		}
    });
	
	/*
	$(document).on('click', '.js_variant_item', function(e){
		$(this).parents('.card-wrap').addClass('pre-loading');
	});
	*/
	
	$(document).on('keyup', '.js_card_count:input', function(e){
		var par = $(this);
		delay(function () {
			var max = par.attr('max');
			var min = par.attr('min');
			var qty = parseInt(par.val());
			if (!qty || isNaN(qty) || qty < 0) {
				qty = 1;
			}
			if(qty > max){
				qty = max;
			}
			if(qty < min){
				qty = min;
			}
			var price = parseFloat(par.parents('.js_wrap_card_info_basket').find('.js_item_price').attr('data-price'));
			if (price && !isNaN(price)) {
				var total = price * qty;
				par.parents('.js_wrap_card_info_basket').find('.js_item_price').text(formatPrice(total) + ' ₽');
			}

			//Обновялем, если находимся в корзине
			var qty_real = parseInt(par.val());
			if (par.hasClass('in_cart') && qty_real > 0 && qty_real) {
				var form_obj = par.parents('form');
				var action = form_obj.attr('action');
				var params = form_obj.serialize();

				$.ajax({
					'url': action,
					'type': 'POST',
					'data': params,
					'dataType': 'JSON',
					'success': function (data) {
						updateCart();
					}
				});
			}
		}, 500);
	});
	
	$(document).on('click', '.js-add-to-cart', function(e){
        e.preventDefault();
        var par = $(this);
        delay(function(){
            var form_obj = par.parents('form');
            var action = form_obj.attr('action');
            var params = form_obj.serialize();
			
            $.ajax({  
                'url': action,
                'type': 'POST',
                'data': params,
                'dataType':'JSON',
                'success': function(data){
					updateCart();
					//alert('Товар добавлен в корзину');
					$('.header-cart').addClass('show');
					setTimeout(function () {
						$('.header-cart').removeClass('show');
					}, 2000);
                }
            });
        },250);
    });
	
	$(document).on('click', '.js_cart_remove', function(e){
        e.preventDefault();
        var par = $(this);
        delay(function(){
			
			//Обнуляем кол-во перед отправкой
			par.parents('.js_item_row').find('.js_card_count').val(0);
			
            var form_obj = par.parents('form');
            var action = form_obj.attr('action');
            var params = form_obj.serialize();
			
            $.ajax({  
                'url': action,
                'type': 'POST',
                'data': params,
                'dataType':'JSON',
                'success': function(data){
					updateCart();
                }
            });
        },250);
    });
	
	$(document).on('click', '.js_cart_remove_in_folder', function(e){
        e.preventDefault();
        var par = $(this);
        delay(function(){
			
			//Обнуляем кол-во перед отправкой
			par.parents('.js_item_row').find('.js_card_count').val(0);
			
            var form_obj = par.parents('form');
            var action = form_obj.attr('action');
            var params = form_obj.serialize();
			
            $.ajax({  
                'url': action,
                'type': 'POST',
                'data': params,
                'dataType':'JSON',
                'success': function(data){
					updateCart();
                }
            });
        },250);
    });
	
	$(document).on('click', '.jq-number__spin.minus', function(e){
        e.preventDefault();
        var par = $(this);
        delay(function(){
            var form_obj = par.parents('form');
            var action = form_obj.attr('action');
            var params = form_obj.serialize();
			
            $.ajax({  
                'url': action,
                'type': 'POST',
                'data': params,
                'dataType':'JSON',
                'success': function(data){
					updateCart();
                }
            });
        },250);
    });
	
	$(document).on('click', '.jq-number__spin.plus', function(e){
        e.preventDefault();
        var par = $(this);
        delay(function(){
            var form_obj = par.parents('form');
            var action = form_obj.attr('action');
            var params = form_obj.serialize();
			
            $.ajax({  
                'url': action,
                'type': 'POST',
                'data': params,
                'dataType':'JSON',
                'success': function(data){
					updateCart();
                }
            });
        },250);
    });
	
	$('.js_save_form_edit_personal').on('click', function(e){
        e.preventDefault();
		$(this).parents('form').submit();
	});
	
	$('#send_form_edit_personal').validate({
        submitHandler: function(form) {
            delay(function(){  
                var form_obj = $(form);  
                var action = form_obj.attr('action');  
                var params = form_obj.serialize();
				
				var new_password = $.trim(form_obj.find('.js_new_password').val());
				var new_password2 = $.trim(form_obj.find('.js_new_password2').val());
				form_obj.find('.js_new_password,.js_new_password2').removeClass('error');
				if(new_password && new_password2 && new_password != new_password2){
					form_obj.find('.js_new_password,.js_new_password2').addClass('error');
					return false;
				}
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){
						if(new_password && new_password2 && new_password != new_password2){
							form_obj.find('.js_new_password,.js_new_password2').val('');
						}
						openFancyboxModal(data.header, data.text, data.status);
						setTimeout(function() { 
							$.fancybox.close(); 
						}, 7000);
                    }
                });  
            },250);  
            return false;  
        },
		invalidHandler: function(event, validator) {
			return false;
		},
		errorPlacement: function(error, element) {
			return false;
		}
    });
	
	$('.js_edit_address').on('click', function(e){
        e.preventDefault();
		var par = $(this);
		var json = JSON.parse(par.attr('data-json'));
		
		$('#send_form_personal_address_edit .js_edit_id').val(json.id);
		$('#send_form_personal_address_edit .js_city').val(json.city);
		$('#send_form_personal_address_edit .js_address').val(json.address);
		$('#send_form_personal_address_edit .js_zip').val(json.zip);
		
		$.fancybox.open({
			src:'#address-edit-modal',
			opts:fancyOpt
		});
	});
	
	$('.js_remove_address').on('click', function(e){
        e.preventDefault();
		var par = $(this);
		var id = par.attr('data-id');
		var url = par.attr('data-url');
		
		$.ajax({  
			'url': url,
			'type': 'POST',
			'data': {address_id:id},
			'dataType':'JSON',
			'success': function(data){
				par.parents('.js_item_row').addClass('deleted');
				par.parents('.js_item_row').find('.js_edit_address, .js_remove_address').hide();
				par.parents('.js_item_row').find('.js_address_return').show();
			}
		});
	});
	
	$('.js_address_return').on('click', function(e){
        e.preventDefault();
		var par = $(this);
		var id = par.attr('data-id');
		var url = par.attr('data-url');
		
		$.ajax({  
			'url': url,
			'type': 'POST',
			'data': {address_id:id, is_return:1},
			'dataType':'JSON',
			'success': function(data){
				par.hide();
				par.parents('.js_item_row').removeClass('deleted');
				par.parents('.js_item_row').find('.js_edit_address, .js_remove_address').show();
			}
		});
	});
	
	$('#send_form_personal_address_add').validate({
        submitHandler: function(form) {
            delay(function(){  
                var form_obj = $(form);  
                var action = form_obj.attr('action');  
                var params = form_obj.serialize();
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){
						//openFancyboxModal(data.header, data.text, data.status);
						if(data.status){
							//form_obj.trigger('reset');
							location.reload();
						}else{
                            openFancyboxModal(data.header, data.text, data.status);
						}
						/*
						setTimeout(function() { 
							$('#fancybox-container-3 .fancybox-close-small,#fancybox-container-2 .fancybox-close-small').trigger('click');
						}, 7000);
						*/
                    }
                });  
            },250);  
            return false;  
        },
		invalidHandler: function(event, validator) {
			return false;
		},
		errorPlacement: function(error, element) {
			return false;
		}
    });
	
	$('#send_form_personal_address_edit').validate({
        submitHandler: function(form) {
            delay(function(){  
                var form_obj = $(form);  
                var action = form_obj.attr('action');
                var params = form_obj.serialize();
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){
						//$.fancybox.close();
						//openFancyboxModal(data.header, data.text, data.status);
						if(data.status){
							//form_obj.trigger('reset');
							location.reload();
						}else{
                            openFancyboxModal(data.header, data.text, data.status);
						}
						/*
						setTimeout(function() { 
							$.fancybox.close();
						}, 7000);
						*/
                    }
                });  
            },250);  
            return false;  
        },
		invalidHandler: function(event, validator) {
			return false;
		},
		errorPlacement: function(error, element) {
			return false;
		}
    });
	
	$('.js_select_address_order').on('click', function(e){
		e.preventDefault();
		var json = JSON.parse($('.js_address_order:checked:first').attr('data-json'));
		$('#nc_netshop_add_order_form .js_order_city').val(json.city);
		$('#nc_netshop_add_order_form .js_order_address').val(json.address);
		$('#nc_netshop_add_order_form .js_order_zip').val(json.zip);
		$.fancybox.close();
	});
	
	$('#nc_netshop_add_order_form').validate({
        submitHandler: function(form) {
            return true;
        },
		invalidHandler: function(event, validator) {
			return false;
		},
		errorPlacement: function(error, element) {
			return false;
		}
    });
	
	$('.js_next_page_order').on('click', function(e){
		e.preventDefault();
		$(this).parents('form').find('.js_go_next_page').remove();
		$(this).parents('form').append('<input type="hidden" class="js_go_next_page" name="go_to_next_page" value="1" />');
		$(this).parents('form').submit();
	});
	
	$('#send_form_recovery_password').validate({
        submitHandler: function(form) {
            delay(function(){  
                var form_obj = $(form);
                var params = form_obj.serialize();
                  
                $.ajax({  
                    'url': location.pathname,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){
						$.fancybox.close(); 
						openFancyboxModal(data.header, data.text, data.status);
						if(data.status){
							form_obj.trigger('reset');
						}
						setTimeout(function() { 
							$.fancybox.close();
						}, 7000);
                    }
                });  
            },250);  
            return false;  
        },
		invalidHandler: function(event, validator) {
			return false;
		},
		errorPlacement: function(error, element) {
			return false;
		}
    });
	
	$('#send_form_change_password').validate({
        submitHandler: function(form) {
            delay(function(){  
                var form_obj = $(form);
				var action = form_obj.attr('action');
                var params = form_obj.serialize();
                  
                $.ajax({  
                    'url': action,  
                    'type': 'POST',  
                    'data': params,  
                    'dataType':'JSON',  
                    'success': function(data){
						$.fancybox.close(); 
						openFancyboxModal(data.header, data.text, data.status);
						if(data.status){
							form_obj.trigger('reset');
						}
						setTimeout(function() { 
							$.fancybox.close();
						}, 7000);
                    }
                });  
            },250);  
            return false;  
        },
		invalidHandler: function(event, validator) {
			return false;
		},
		errorPlacement: function(error, element) {
			return false;
		}
    });
	
	$(window).on('resize', function(){
		setImageCardHeight();
		setSimilarItemsHeight();
		setGalleryItemsHeight();
	});
	setImageCardHeight();
	setTimeout(function(){
		setSimilarItemsHeight();
		setGalleryItemsHeight();
	},150);
	
	$(document).on('click', '.js_open_modal_no_cache', function(e){
		e.preventDefault();
		var par = $(this);
		$.fancybox.open({
			src:par.attr('data-src'),
			type:'ajax',
			opts:fancyOpt
		});
	});
	
	
	$(document).on('click', '.js_show_company_list', function (e) {
		e.preventDefault();
		var object_link = $(this).attr('href');
        $.ajax({
            'url': object_link,
            'type': 'POST',
            'dataType':'HTML',
            'success': function(data){
                $('.js_content_company').html($(data).find('.js_company_detail').html());
                $(".js-modal-link").fancybox(fancyOpt);
                formsInit();
                setValidateFormReviewCompany();
                changeRating();
                $('.js_content_company').show();
                setAutoCompleteSearchArticlesReviewBracing();
                $('body,html').animate({
                    scrollTop: $('.js_content_company').offset().top - 100
                }, 1000);
            }
        });
    });



	$('#form_item_card_full_input_add_to_cart').on('submit', function (e) {
		e.preventDefault();
		return false;
    });

	$('#form_cart').on('submit', function (e) {
		e.preventDefault();
		return false;
    });
	
});