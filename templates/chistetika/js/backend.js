'use strict';


var jsonLangConsts = JSON.parse($('.js_lang_consts').attr('data-json'));


function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}

function changeRating(){
	$('.rating-vote-line').each(function () {
        var $currentLine = $(this);
        var $voteStars = $currentLine.find('.rating-vote__input');
        var currentVoteId = 0;

        $voteStars.hover(function () {
            var currentId = $(this).parent().index();
            $voteStars.removeClass('hover active');
            for(var i=0; i<currentId+1; i++){
                $($voteStars[i]).addClass('hover');
            }
        }, function () {
            $voteStars.removeClass('hover');
            for(var i=0; i<currentVoteId+1; i++){
                $($voteStars[i]).addClass('active');
            }
        });

        $voteStars.on('change', function () {
            currentVoteId = $currentLine.find('.rating-vote__input:checked').parent().index();
            $voteStars.removeClass('active');
            for(var i=0; i<currentVoteId+1; i++){
                $($voteStars[i]).addClass('active');
            }
        });
    });
}

function sendCareFilter(){
	var active_tab_index = $('.js-care-tabs .care-tabs__item.active').index();
	var arrSpotIds = [];
	$('.care-panes .js-care-checkbox:eq('+active_tab_index+') input:checkbox:checked').each(function(){
		arrSpotIds.push($(this).val());
	});
	
	var arrTypeClothIds = [];
	$('.js-care-checkbox2 input:checkbox:checked').each(function(){
		arrTypeClothIds.push($(this).val());
	});
	
	var url = $('.js_care_content').attr('data-url');
	$.ajax({  
		'url': url,  
		'type': 'POST',  
		'data': {spot_ids:arrSpotIds.join(','), type_cloth_ids:arrTypeClothIds.join(',')},  
		'dataType':'HTML',  
		'success': function(data){  
			$('.js_care_content').html(data);
			$('.care-result-cont').slideDown(recalcSticky);
			
			$('.care-result-cont').removeClass('care-result-cont_empty');
			if(data.indexOf('catalog-empty') !== -1){
				$('.care-result-cont').addClass('care-result-cont_empty');
			}
		}
	});
}


function socialShare(name, link, title, img, text) {
    var url;
    switch (name) {
        case 'tw':
            url = 'https://twitter.com/intent/tweet?';
            url += 'text=' + encodeURIComponent(title);
            url += '&url=' + encodeURIComponent(link);
            PopupCenter(url, '', 626, 436);
            break;
        case 'vk':
            url = 'http://vk.com/share.php?';
            url += 'url=' + encodeURIComponent(link);
            url += '&title=' + encodeURIComponent(title);
            url += '&image=' + encodeURIComponent(img);
            url += '&description=' + encodeURIComponent(text);
            url += '&noparse=true';
            PopupCenter(url, '', 626, 436);
            break;
        case 'pin':
            url = 'http://pinterest.com/pin/create/button/?';
            url += 'url=' + encodeURIComponent(link);
            url += '&media=' + encodeURIComponent(img);
            url += '&description=' + encodeURIComponent(title);
            PopupCenter(url, '', 626, 436);
            break;
        case 'gp':
            url = 'https://plus.google.com/share?';
            url += 'url=' + encodeURIComponent(link);
            url += '&title=' + encodeURIComponent(title);
            PopupCenter(url, '', 626, 436);
            break;
        case 'mail':
            url = 'mailto:?';
            url += 'body=' + encodeURIComponent(link) + '%0A'+ encodeURIComponent(text);
            url += '&subject=' + encodeURIComponent(title);
            window.open(url);
            break;
        case 'fb':
            FB.ui({
                    method: 'feed',
                    name: title,
                    link: link,
                    description: text,
                    picture: img
                }
            );
            break;
    }
}

$(document).ready(function () {


    if($('.js-share').length>0){

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '2041777582767218', // поменять appid при интеграции
                xfbml      : true,
                version    : 'v2.12'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }



    var filterSliders = document.querySelectorAll('.number-range');
    var filterResBlocks = $('.range-slider');

    $('.range-slider__input, .js-number-input').keydown(function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        } else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

    function addValues() {
        for (var i = 0; i < filterSliders.length; i++) {
			if(filterSliders[i].noUiSlider){
				var currentVal = filterSliders[i].noUiSlider.get();
			}else{
				var currentVal = [0,0];
			}
			
            $(filterResBlocks[i]).find('.range-slider__input_min').val(currentVal[0]);
            $(filterResBlocks[i]).find('.range-slider__input_max').val(currentVal[1]);
        }
		generateFilter();
    }

    if (filterSliders.length > 0) {

        $('.js-range-slider__input_min').on('change', function () {
            var currentVal = parseInt($(this).val().toString().replace(/[^\/\d]/g,''));
			if(isNaN(currentVal)){
				currentVal = 0;
			}
            var currentId = $(this).attr('data-id');
            if(filterSliders[currentId].noUiSlider){
				filterSliders[currentId].noUiSlider.set([currentVal, null]);
			}
            addValues();
        });

        $('.js-range-slider__input_max').on('change', function () {
            var currentVal = parseInt($(this).val().toString().replace(/[^\/\d]/g,''));
			if(isNaN(currentVal)){
				currentVal = 0;
			}
            var currentId = $(this).attr('data-id');
			if(filterSliders[currentId].noUiSlider){
				filterSliders[currentId].noUiSlider.set([null, currentVal]);
			}
            addValues();
        });


        var uiSlider = {
            init: function () {
                var rangeInputs = document.querySelectorAll('.number-range');
                uiSlider.setup(rangeInputs);
            },
            setup: function (sliders) {
                function data(element, value) {
                    return parseFloat(element.parentElement.getAttribute('data-' + value));
                }

                Array.prototype.forEach.call(sliders, function (slider) {
                    var min = data(slider, 'min'),
                        max = data(slider, 'max'),
                        step = data(slider, 'step'),
                        value = slider.parentElement.getAttribute('data-start').split(',');
					if(isNaN(min)){
						min = 0;
					}
					if(isNaN(max)){
						max = 0;
					}
					
					if(min === max){
						return false;
					}
					
                    noUiSlider.create(slider, {
                        start: value,
                        connect: true,
                        step: !isNaN(step) ? step : 1,
                        range: {'min': min, 'max': max},
                        format: {
                            to: function ( value ) {
                                value = parseInt(value);
								if(isNaN(value)){
									value = 0;
								}
                                return numberFormat(value.toString().replace(/[^\/\d]/g,''));
                            },
                            from: function ( value ) {
                                return value;
                            }
                        }
                    });

                    slider.noUiSlider.on('slide', addValues);
                });
            }
        };

        uiSlider.init();
    }

    $('.js-share a').on('click', function (e) {
        var name = $(this).attr('data-network');
        var $dataBlock = $('.js-share');
        var link = $dataBlock.attr('data-link');
        var title = $dataBlock.attr('data-title');
        var img = $dataBlock.attr('data-img');
        var text = $dataBlock.attr('data-text');
        socialShare(name, link, title, img, text);
        e.preventDefault();
    });


    if($('#contacts-map').length>0) {
        //$.getScript("https://api-maps.yandex.ru/2.1/?lang=ru-RU", function () {
            ymaps.ready(contactsMapInit);
        //});
    }

	
	var activeTypeSpotIds = [];
    $('.js-care-checkbox').each(function () {
        var $currentCheckboxes = $(this).find('input:checkbox');
        var $currentPics = $(this).find('.care-pics__item');

        $currentCheckboxes.on('change', function (e) {
			var currentId = $(this).closest('.checkbox-list__item').index();
			
            if($(this).is(':checked')){
				
                $currentPics.removeClass('active');
                $($currentPics[currentId]).addClass('active');
                $('.care-cloth').slideDown(recalcSticky);
                $('.care-not-selected').text(jsonLangConsts.TEXT_CHOOSE_CLOTH_TYPE);
				
				activeTypeSpotIds.push(currentId);
            }
            else{
                var get_current_index = activeTypeSpotIds.indexOf(currentId);
				if(get_current_index > -1){
				  activeTypeSpotIds.splice(get_current_index,1);
				}
				
                $currentPics.removeClass('active');
				if(activeTypeSpotIds.length > 0){
					$($currentPics[activeTypeSpotIds[activeTypeSpotIds.length - 1]]).addClass('active');
				}

                if($('.js-care-checkbox input:checked').length == 0){
                    $('.care-cloth').slideUp(recalcSticky);
                    $('.care-not-selected').text(jsonLangConsts.TEXT_SELECT_TYPE_SPOT);

                    $('.care-not-selected').show();
                    $('.care-result-cont').slideUp(recalcSticky);
                    $('.js-care-checkbox2 input').prop('checked', false);
                }
                else{
                    $('.care-cloth').slideDown(recalcSticky);
                    $('.care-not-selected').text(jsonLangConsts.TEXT_CHOOSE_CLOTH_TYPE);
                }
            }
			
			var active_tab_index = $('.js-care-tabs .care-tabs__item.active').index();
			var arrSpotIds = [];
			$('.care-panes .js-care-checkbox:eq('+active_tab_index+') input:checkbox:checked').each(function(){
				arrSpotIds.push($(this).val());
			});
			
			if(arrSpotIds.length > 0){
				$('.care-cloth').slideDown(recalcSticky);
				$('.care-not-selected').text(jsonLangConsts.TEXT_CHOOSE_CLOTH_TYPE);
			}else{
				$('.care-cloth').slideUp(recalcSticky);
				$('.care-not-selected').text(jsonLangConsts.TEXT_SELECT_TYPE_SPOT);

				$('.care-not-selected').show();
				$('.care-result-cont').slideUp(recalcSticky);
				$('.js-care-checkbox2 input').prop('checked', false);
				$currentPics.removeClass('active');
			}
			
			var arrTypeClothIds = [];
			$('.js-care-checkbox2 input:checkbox:checked').each(function(){
				arrTypeClothIds.push($(this).val());
			});
			if(arrTypeClothIds.length > 0){
				sendCareFilter();
			}
			
        });

    });

	
	var activeSpotIds = [];
    $('.js-care-checkbox2').each(function () {
        var $currentCheckboxes = $(this).find('input:checkbox');
        var $currentPics = $(this).find('.care-pics__item');

        $currentCheckboxes.on('change', function (e) {
			var currentId = $(this).closest('.checkbox-list__item').index();
			
            if($(this).is(':checked')){
                $currentPics.removeClass('active');
                $($currentPics[currentId]).addClass('active');

                $('.care-not-selected').hide();
                //$('.care-result-cont').slideDown(); //сюда подгрузить через ajax контент
				
				sendCareFilter();
				
				activeSpotIds.push(currentId);
            }
            else{
				var get_current_index = activeSpotIds.indexOf(currentId);
				if(get_current_index > -1){
				  activeSpotIds.splice(get_current_index,1);
				}
				
                $currentPics.removeClass('active');
				if(activeSpotIds.length > 0){
					$($currentPics[activeSpotIds[activeSpotIds.length - 1]]).addClass('active');
				}

                if($('.js-care-checkbox2').find('input:checked').length == 0){
                    $('.care-not-selected').show();
                    $('.care-result-cont').slideUp(recalcSticky);
                }
                else{
                    $('.care-not-selected').hide();
                    $('.care-result-cont').slideDown(recalcSticky); //сюда подгрузить через ajax контент
                }
            }
			
			var arrTypeClothIds = [];
			$('.js-care-checkbox2 input:checkbox:checked').each(function(){
				arrTypeClothIds.push($(this).val());
			});
			if(arrTypeClothIds.length > 0){
				sendCareFilter();
			}else{
				players('stopVideo');
			}
        });
    });


    if($('#restoration-map').length>0) {
        //$.getScript("https://api-maps.yandex.ru/2.1/?lang=ru-RU", function () {
            ymaps.ready(restorationMapInit);
        //});


        $('.js-restoration-show-map').on('click', function (e) {
            $(this).addClass('active');
            $('.js-restoration-show-list').removeClass('active');
            $('.restoration-map').slideDown();
            $('.restoration-list').slideUp();
			
			//
			$('.js_content_company').show();
			
			setTimeout(function(){
				restorationMap.container.fitToViewport();
			},500);
			
            e.preventDefault();
        });

        $('.js-restoration-show-list').on('click', function (e) {
            
			var arrIds = [];
			var objects = ymaps.geoQuery(restorationMapObjectManager.objects).searchInside(restorationMap);
			if(objects._objects.length > 0){
				objects.each(function (object){
					arrIds.push(object.properties._data.id);
				});
			}
			if(arrIds.length <= 0){
				$(this).attr('data-no-ids', 1);
			}else{
				$(this).removeAttr('data-no-ids');
				$(this).attr('data-ids', arrIds);
			}
			
			$(this).addClass('active');
            $('.js-restoration-show-map').removeClass('active');
            $('.restoration-map').slideUp();
            $('.restoration-list').slideDown();
			
			//
			$('.js_content_company').hide();
			
            e.preventDefault();
        });
    }


    $('body').on('click', '.js-catalog-load-more', function (e) {
        var $currentBtn = $(this);

        $('.catalog-wrap').addClass('pre-loading');
		$('.media-wrap').addClass('pre-loading');

        windTop = $(window).scrollTop();
		
		var url = $currentBtn.attr('href');
		var param_name = $currentBtn.attr('data-param-name');
		var param_value = $currentBtn.attr('data-param-value');
		var data = {};
		data[param_name] = param_value;
		data['isNaked'] = 1;
		$.ajax({  
			'url': url,  
			'type': 'POST', 
			'dataType':'HTML',
			'data': data, 
			'success': function(data){
				var currentTopPos = $currentBtn.offset().top - 120;
				$('.catalog-list').append($(data).find('.catalog-list').html());
				$('.js_catalog_list').append($(data).find('.js_catalog_list').html());
				$.fn.matchHeight._update();
				
				var href = $(data).find('.js-catalog-load-more').attr('href');
				var text = $(data).find('.js-catalog-load-more').text();
				if(href){
					$currentBtn.attr('href', href);
					$currentBtn.text(text);
				}else{
					$currentBtn.remove();
				}
				$('.page-nav').html($(data).find('.page-nav').html());
				
                catalogLoad();
				
                $("html,body").stop().animate({scrollTop: currentTopPos}, 800);
				$('.media-wrap').removeClass('pre-loading');
				setTimeout(function () {
					$.fn.matchHeight._update();
					$('.media__item').addClass('animate-in');
				}, 100); // timeout example
			}  
		});
		
		
		
		
        e.preventDefault();
    });


    if(viewport().width>768 && $('.js-search-autocomplete').length>0) {
        $('.js-search-autocomplete').on('blur', function () {
            $('.js-header-search-results').removeClass('show');
        });

        var $searchResults = $('.js-header-search-results');
		
		$('.js-search-autocomplete').on('keyup', function(e){
			var val = $.trim($(this).val());
			if(val.length <= 1){
				$searchResults.removeClass('show');
			}
		});
		
        $('.js-search-autocomplete').autocomplete({
            appendTo: ".js-header-search-results",
			delay:500,
			minLength:2,
            focus: function (event, ui)
            {
                var currentValue = ui.item.value.replace(/<{1}[^<>]{1,}>{1}/g," ");
                $('.js-search-autocomplete').val(currentValue);
                return false;
            },
            select: function (event, ui)
            {
                var currentValue = ui.item.value.replace(/<{1}[^<>]{1,}>{1}/g," ");
                $('.js-search-autocomplete').val(currentValue);
                return false;
            },
            source: function (request, response) {
                var url = $(this.element).attr('data-url');
				$.ajax({
                    url: url,
                    data: {query:request.term},
					dataType:'JSON',
                    beforeSend: function () {
                        $('.header-search').addClass('loading');
                    },
                    success: function (data) {
						var size = Object.keys(data.items).length;
						$('.header-search').removeClass('loading');
						$searchResults.addClass('show');
						if (size > 0) {
							$searchResults.removeClass('no_result');
							$searchResults.find('.empty-message').hide();
							$('.js-search-full-url').attr('href', data.url.value+'?search_query='+request.term);
							$('.js-search-full-url .count').text(data.url.count);

							response($.map(data.items, function (item) {
								return {
									value: item.name,
									link: item.link
								};
							}));
						}
						else {
							$searchResults.addClass('no_result');
							$searchResults.find('.empty-message').show();
						}
                    }
                })
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {

            var inner_html = '<a href="' + item.link + '">' + item.value + '</a>';

            return $("<li></li>")
                .data("item.autocomplete", item)
                .append(inner_html)
                .appendTo(ul);
        };
    }


    $('.js-catalog-view').on('click', function (e) {
        if(!$(this).hasClass('active')) {
            var currentUrl = $(this).attr('data-href');
            var currentType = $(this).attr('data-type');

            $('.js-catalog-view').removeClass('active');
            $(this).addClass('active');

            $('.catalog-wrap').addClass('pre-loading');
			
			$('.catalog-list').find('.catalog-list__item').addClass('animated');
			
			if (currentType == 0) {
				$('.catalog-list').removeClass('type2');
				$('.catalog-list').addClass('type1');
			}
			if (currentType == 1) {
				$('.catalog-list').removeClass('type1 type2');
			}
			if (currentType == 2) {
				$('.catalog-list').removeClass('type1');
				$('.catalog-list').addClass('type2');
			}
			
			$('.catalog-list').find('.catalog-list__item').removeClass('animated');
			$('.catalog-wrap').removeClass('pre-loading');
        }

       e.preventDefault();
    });



    changeRating();


    $(document).on('click', '.js-review-vote', function (e) {
        if($(this).parent().hasClass('disabled')){
			e.preventDefault();
			return false;
        }
		
		var par = $(this);
		var id = parseInt(par.attr('data-id'));
		var company = parseInt(par.attr('data-company'));
		var type = parseInt(par.attr('data-type'));
		var url = par.attr('data-url');
		
		var total = parseInt(par.find('.js_total_value').text());
		if(isNaN(total)){
			total = 0;
		}
		
		$.ajax({  
			'url': url,  
			'type': 'POST',  
			'data': {id:id,company_id:company,type:type},  
			'dataType':'JSON',  
			'success': function(data){
				if(!data.status){
					alert(data.text);
				}else{
					par.parent().addClass('disabled');
					total++;
					par.find('.js_total_value').text(total);
				}
			}
		});
		
    });

	var arrClassFavoriteBlockAll = ['is_catalog', 'is_favorite', 'is_card', 'is_similar'];

    $('body').on('click', '.js-add-favorites', function (e) {
        e.preventDefault();
		var par = $(this);
		$(this).toggleClass('active');
        $('.favorites-scroll-wrap__empty').hide();
        $('.favorites-popup__links').css('display', 'flex');
		var url = $(this).attr('data-url');
		var id = parseInt($(this).attr('data-id'));
		var class_id = parseInt($(this).attr('data-class-id'));
		
		
		var total = parseInt($('.js_header_favorites_items_total').text());
		if(isNaN(total)){
			total = 0;
		}
		
        if($(this).hasClass('active')){
			$.ajax({  
				'url': url,  
				'type': 'POST',  
				'data': {id:id,class_id:class_id},  
				'dataType':'JSON',  
				'success': function(data){
					$('.favorites-scroll').slick('slickAdd', '<div><div class="favorites-scroll__item">'+data.html_header_element_one+'</div></div>');
					$('.favorites-scroll').slick('unslick');
					setFavoriteSlider();
				}
			});
			
			var currentTop = $(this).offset().top;
            var currentLeft = $(this).offset().left;

            var headerFavTop = currentTop - $('.js-toggle-favorites').offset().top;
            var headerFavLeft = $('.js-toggle-favorites').offset().left  - currentLeft;

            $('.favorites-animate').css({'top':currentTop, 'left': currentLeft, 'transform': 'translate('+ headerFavLeft +'px, -'+headerFavTop+'px)'}).addClass('animate');

            setTimeout(function () {
                $('.favorites-animate').attr('style', '').removeClass('animate');
            }, 400);
			
			total++;
        }else{
			total--;
			//Получаем имя-класса, по которому кликнули, чтобы потом при вывозе
			//$('.js_remove_favorite_header_'+class_id+'_'+id).trigger('click');
			//установить данный класс, чтобы там удалить отовсюду, кроме текущего кликнутого элемента, иначе будет дублирование состояния
			var active_class_favorite_from = '';
			for(var i = 0; i <= arrClassFavoriteBlockAll.length; i++){
				if($(this).hasClass(arrClassFavoriteBlockAll[i])){
					active_class_favorite_from = arrClassFavoriteBlockAll[i];
					break;
				}
			}
			//Удаляем из шапки
			$('.js_remove_favorite_header_'+class_id+'_'+id).addClass(active_class_favorite_from).trigger('click');
			//Обновляем в разделе Изобранного
			if(active_class_favorite_from === 'is_favorite'){
				generateFilter();
			}
		}
		
		if(total <= 0){
			$('.js_header_favorites_items_total').hide().text(0);
		}else{
			$('.js_header_favorites_items_total').show().text(total);
		}
    });

    $('body').on('click', '.js-remove-favorites', function(e) {
		e.preventDefault();
		
		var par = $(this);
		var url = par.attr('data-url');
		var id = parseInt(par.attr('data-id'));
		var class_id = parseInt(par.attr('data-class-id'));
		
		var currentId = $('.favorites-scroll .slick-slide').index($(this).parents('.slick-slide')[0]);
        $('.favorites-scroll').slick('slickRemove', currentId);
        if($('.favorites-scroll__item').length == 0){
            $('.favorites-scroll-wrap__empty').show();
            $('.favorites-popup__links').hide()
        }
		
		$.ajax({  
			'url': url,  
			'type': 'POST',  
			'data': {id:id,class_id:class_id},  
			'dataType':'JSON',  
			'success': function(data){
				
			}  
		});
		
		//Удаляем из раздела избранного, из каталога, из модального окна, из карточки товара, из похожие ткани
		for(var i = 0; i <= arrClassFavoriteBlockAll.length; i++){
			if(!par.hasClass(arrClassFavoriteBlockAll[i])){
				$('.js-add-favorites.'+arrClassFavoriteBlockAll[i]).each(function(){
					var get_class_id = parseInt($(this).attr('data-class-id'));
					var get_id = parseInt($(this).attr('data-id'));
					if(get_class_id === class_id && get_id === id){
						$(this).toggleClass('active');
					}
				});
			}
		}
		
		var total = parseInt($('.js_header_favorites_items_total').text());
		if(isNaN(total)){
			total = 0;
		}
		total--;
		if(total <= 0){
			$('.js_header_favorites_items_total').hide().text(0);
		}else{
			$('.js_header_favorites_items_total').show().text(total);
		}
    });
	
	
	
	
	$('body').on('click', '.js-open-email-form', function (e) {
		$('.js-open-email-form').hide();
		$('.email-form').show();
		e.preventDefault();
	});

	$('body').on('click', '.js-close-email-form', function (e) {
		$('.js-open-email-form').show();
		$('.email-form').hide();
		e.preventDefault();
	});
	
	
	$('body').on('click', '.js-filter-media', function (e) {
        var currentUrl = $(this).attr('href');
        $('.media-category__item').removeClass('active');
        $(this).parent().addClass('active');
        $('.media-wrap').addClass('pre-loading');
        $.ajax({
            url: currentUrl
        }).done(function (data) {
			$('#media-ajax').html($(data).find('#media-ajax').html());
			$('.media-wrap').removeClass('pre-loading');
			$.fn.matchHeight._update();
            
			setTimeout(function () {
                $.fn.matchHeight._update();
                $('.media__item').addClass('animate-in');
            }, 100); // timeout example
        });
        e.preventDefault();
    });


});


function catalogLoad(){

    $('.js-item-tile-slider').each(function () {
        var $currentSlider = $(this);

        if(!$currentSlider.hasClass('slick-initialized')) {
			
			$currentSlider.on('init', function(event, slick){
				setIconItem($currentSlider);
			});
			
            $currentSlider.slick({
                lazyLoad: 'ondemand',
                dots: false,
                infinite: true,
                arrows: true,
                speed: 600,
                slidesToShow: 1,
                fade: false,
                prevArrow: '<button type="button" class="slick-prev"><span></span></button>',
                nextArrow: '<button type="button" class="slick-next"><span></span></button>',
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            arrows: false,
                            touchMove: false,
                            draggable: false,
                            swipe: false
                        }
                    }
                ]
            });
			
			$currentSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
				if(nextSlide != 0){
					$(this).closest('.item-tile').addClass('show-info');
				}
				else{
					$(this).closest('.item-tile').removeClass('show-info');
				}
			});
			
			$currentSlider.on('afterChange', function(event, slick, currentSlide){
				var json_item = JSON.parse($currentSlider.find('.slick-current .item-tile-slider__link').attr('data-json'));
				$currentSlider.parents('.catalog-list__item').find('.item-tile__name').text(json_item.name);
				setIconItem($currentSlider);
			});
        }
    });

    $('.catalog-list__item').removeClass('animated');

    $('.catalog-wrap').removeClass('pre-loading');
	
	$(".js-modal-link").fancybox(fancyOpt);
}


var contactsMap = '';

var restorationMap = '';
var restorationMapObjectManager = '';

function contactsMapInit() {
	
	var template_path = jsonLangConsts.TEMPLATE_PATH;

    var mapObj = $('#contacts-map');

    var zoomVal = $('#contacts-map').attr('data-zoom');

    if(windWidth<768){
        zoomVal *= 0.7;
    }
    var zoomControlSizeVal = 'small';

    if(windWidth>1023){
        zoomControlSizeVal = 'large';
    }
    if(contactsMap){
        contactsMap.destroy();
    }

    var cityName = $('#contacts-map').attr('data-city');

    var myGeocoder = ymaps.geocode(cityName);

    myGeocoder.then(
        function (res) {

            var myMap = new ymaps.Map('contacts-map', {
                center: res.geoObjects.get(0).geometry.getCoordinates(),
                zoom: zoomVal,
                controls: [],
                zoomControlSize: zoomControlSizeVal
            });

            contactsMap = myMap;

            if(isTouchDev && windWidth<768) {
                myMap.behaviors.disable('drag');
            }
            myMap.behaviors.disable("scrollZoom");

            var multiTimeout;

            myMap.events.add('multitouchstart', function (e) {
                clearTimeout(multiTimeout);
                $('.contacts-map-wrap').addClass('hide-caption');
            });

            myMap.events.add('multitouchend', function (e) {
                multiTimeout = setTimeout(function () {
                    $('.contacts-map-wrap').removeClass('hide-caption');
                }, 1000);
            });

            var zoomControl = new ymaps.control.ZoomControl({options: { position: { right: 10, top: 10 }}});

            myMap.controls.add(zoomControl);

            var pointArr = $('#contacts-map').attr('data-points').split(';');
            var pointArrIds = $('#contacts-map').attr('data-ids').split(';');

            var activeContact = pointArrIds[0];

            var objectManager = new ymaps.ObjectManager({
                clusterize: true
            });

            var MyIconContentLayout = ymaps.templateLayoutFactory.createClass('<div class="cluster-ico">$[properties.geoObjects.length]</div>');

            objectManager.clusters.options.set({
                clusterIcons:  [{
                    href: template_path+'img/cluster-ico.svg',
                    size: [40, 40],
                    offset: [-20, -20]
                }],
                clusterIconContentLayout: MyIconContentLayout
            });


            var myObjects = [];
            var pointObject = {};

            for (var i = 0, l = pointArr.length; i < l; i++) {
                myObjects.push({
                    type: 'Feature',
                    id: pointArrIds[i],
                    geometry: {
                        type: 'Point',
                        coordinates: pointArr[i].split(',')
                    }
                });

                pointObject[pointArrIds[i]] = pointArr[i].split(',');
            }

            objectManager.add(myObjects);
            myMap.geoObjects.add(objectManager);


            objectManager.objects.options.set({
                iconLayout: 'default#image',
                iconImageHref: template_path+'img/map-pin.svg',
                iconImageSize: [25, 36],
                iconImageOffset: [-12, -36]
            });


            function pointClick(objectId){
                var pointArr = $('#contacts-map').attr('data-points').split(';');
				var pointArrIds = $('#contacts-map').attr('data-ids').split(';');
				var pointObject = {};
				for (var i = 0, l = pointArr.length; i < l; i++) {
					pointObject[pointArrIds[i]] = pointArr[i].split(',');
				}
				
				var oldActive = 0;
                var is_change_city = parseInt($('#contacts-map').attr('data-change-city'));
                if(!is_change_city || isNaN(is_change_city)){
                    oldActive = pointObject[activeContact];
                }else{
                    oldActive = $('#contacts-map').attr('data-center').split(',');
                }
				if(!oldActive){
					oldActive = pointArrIds[0];
				}
				

                objectManager.objects.setObjectOptions(activeContact, {
                    iconLayout: 'default#image',
                    iconImageHref: template_path+'img/map-pin.svg',
                    iconImageSize: [25, 36],
                    iconImageOffset: [-12, -36]
                });

                activeContact = objectId;

                objectManager.objects.setObjectOptions(objectId, {
                    iconLayout: 'default#image',
                    iconImageHref: template_path+'img/map-pin-selected.svg',
                    iconImageSize: [40, 58],
                    iconImageOffset: [-20, -58]
                });

                var currentPoint = pointObject[objectId];

                myMap.panTo([oldActive, currentPoint], { delay: 0, duration: 400}).then(function () {
                    myMap.setZoom(14, {
                        duration: 400
                    });

                    if(viewport().width<1024){
                        myMap.balloon.open(myMap.getCenter(), { content: $('.company-info[data-id="'+activeContact+'"]').html() });
                    }

                }, function (err) {
                }, this);

                $('.company-info').removeClass('active');

                $('.company-info').each(function(){
                    var get_id = $(this).attr('data-id');
                    if(get_id == activeContact){
                        $(this).addClass('active');
                        if(windWidth>1023) {
                            var target_el = $(this).offset().top - windHeight/2;

                            $("html,body").stop().animate({scrollTop: target_el}, 800);
                        }
						$('#form_send_app_contact #whom').val(get_id).trigger('refresh');
                    }
                });

                $('#contacts-map').attr('data-change-city',0);
            }

            objectManager.objects.events.add('click', function (e) {
                var objectId = e.get('objectId');
                pointClick(objectId);
            });


            $(document).on('click', '.company-info', function () {
                var currentId = $(this).attr('data-id');
                pointClick(currentId);
            });
        },
        function (err) {
            console.log(err);
        }
    );
}

function restorationMapInit() {

	var template_path = jsonLangConsts.TEMPLATE_PATH;

    var mapObj = $('#restoration-map');

    var zoomVal = mapObj.attr('data-zoom');

    if(windWidth<768){
        zoomVal *= 0.7;
    }
    var zoomControlSizeVal = 'small';

    if(windWidth>1023){
        zoomControlSizeVal = 'large';
    }
    if(restorationMap){
        restorationMap.destroy();
    }

    var cityName = mapObj.attr('data-city');

    var myGeocoder = ymaps.geocode(cityName);

    myGeocoder.then(
        function (res) {
			
            var myMap = new ymaps.Map('restoration-map', {
                center: res.geoObjects.get(0).geometry.getCoordinates(),
                zoom: zoomVal,
                controls: [],
                zoomControlSize: zoomControlSizeVal
            });

            restorationMap = myMap;

            if(isTouchDev && windWidth<768) {
                myMap.behaviors.disable('drag');
            }
            myMap.behaviors.disable("scrollZoom");

            var multiTimeout;

            myMap.events.add('multitouchstart', function (e) {
                clearTimeout(multiTimeout);
                $('.restoration-map-wrap').addClass('hide-caption');
            });

            myMap.events.add('multitouchend', function (e) {
                multiTimeout = setTimeout(function () {
                    $('.restoration-map-wrap').removeClass('hide-caption');
                }, 2000);
            });

            var zoomControl = new ymaps.control.ZoomControl({options: { position: { right: 10, top: 10 }}});

            myMap.controls.add(zoomControl);

            var pointArr = $(mapObj).attr('data-points').split(';');
            var pointArrIds = $(mapObj).attr('data-ids').split(';');
			var pointInfoArr = $(mapObj).attr('data-points-info').split(';');

            var activeContact = pointArrIds[0];
			if(mapObj.hasClass('is_one')){
				myMap.setCenter(pointArr[0].split(','));
			}

            var objectManager = new ymaps.ObjectManager({
                clusterize: true
            });

            var MyIconContentLayout = ymaps.templateLayoutFactory.createClass('<div class="cluster-ico">$[properties.geoObjects.length]</div>');

            objectManager.clusters.options.set({
                clusterIcons:  [{
                    href: template_path+'img/cluster-ico.svg',
                    size: [40, 40],
                    offset: [-20, -20]
                }],
                clusterIconContentLayout: MyIconContentLayout
            });


            var myObjects = [];
            var pointObject = {};
			var currentInfo;
			var properties = {};

            for (var i = 0, l = pointArr.length; i < l; i++) {
				
				currentInfo = JSON.parse(pointInfoArr[i]);
				properties = {
					balloonContentHeader: currentInfo.name,
					balloonContentBody: currentInfo.content,
					id:pointArrIds[i],
				}
				
                myObjects.push({
                    type: 'Feature',
                    id: pointArrIds[i],
					properties: properties,
                    geometry: {
                        type: 'Point',
                        coordinates: pointArr[i].split(',')
                    }
                });

                pointObject[pointArrIds[i]] = pointArr[i].split(',');
            }

            objectManager.add(myObjects);
            myMap.geoObjects.add(objectManager);


            objectManager.objects.options.set({
                iconLayout: 'default#image',
                iconImageHref: template_path+'img/map-pin.svg',
                iconImageSize: [25, 36],
                iconImageOffset: [-12, -36],
				hideIconOnBalloonOpen: false,
				balloonOffset:[0, -36]
            });


            function pointClick(objectId){
                var oldActive = 0;
                var is_change_city = parseInt($(mapObj).attr('data-change-city'));
                if(!is_change_city || isNaN(is_change_city)){
                    oldActive = pointObject[activeContact];
                }else{
                    oldActive = $(mapObj).attr('data-center').split(',');
                }

                objectManager.objects.setObjectOptions(activeContact, {
                    iconLayout: 'default#image',
                    iconImageHref: template_path+'img/map-pin.svg',
                    iconImageSize: [25, 36],
                    iconImageOffset: [-12, -36],
					hideIconOnBalloonOpen: false,
					balloonOffset:[0, -36]
                });

                activeContact = objectId;

                objectManager.objects.setObjectOptions(objectId, {
                    iconLayout: 'default#image',
                    iconImageHref: template_path+'img/map-pin-selected.svg',
                    iconImageSize: [40, 58],
                    iconImageOffset: [-20, -58],
					hideIconOnBalloonOpen: false,
					balloonOffset:[0, -36]
                });


                var currentPoint = pointObject[objectId];
				
				/*
                myMap.panTo([oldActive, currentPoint], { delay: 0, duration: 400}).then(function () {
                    myMap.setZoom(14, {
                        duration: 400
                    });

                }, function (err) {
                }, this);
				*/

            }
			
			objectManager.objects.events.add('mouseenter', function (e) {
                var geoObject = e.get('target'),
                    objectId = e.get('objectId'),
                    balloon = objectManager.objects.balloon;
                balloon.open(objectId);
                balloon.events.add('mouseleave', function () {
                    balloon.close();
                });
            });

            objectManager.objects.events.add('click', function (e) {
                var objectId = e.get('objectId');
                pointClick(objectId);
				
				var object_link = '';
				$('.restoration-list .restoration-list__item').each(function(){
					if(objectId == $(this).attr('data-id')){
						object_link = $(this).attr('href');
					}
				});
				
				if(object_link){
					$.ajax({
						'url': object_link,
						'type': 'POST',
						'dataType':'HTML',
						'success': function(data){
							$('.js_content_company').html($(data).find('.js_company_detail').html());
							$(".js-modal-link").fancybox(fancyOpt);
							formsInit();
							setValidateFormReviewCompany();
							changeRating();
							$('.js_content_company').show();
							setAutoCompleteSearchArticlesReviewBracing();
						}  
					});
				}
            });
			
			
			myMap.events.add('actionend', function (e) {
				if($('.js-restoration-show-list').hasClass('active')){
					return false;
				}
				
				var arrIds = [];
				var objects = ymaps.geoQuery(objectManager.objects).searchInside(myMap);
				if(objects._objects.length > 0){
					objects.each(function (object){
						arrIds.push(object.properties._data.id);
					});
				}
				if(arrIds.length <= 0){
					arrIds = -1;
				}
				
				var country_id = $('select.js_change_country').find('option:selected').val();
				
				$.ajax({
					'url': location.pathname,
					'type': 'GET',
					'dataType':'HTML',
					'data':{ids:arrIds,country_id:country_id},
					'success': function(data){
						$('.restoration-list').html($(data).find('.restoration-list').html());
					}  
				});
			});
			
			if(!$('.js-restoration-show-list').hasClass('active')){
				var arrIds = [];
				var objects = ymaps.geoQuery(objectManager.objects).searchInside(myMap);
				if(objects._objects.length > 0){
					objects.each(function (object){
						arrIds.push(object.properties._data.id);
					});
				}
				if(arrIds.length <= 0){
					arrIds = -1;
				}
				
				var country_id = $('select.js_change_country').find('option:selected').val();
				
				$.ajax({
					'url': location.pathname,
					'type': 'GET',
					'dataType':'HTML',
					'data':{ids:arrIds,country_id:country_id},
					'success': function(data){
						$('.restoration-list').html($(data).find('.restoration-list').html());
					}  
				});
			}

            $(document).on('click', '.company-info', function () {
                var currentId = $(this).attr('data-id');
                pointClick(currentId);
            });
			
			restorationMapObjectManager = objectManager;
        },
        function (err) {
            console.log(err);
        }
    );
}