<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

$id = (int)$_POST['id'];
$class_id = (int)$_POST['class_id'];

$arrResult = array(
	'status' => 0,
	'text' => 'error',
	'html_header_element_one' => NULL,
);

if(!empty($id) && !empty($class_id)){
	$arrResult['status'] = 1;
	$arrResult['text'] = 'OK';
	$netshop = nc_netshop::get_instance();
	$netshop->goodslist_favorite->toggle($id, $class_id);
	
	if($class_id !== 31){
		$allItems = $db->get_results("SELECT Message_ID FROM Message31 
									  WHERE Collection = '".(int)$id."' AND 
											Parent_Message_ID = 0 AND 
											Language_Parent IS NULL",ARRAY_A);
		if(!empty($allItems)){
			$arrItemToJson = array();
			foreach($allItems as $arrRow){
				$arrItemToJson[] = array(
					'id' => $arrRow['Message_ID'],
				);
			}
		}
	}else{
		$arrItemToJson = array(
			array(
				'id' => $id,
				'show_by_article' => true,
			),
		);
	}
	
	$arrResult['html_header_element_one'] = trim(s_list_class(sct::sub('item'), sct::cc('item'), "nc_ctpl=".sct::tpl('item')."&isNaked=1&json_favorites_small=".json_encode($arrItemToJson)));
}

ob_end_clean();
echo json_encode($arrResult);
exit();
?>