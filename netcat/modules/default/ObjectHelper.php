<?php


class ObjectHelper
{
    /**
     * Проверяет корректность заполнения полей
     *
     * @param array $dataFields поля, которые нужно проверить
     *
     * @return array массив с ошибками и переменной, которая потом пойдет в $posting
     */
    public static function checkFields($dataFields = false)
    {
        global $f_Language_ID;

        $checkPassed = 0;
        $checkWarn = [];

        if ($f_Language_ID == 2 || !is_array($dataFields)) {
            return array('warn' => 'Неизвестная ошибка', 'passed' => $checkPassed);
        }

        foreach ($dataFields as $dataField) {
            $varName = "f_{$dataField}_ru";
            global $$varName;
            if ($$varName == '') {
                $checkWarn[] = "Не заполнено поле {$dataField} (русская версия);";
            }
        }

        foreach ($dataFields as $dataField) {
            $varName = "f_{$dataField}_en";
            global $$varName;
            if ($$varName == '') {
                //$checkWarn[] = "Не заполнено поле {$dataField} (английская версия);";
            }
        }

        if (count($checkWarn) == 0) {
            $checkPassed = 1;
        }

        return array('warn' => implode(' ', $checkWarn), 'passed' => $checkPassed);
    }

    /**
     * Функция создает полную копию объекта
     *
     * @param int $messageId элемент, для которого нужно создать копию
     * @param int $typeLanguageId язык, который будет указан в копии
     * @param bool $nullSubAndSubClass обнулять ли Sub и SubClass для копии (чтобы не показывать ее нигде)
     *
     * @return bool результат работы функции
     */
    public static function addCopy($messageId = false, $typeLanguageId = 2, $nullSubAndSubClass = false)
    {
        global $db, $current_cc, $f_Language_ID;

        if (!$messageId || $f_Language_ID == 2 || !is_array($current_cc)) {
            return false;
        }

        /*
            CREATE TEMPORARY TABLE temp_table SELECT * FROM Message2019 WHERE Message_ID = 7;
            UPDATE temp_table SET Message_ID = Message_ID+1,Language_ID = 2,Language_Parent = Message_ID;
            INSERT INTO Message2019 SELECT * from temp_table;
            DROP TABLE temp_table;
        */

        $class_id = $current_cc["Class_ID"];
        $tempTable = 'TempTable_' . md5(time());
        $classTable = 'Message' . $class_id;

        $db->query("CREATE TEMPORARY TABLE {$tempTable} SELECT * FROM {$classTable} WHERE Message_ID = '{$messageId}';");
        $db->query("UPDATE `{$tempTable}` SET `Parent_Message_ID` = '0', `Language_Parent` = `Message_ID`, `Message_ID` = `Message_ID` + 1, `Language_ID` = '{$typeLanguageId}';");

        //для обнуления Subdivision_ID и Sub_Class_ID для категории английской версии
        if ($nullSubAndSubClass) {
            $db->query("UPDATE `{$tempTable}` SET `Subdivision_ID` = '0', `Sub_Class_ID` = '0';");
        }
        $db->query("INSERT INTO `{$classTable}` SELECT * FROM `{$tempTable}`;");
		//add 13.09.2017 - Если идет редактирование объекта, для него не нашлось английской версии
		//то при добавлении в таблице будет дубликат, соответственно надо взять max id в этой таблице
		if(mb_strripos($db->last_error,"Duplicate") !== false){
			$db->last_error = "";
			$max_id = $db->get_var("SELECT MAX(`Message_ID`) FROM `{$classTable}`");
			$max_id++;
			$db->query("UPDATE `{$tempTable}` SET `Message_ID` = '".$max_id."'");
			$db->query("INSERT INTO `{$classTable}` SELECT * FROM `{$tempTable}`;");
		}
        $db->query("DROP TABLE `{$tempTable}`;");

        return true;
    }

    /**
     * Функция удаляет копию объекта
     *
     * @param int $messageId элемент, для которого нужно удалить копию
     * @param int $typeLanguageId язык, копию с которым нужно удалить
     * @param bool $nullSubAndSubClass выбирать ли только обнуленные Sub и SubClass или все сразу
     *
     * @return bool результат работы функции
     */
    public static function dropCopy($messageId = false, $typeLanguageId = 2, $nullSubAndSubClass = false)
    {
        global $db, $current_cc, $f_Language_ID;

        if (!$messageId || $f_Language_ID == 2 || !is_array($current_cc)) {
            return false;
        }

        $class_id = $current_cc["Class_ID"];
        $classTable = 'Message' . $class_id;

        if ($nullSubAndSubClass) {
            $db->query("DELETE FROM {$classTable} WHERE `Language_Parent` = '{$messageId}' AND `Language_ID` = '{$typeLanguageId}' AND `Subdivision_ID` = '0' AND `Sub_Class_ID` = '0';");
        } else {
            $db->query("DELETE FROM {$classTable} WHERE `Language_Parent` = '{$messageId}';");
        }

        return true;
    }

    /**
     * Получает данные по объекту
     *
     * @param int $messageId объект, по которому нужно собрать данные
     * @param array $dataFields мультиязычные поля объекта
     *
     * @return bool|array массив с данными по объекту
     */
    public static function getData($messageId = false, $dataFields = false)
    {
        global $db, $current_cc, $f_Language_ID;

        if (!$messageId || $f_Language_ID == 2 || !is_array($current_cc) || !is_array($dataFields)) {
            return false;
        }

        $class_id = $current_cc["Class_ID"];
        $classTable = 'Message' . $class_id;

        $selectRuDataArray = array();
        foreach ($dataFields as $dataField) {
            $selectRuDataArray[] = "`ru`.`{$dataField}` AS `{$dataField}_Ru`";
        }
        $selectRuData = implode(', ', $selectRuDataArray);

        $selectEnDataArray = array();
        foreach ($dataFields as $dataField) {
            $selectEnDataArray[] = "`en`.`{$dataField}` AS `{$dataField}_En`";
        }
        $selectEnData = implode(', ', $selectEnDataArray);

        $data = $db->get_row("
          SELECT {$selectRuData}, {$selectEnData} 
          FROM `{$classTable}` `ru` 
          LEFT JOIN `{$classTable}` `en` ON (`ru`.`Message_ID` = `en`.`Language_Parent` AND `en`.`Language_ID` = '2')
          WHERE `ru`.`Message_ID` = '{$messageId}';
        ");

        return $data;
    }

    /**
     * Функция заполняет мультиязычные поля в таблице
     *
     * @param int $messageId элемент, для которого нужно обработать мультиязычность
     * @param array $dataFields поля, для которых нужно обработать мультиязычность
     * @param string $keywordBase поле, с которого нужно сгенерировать кейворд
     * @param array $keywordsAdd поля, которые нужно дописать к кейворду
     *
     * @return bool результат работы функции
     */
    public static function fillData($messageId = false, $dataFields = false, $keywordBase = false, $keywordAdd = false)
    {
        global $db, $current_cc, $f_Language_ID;

        if (!$messageId || $f_Language_ID == 2 || !is_array($current_cc) || !is_array($dataFields)) {
            return false;
        }

        $class_id = $current_cc["Class_ID"];
        $classTable = 'Message' . $class_id;

        $updateQueryRuArray = array();
        foreach ($dataFields as $dataIndex => $dataField) {
            $varName = "f_{$dataField}_ru";
            global $$varName;
            $varValue = $$varName;
            $varValue = $db->escape($varValue);
            $updateQueryRuArray[] = "`{$dataField}` = '{$varValue}'";
        }
        if ($keywordBase) {
            $keywordVar = "f_{$keywordBase}_ru";
            $keywordRu = nc_transliterate($$keywordVar, true);
            $updateQueryRuArray[] = "`Keyword` = '" . $keywordRu . ($keywordAdd ? "-" . $keywordAdd : "") . "'";
        }
        $updateQueryRu = implode(', ', $updateQueryRuArray);
        $db->query("UPDATE `{$classTable}` SET {$updateQueryRu} WHERE `Message_ID` = '{$messageId}' AND `Language_ID` = '1';");

        $updateQueryEnArray = array();
        foreach ($dataFields as $dataIndex => $dataField) {
            $varName = "f_{$dataField}_en";
            global $$varName;
            $varValue = $$varName;
            $varValue = $db->escape($varValue);
            $updateQueryEnArray[] = "`{$dataField}` = '{$varValue}'";
        }
        if ($keywordBase) {
            $keywordVar = "f_{$keywordBase}_en";
            $keywordEn = nc_transliterate($$keywordVar, true);
            if($keywordEn == $keywordRu) {
                $keywordEn .= '-en';
            }
            $updateQueryEnArray[] = "`Keyword` = '" . $keywordEn . ($keywordAdd ? "-" . $keywordAdd : "") . "'";
        }
        $updateQueryEn = implode(', ', $updateQueryEnArray);
		
		//Добавлена проверка на наличие английской версии - 31.08.2017
		$exists_langEn = $db->get_var("SELECT `Message_ID` FROM `{$classTable}` WHERE `Language_Parent` = '{$messageId}' AND `Language_ID` = '2';");
		if(empty($exists_langEn)){
			self::addCopy($messageId, 2, true);
		}

        $existCols = $db->get_col("SHOW COLUMNS FROM `{$classTable}` WHERE `Field` NOT IN('Message_ID','Subdivision_ID','Sub_Class_ID','Language_ID','Language_Parent','ncSMO_Image','Checked','Priority','Keyword','ncTitle','ncKeywords','ncDescription','ncSMO_Title','ncSMO_Description');");
        $updateLangCopyFields = array();
        foreach ($existCols as $existCol) {
            $updateLangCopyFields[] = "`lang`.`{$existCol}` = `orig`.`{$existCol}`";
        }
        $updateLangCopyData = implode(', ', $updateLangCopyFields);
        $db->query("UPDATE `{$classTable}` `lang` LEFT JOIN `{$classTable}` `orig` ON (`orig`.`Message_ID` = `lang`.`Language_Parent` AND `orig`.`Language_ID` = '1') SET {$updateLangCopyData}, `lang`.`Parent_Message_ID` = '0' WHERE `lang`.`Language_Parent` = '{$messageId}' AND `lang`.`Language_ID` = '2';");

        $db->query("UPDATE `{$classTable}` SET {$updateQueryEn} WHERE `Language_Parent` = '{$messageId}' AND `Language_ID` = '2';");

		
		//Добавлено 09.04.2018
		//Сохраняем данные во вкладке Дополнительно
		ObjectHelper::saveSystemData($class_id,$messageId);
		
		
        return true;
    }

    public static function saveSystemData($class_id = NULL, $main_message_id = NULL){
		if(empty($class_id) || empty($main_message_id)){
			return NULL;
		}
		
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		
		$allLanguage = $db->get_results("SELECT SiteLanguage_ID,SiteLanguage_Name FROM Classificator_SiteLanguage WHERE Checked = 1",ARRAY_A);
		foreach($allLanguage as $arrRow){
			self::saveSystemDataLang($class_id,$main_message_id,$arrRow["SiteLanguage_ID"],$arrRow["SiteLanguage_Name"]);
		}
		
		return true;
	}
	
	public static function saveSystemDataLang($class_id = NULL, $main_message_id = NULL, $lang_id = NULL, $lang_prefix = NULL){
		if(empty($class_id) || empty($main_message_id) || empty($lang_id) || empty($lang_prefix)){
			return NULL;
		}
		
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		
		$arrFields = FieldHelper::getFieldsSystem();
		$arrDataFieldsUpdate = array();
		foreach($arrFields as $field_name){
			$post_value = $_POST["f_".$field_name."_".$lang_prefix];
			if($field_name === "Checked" && empty($post_value)){
				$post_value = "off";
			}
			
			if($field_name === "ncSMO_Image"){
				self::saveImageSMO($class_id,$main_message_id,$lang_id,$lang_prefix);
				continue;
			}
			
			if(isset($post_value)){
				if($post_value === "on"){
					$post_value = 1;
				}
				if($post_value === "off"){
					$post_value = 0;
				}
				$arrDataFieldsUpdate[] = $field_name." = '".$db->escape($post_value)."'";
			}
		}
		
		if($lang_id != 1){
			$db->query("UPDATE Message".(int)$class_id." SET ".implode(",",$arrDataFieldsUpdate)." 
						WHERE Language_ID = '".(int)$lang_id."' AND Language_Parent = '".(int)$main_message_id."'");
		}else{
			$db->query("UPDATE Message".(int)$class_id." SET ".implode(",",$arrDataFieldsUpdate)." 
						WHERE Message_ID = '".(int)$main_message_id."'");
		}
		
		return true;
	}
	
	public static function saveImageSMO($class_id = NULL, $main_message_id = NULL, $lang_id = NULL, $lang_prefix = NULL){
		if(empty($class_id) || empty($main_message_id) || empty($lang_id) || empty($lang_prefix)){
			return NULL;
		}
		
		global $FILES_FOLDER,$sub,$cc;
		
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		
		$is_post_delete_old = $_POST["delete_old_image_smo_".$lang_prefix] === "on" ? true : false;
		$old_file = NULL;
		$file_post = $_FILES["f_ncSMO_Image_".$lang_prefix];
		
		
		
		
		if($is_post_delete_old || !empty($file_post["tmp_name"])){
			if($lang_id != 1){
				$old_file = $db->get_var("SELECT ncSMO_Image FROM Message".$class_id."
										  WHERE Language_ID = '".(int)$lang_id."' AND Language_Parent = '".(int)$main_message_id."'");
			}else{
				$old_file = $db->get_var("SELECT ncSMO_Image FROM Message".$class_id."
										  WHERE Message_ID = '".(int)$main_message_id."'");
			}
		}
		
		if(!empty($old_file)){
			$arrFile = explode(":",$old_file);
			$full_image_path = $FILES_FOLDER.$arrFile[3];
			unlink($full_image_path);
			if($lang_id != 1){
				$db->query("UPDATE Message".$class_id." SET ncSMO_Image = NULL
							WHERE Language_ID = '".(int)$lang_id."' AND Language_Parent = '".(int)$main_message_id."'");
			}else{
				$db->query("UPDATE Message".$class_id." SET ncSMO_Image = NULL
							WHERE Message_ID = '".(int)$main_message_id."'");
			}
		}
		
		if(!empty($file_post["tmp_name"])){
			$arrFile = array(
							"tmp_name" => $file_post["tmp_name"],
							"folder" => "/".$sub."/".$cc."/",
							"name" => $file_post["name"],
			);
			$save_message_id = $main_message_id;
			if($lang_id != 1){
				$save_message_id = (int)$db->get_var("SELECT Message_ID FROM Message".$class_id." 
													  WHERE Language_ID = '".(int)$lang_id."' AND Language_Parent = '".(int)$main_message_id."'");
			}
			$is_save = $nc_core->files->field_save_file($class_id, "ncSMO_Image", $save_message_id, $arrFile);
		}
		
		return true;
	}
}