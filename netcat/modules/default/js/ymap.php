<script src="https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU&onload=initmap" type="text/javascript"></script>
<script src="/netcat/modules/default/js/location-tool.js" type="text/javascript"></script>
<script src="/netcat/modules/default/js/cross-control.js" type="text/javascript"></script>
<script src="/netcat/modules/default/js/geolocation-button.js" type="text/javascript"></script>
<?
	// назначение значений переменным в разных компонентах
	
	//$f_markerPosition = $f_maincoordinates;
	//if (!$f_mapZoom) $f_mapZoom = 11;
	
	$f_markerPosition = ($f_Longitude && $f_Latitude ? $f_Longitude.', '.$f_Latitude : '');
	$f_mapZoom = $f_Zoom;
	if (!$f_mapZoom) $f_mapZoom = 11;
	
?>
<div id="mapscripts"></div>
<style type='text/css'>
	.hideb{display:none!important;}
    #YMapsID {
	width: 100%;
	height: 400px;
	}
	/*.hero-unit p {
	line-height: 20px;
	}*/
	.cross-control {
	/*background: url(center.gif) no-repeat;*/
	position: absolute;
	width: 16px;
	height: 16px;
	}
</style>
<script type='text/javascript'>
	var marklink;

    $(document).bind('modal-loaded',function(){
        // alert('Окно загружено');
	});
	
	function initmap () {
	
	var valarr=[];
		var startval = '<?=($f_ObjectType?$f_ObjectType:"");?>';
		
		$('#ObjectType option').each(function(){
			var optval = $(this).attr('value');
			if(optval) valarr.push(optval);
		});
		// префикс название класса идентификатора
		var varname='var';
		
		if (valarr) var valstr='.'+varname+valarr.join(', .'+varname);
		if (valstr) {
			$(valstr).addClass('hideb');
		}
		if(startval!=''){
			$('.'+varname+startval).removeClass('hideb');
		}
		
		$('#ObjectType').change(function(){
			var valnum = $(this).val();
			$nc(valstr).addClass('hideb');
			if (valnum) {
				$('.'+varname+valnum).removeClass('hideb');
			}
		});
		
		
		// cross-control
		CrossControl.Layout = ymaps.templateLayoutFactory.createClass(
		'<div class="cross-control" style="right:$[options.position.right]px; top:$[options.position.top]px;"></div>'
		);
		
		// geolocation-js
		ymaps.util.augment(GeolocationButton, ymaps.control.Button, {
			/**
				* Метод будет вызван при добавлении кнопки на карту.
				* @function
				* @name GeolocationButton.onAddToMap
				* @param {ymaps.Map} map Карта на которую добавляется кнопка.
				*/
				onAddToMap: function () {
					GeolocationButton.superclass.onAddToMap.apply(this, arguments);
					
					ymaps.option.presetStorage.add('geolocation#icon', {
						iconImageHref: 'man.png',
						iconImageSize: [27, 26],
						iconImageOffset: [-10, -24]
					});
					
					this.hint = new GeolocationButtonHint(this);
					// Обрабатываем клик на кнопке.
					this.events.add('click', this._onBtnClick, this);
				},
				/**
					* Метод будет вызван при удалении кнопки с карты.
					* @function
					* @name GeolocationButton.onRemoveFromMap
					* @param {ymaps.Map} map Карта с которой удаляется кнопка.
					*/
					onRemoveFromMap: function () {
						this.events.remove('click', this._onBtnClick, this);
						this.hint = null;
						ymaps.option.presetStorage.remove('geolocation#icon');
						
						GeolocationButton.superclass.onRemoveFromMap.apply(this, arguments);
					},
					/**
						* Обработчик клика на кнопке.
						* @function
						* @private
						* @name GeolocationButton._onBtnClick
						* @param {ymaps.Event} e Объект события.
						*/
						_onBtnClick: function (e) {
							// Меняем иконку кнопки на прелоадер.
							this.toggleIconImage('loader.gif');
							
							if(navigator.geolocation) {
								// Запрашиваем текущие координаты устройства.
								navigator.geolocation.getCurrentPosition(
								ymaps.util.bind(this._onGeolocationSuccess, this),
								ymaps.util.bind(this._onGeolocationError, this),
								this._options
								);
							}
							else {
								this.handleGeolocationError('Ваш броузер не поддерживает GeolocationAPI.');
							}
						},
						
						/**
							* Обработчик успешного завершения геолокации.
							* @function
							* @private
							* @name GeolocationButton._onGeolocationSuccess
							* @param {Object} position Объект, описывающий текущее местоположение.
							*/
							_onGeolocationSuccess: function (position) {
								this.handleGeolocationResult(position);
								// Меняем иконку кнопки обратно
								this.toggleIconImage('wifi.png');
							},
							/**
								* Обработчик ошибки геолокации.
								* @function
								* @name GeolocationButton._onGeolocationError
								* @param {Object} error Описание причины ошибки.
								*/
								_onGeolocationError: function (error) {
									this.handleGeolocationError('Точное местоположение определить не удалось.');
									// Меняем иконку кнопки обратно.
									this.toggleIconImage('wifi.png');
									
									if(console) {
										console.warn('GeolocationError: ' + GeolocationButton.ERRORS[error.code - 1]);
									}
								},
								/**
									* Обработка ошибки геолокации.
									* @function
									* @name GeolocationButton.handleGeolocationError
									* @param {Object|String} err Описание ошибки.
									*/
									handleGeolocationError: function (err) {
										this.hint
										.show(err.toString())
										.hide(2000);
									},
									/**
										* Меняет иконку кнопки.
										* @function
										* @name GeolocationButton.toggleIconImage
										* @param {String} image Путь до изображения.
										*/
										toggleIconImage: function (image) {
											this.data.set('image', image);
										},
										/**
											* Обработка результата геолокации.
											* @function
											* @name GeolocationButton.handleGeolocationResult
											* @param {Object} position Результат геолокации.
											*/
											handleGeolocationResult: function (position) {
												var location = [position.coords.latitude, position.coords.longitude],
												accuracy = position.coords.accuracy,
												map = this.getMap(),
												options = this._options,
												placemark = this._placemark,
												circle = this._circle;
												
												// Смена центра карты (если нужно)
												if(!options.noCentering) {
													map.setCenter(location, 15, {
														checkZoomRange: true
													});
												}
												
												// Установка метки по координатам местоположения (если нужно).
												if(!options.noPlacemark) {
													// Удаляем старую метку.
													if(placemark) {
														map.geoObjects.remove(placemark);
													}
													this._placemark = placemark = new ymaps.Placemark(location, {}, { preset: 'geolocation#icon' });
													map.geoObjects.add(placemark);
													// Показываем адрес местоположения в хинте метки.
													this.getLocationInfo(placemark);
												}
												
												// Показываем точность определения местоположения (если нужно).
												if(!options.noAccuracy) {
													// Удаляем старую точность.
													if(circle) {
														map.geoObjects.remove(circle);
													}
													this._circle = circle = new ymaps.Circle([location, accuracy], {}, { opacity: 0.5 });
													map.geoObjects.add(circle);
												}
											},
											/**
												* Получение адреса по координатам метки.
												* @function
												* @name GeolocationButton.getLocationInfo
												* @param {ymaps.Placemark} point Метка для которой ищем адрес.
												*/
												getLocationInfo: function (point) {
													ymaps.geocode(point.geometry.getCoordinates())
													.then(function (res) {
														var result = res.geoObjects.get(0);
														
														if(result) {
															point.properties.set('hintContent', result.properties.get('name'));
														}
													});
												}
											});
											// #geolocation-js
											
											var myMap = new ymaps.Map('YMapsID', {
												center: [<?=($f_markerPosition && strpos($f_markerPosition,",")!==false ?$f_markerPosition:"55.753994, 37.622093");?>],
												zoom: <?=($f_mapZoom?$f_mapZoom:"9");?>,
												behaviors: ['default', 'scrollZoom']
											}),
											// Создание кнопки определения местоположения
											geolocationButton = new GeolocationButton({
												data: {
													// image: 'wifi.png',
													title: 'Определить местоположение'
												},
												geolocationOptions: {
													enableHighAccuracy: true // Режим получения наиболее точных данных
												}
												}, {
												// Зададим опции для кнопки.
												selectOnClick: false
											});
											
											myMap.controls
											.add('mapTools')
											.add(new CrossControl)
											.add(geolocationButton, { top: 5, left: 100 })
											.add('zoomControl')
											.add('typeSelector', { top: 5, right: 5 })
											.add(new ymaps.control.SearchControl({ noPlacemark: true }), { top: 5, left: 200 });
										
											new LocationTool(myMap);
											
											$('#findOnMap').unbind('click');
											$(document).on('click', '#findOnMap', function(e){
												var address = $('#find_address').val();
												if (address.length == 0){
													return;
												}
												ymaps.geocode(address).then( 
												  function(res) {
													  $('#markerPosition').val(res.geoObjects.get(0).geometry.getCoordinates()[0]+', '+res.geoObjects.get(0).geometry.getCoordinates()[1]);
													  myMap.setCenter(res.geoObjects.get(0).geometry.getCoordinates(), 9);
													  myPlacemark.geometry.setCoordinates(res.geoObjects.get(0).geometry.getCoordinates());
													  myPlacemark.events.fire('drag');
													  myMap.setZoom(14, {duration: 1000});
												  } 
												); 
											});
											
											<?/*if(!$f_markerPosition && $current_cc['Class_ID']==2007){?>
											var myGeocoder3 = ymaps.geocode(
												'<?=strip_tags($f_Address);?>', {
													boundedBy: myMap.getBounds(),
													strictBounds: false,
													results: 1
												}
											);
											
											myGeocoder3.then(
												function (res) {
													if (res.geoObjects.getLength()) {
														var point = res.geoObjects.get(0);
														marklink.geometry.setCoordinates(point.geometry.getCoordinates());
														myMap.setBounds(myMap.geoObjects.getBounds());
														myMap.setZoom(15);
														$('#markerPosition').val(marklink.geometry.getCoordinates());
														// $('#mapZoom').val();
														// $('#mapCenter').val();
													}
												},
												function (error) {
													alert("Возникла ошибка при определении адреса: " + error.message);
												}
											);
											
											// alert(marklink.geometry.getCoordinates());
											
											// for(property in marklink) { 
											//   alert(property + ": " + marklink[property]); 
											//} 
											<?}*/?>
										
											
										}
									</script>									