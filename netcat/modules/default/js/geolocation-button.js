/**
 * Класс кнопки определения местоположения пользователя.
 * с помощью Geolocation API.
 * @see http://www.w3.org/TR/geolocation-API/
 * @class
 * @name GeolocationButton
 * @param {Object} params Данные для кнопки и параметры к Geolocation API.
 * @param {Object} options Опции кнопки.
 */
function GeolocationButton(params, options) {
    GeolocationButton.superclass.constructor.call(this, params, options);

    // Расширяем опции по умолчанию теми, что передали в конструкторе.
    this._options = ymaps.util.extend({
        // Не центрировать карту.
        noCentering: false,
        // Не ставить метку.
        noPlacemark: false,
        // Не показывать точность определения местоположения.
        noAccuracy: false,
        // Режим получения наиболее точных данных.
        enableHighAccuracy: true,
        // Максимальное время ожидания ответа (в миллисекундах).
        timeout: 10000,
        // Максимальное время жизни полученных данных (в миллисекундах).
        maximumAge: 1000
    }, params.geolocationOptions);
}

/**
 * Человекопонятное описание кодов ошибок.
 * @static
 */
GeolocationButton.ERRORS = [
    'permission denied',
    'position unavailable',
    'timeout'
];

/**
 * Класс хинта кнопки геолокации, будем использовать для отображения ошибок.
 * @class
 * @name GeolocationButtonHint
 * @param {GeolocationButton} button Экземпляр класса кнопки.
 */
function GeolocationButtonHint(button) {
    this._button = button;
    this._map = button.getMap();
    this._offset = { left: 35, top: -18 };
}
/**
 * Отображает хинт справа от кнопки.
 * @function
 * @name GeolocationButtonHint.show
 * @param {String} text
 * @returns {GeolocationButtonHint}
 */
GeolocationButtonHint.prototype.show = function (text) {
    var map = this._map,
        offset = this._offset,
        buttonPagePixels = this._button.getLayout().getElement().getBoundingClientRect(),
        pagePixels = [buttonPagePixels.left + offset.left, buttonPagePixels.top + offset.top],
        globalPixels = map.converter.pageToGlobal(pagePixels),
        position = map.options.get('projection').fromGlobalPixels(globalPixels, map.getZoom());

    this._hint = map.hint.show(position, text);

    return this;
};
/**
 * Прячет хинт с нужной задержкой.
 * @function
 * @name GeolocationButtonHint.hide
 * @param {Number} timeout Задержка в миллисекундах.
 * @returns {GeolocationButtonHint}
 */
GeolocationButtonHint.prototype.hide = function (timeout) {
    var hint = this._hint;

    if(hint) {
        setTimeout(function () {
            hint.hide();
        }, timeout);
    }

    return this;
};
