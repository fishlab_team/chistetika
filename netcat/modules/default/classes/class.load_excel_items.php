<?php
//Формируем строку по шаблону
function patternColumn($objExcel = NULL, $i = NULL, $pattern_template = NULL, $no_unique = NULL){
	if(!$pattern_template || !$objExcel || !$i){
		return NULL;
	}
	
	preg_match_all("/(%([A-Z])%)/", $pattern_template, $output_array);
	
	$arrColumnName = array();
	
	if(!empty($output_array[2])){
		foreach($output_array[2] as $value){
			$arrColumnName[] = $value;
		}
		if(!$no_unique){
			$arrColumnName = array_unique($arrColumnName);
		}
	}
	
	if(!empty($arrColumnName)){
		$new_pattern_template = "";
		foreach($arrColumnName as $column_name){
			$new_pattern_template = str_replace('%'.$column_name.'%',$objExcel->getCell($column_name.$i)->getValue(),($new_pattern_template ? $new_pattern_template : $pattern_template));
		}
		if(!empty($new_pattern_template)){
			$new_pattern_template = preg_replace('/,{1,}/',',',$new_pattern_template);
			return $new_pattern_template;
		}
	}
	
	return NULL;
}


class loadExcelItems{
	
	public $file_path = NULL;
	
	
	protected $arrTextSteps = array();
	protected $lang_name = NULL;
	protected $lang_id = NULL;
	protected $objPHPExcel = NULL;
	protected $arrDataForFields = array();
	protected $arrDataBingToTable = array();
	protected $arrParentItems = array();
	protected $arrCollectionData = array();
	
	protected $arrItemsToChecked = array();
	protected $arrItemsToUpdateImages = array();
	protected $is_download_images = false;
	
	protected $arrListIndexToTable = array(
		'NOMENCLATURE_CHISTETIKA' => array(
			'table' => 'Message31',
			'sub' => 7,
			'cc' => 5,
			'class_id' => 31,
		),
		'Коллекции' => array(
			'table' => 'Message160',
			'sub' => 7,
			'cc' => 6,
			'class_id' => 160,
		),
		'Тип дизайна' => array(
			'table' => 'Message162',
			'sub' => 7,
			'cc' => 7,
			'class_id' => 162,
		),
		'Тип ткани' => array(
			'table' => 'Message164',
			'sub' => 7,
			'cc' => 8,
			'class_id' => 164,
		),
		'Тип товара' => array(
			'table' => 'Message243',
			'sub' => 7,
			'cc' => 79,
			'class_id' => 243,
		),
		'Область применения' => array(
			'table' => 'Message168',
			'sub' => 7,
			'cc' => 9,
			'class_id' => 168,
		),
		'Цвет' => array(
			'table' => 'Message172',
			'sub' => 7,
			'cc' => 10,
			'class_id' => 172,
		),
		'Технологии' => array(
			'table' => 'Message174',
			'sub' => 7,
			'cc' => 11,
			'class_id' => 174,
		),
		'Уход' => array(
			'table' => 'Message176',
			'sub' => 7,
			'cc' => 12,
			'class_id' => 176,
		),
		'Направление в рулоне' => array(
			'table' => 'Message178',
			'sub' => 7,
			'cc' => 13,
			'class_id' => 178,
		),
	);
	protected $arrFieldsIsToInt = array(
		'isNew',
		'isHit',
		'isSale',
		'isContractFabric',
		'isUnderOrder',
		'OnRequest',
		'Weight',
		'StockUnits',
		'OrderMinQuantity',
		'Available',
		'Yandex',
		'ID',
	);
	protected $arrFieldsIsToFloat = array(
		'Price',
	);
	protected $arrFieldsIsToDate = array(
		'DateExpectedOfReceipt',
	);
	protected $arrPriorityList = array(
		'Коллекции',
		'Тип дизайна',
		'Тип ткани',
		'Область применения',
		'Цвет',
		'Технологии',
		'Уход',
		'Направление в рулоне',
		'NOMENCLATURE_CHISTETIKA',
	);
	/*
		Максимальное кол-во полей для каждого листа
		Значение нужно писать как необходимое кол-во + 1
		Т.е. если полей 5, то надо писать 6
	*/
	protected $arrMaxColumnList = array(
		'Коллекции' => 6,
		'Уход' => 4,
		'NOMENCLATURE_CHISTETIKA' => 31,
	);
	
	
	
	function __construct($file_path = NULL, $is_download_images = NULL){
		if(!empty($is_download_images)){
			$this->setDownloadImages();
		}
		
		$this->setFile($file_path)
			 ->getDefaultFilePath()
			 ->checkFile()
			 ->setTextStep('Шаг 1: проверка файла на существования - Выполнено');
		
		$this->openFile()
			 ->eachList();
	}
	
	/*ШАГ 1 РАБОТА С ФАЙЛОМ*/
	protected function getDefaultFilePath(){
		global $FILES_FOLDER;
		
		if(empty($this->file_path) || !file_exists($this->file_path)){
			$this->file_path = $FILES_FOLDER.'loadExcel/items.xlsx';
		}
		
		return $this;
	}
	
	protected function checkFile(){
		if(empty($this->file_path) || !file_exists($this->file_path)){
			//throw new UnderflowException('Файл не найден!');
		}
		
		return $this;
	}
	
	function setFile($file_path = NULL){
		$this->file_path = $file_path;
		
		return $this;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*ШАГ 3 Открытие файла*/
	function openFile(){
		$this->objPHPExcel = PHPExcel_IOFactory::load($this->file_path);
		
		$this->setTextStep('Шаг 2: открытие файла - Выполнено');
		
		return $this;
	}
	
	protected function setDataForFields($active_list_name = NULL){
		switch($active_list_name){
			case 'NOMENCLATURE_CHISTETIKA':{
				$this->arrDataForFields['collection'] = $this->getDataForField('Message160', array('Message_ID', 'Name'));
				$this->arrDataForFields['design'] = $this->getDataForField('Message162', array('Message_ID', 'Name'));
				$this->arrDataForFields['cloth'] = $this->getDataForField('Message164', array('Message_ID', 'Name'));
				$this->arrDataForFields['direction_roll'] = $this->getDataForField('Message178', array('Message_ID', 'Name'));
				$this->arrDataForFields['countries'] = $this->getDataForField('Classificator_Countries', array('Countries_ID', 'Countries_Name'), 2);
				$this->arrDataForFields['direction_roll'] = $this->getDataForField('Message178', array('Message_ID', 'Name'));
				//Сразу за раз устанавилваем и товары и похожие товары, чтобы 2 раза запрос не делать
				//$this->arrDataForFields['similar_cloth'] = $this->arrDataForFields['items'] = $this->getDataForField('Message31', array('Message_ID', 'Name'));
				$this->arrDataForFields['color'] = $this->getDataForField('Message172', array('Message_ID', 'Name'));
				$this->arrDataForFields['app_area'] = $this->getDataForField('Message168', array('Message_ID', 'Name'));
				$this->arrDataForFields['technology'] = $this->getDataForField('Message174', array('Message_ID', 'Name'));
				$this->arrDataForFields['care'] = $this->getDataForField('Message176', array('Message_ID', 'Name'));
				$this->arrDataForFields['type_item'] = $this->getDataForField('Message243', array('Message_ID', 'Name'));
				break;
			}
		}
		
		return true;
	}
	
	protected function getDataForField($table_name = NULL, $arrFields = array(), $type_table = NULL){
		if(empty($table_name)){
			return NULL;
		}
		
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		if(empty($type_table)){
			$type_table = 1;
		}
		$query_select = !empty($arrFields) ? implode(',', $arrFields) : '*';
		$query_where = $type_table === 1 ? ' AND Language_Parent IS NULL' : NULL;
		
		$result = $db->get_results("SELECT ".$query_select." FROM ".$table_name."
									WHERE Checked = 1".$query_where, ARRAY_A);
		
		return $result;
	}
	
	public function eachList(){
		$objPHPExcel = $this->objPHPExcel;
		
		$this->setTextStep('Шаг 3: чтение файла - Выполнено');
		
		$arrListNames = $objPHPExcel->getSheetNames();
		foreach($this->arrPriorityList as $list_name){
			if(in_array($list_name, $arrListNames)){
				$this->setTextStep('Шаг 4: чтение данных из листа "'.$list_name.'" - Выполнено');
				if($list_name === 'NOMENCLATURE_CHISTETIKA'){
					$this->eachListData($list_name);
				}else{
					$this->eachListDataOther($list_name);
				}
			}
		}
		
		$this->setTextStep('Шаг 5: импорт завершен - Выполнено');
		
		return $this;
	}
	
	protected function eachListDataOther($active_list_name = NULL){
		$objPHPExcel = $this->objPHPExcel;
		$this->setDataForFields($active_list_name);
		
		//Получаем лист
		$sheet = $objPHPExcel->getSheetByName($active_list_name);
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		$arrData = $sheet->toArray();
		$arrListData = $this->arrListIndexToTable[$active_list_name];
		$table = $arrListData['table'];
		$allDataDB = $db->get_results("SELECT Message_ID as ID,Name as Name,LOWER(Name) as NameLower FROM ".$table." 
									   WHERE Language_ID = 1
									   ORDER BY Name", ARRAY_A);
		
		$arrFields = array();
		$arrFieldsLanguage = array();
		$arrCheckedIds = array();
		foreach($arrData as $key => $arrRow){
			$row_i = 1;
			$arrInsertData = array();
			$arrInsertDataLanguage = array();
			$item_name = NULL;
			foreach($arrRow as $k => $value){
				//Если полей вдруг больше, чем положено
				if(!empty($this->arrMaxColumnList[$active_list_name]) && $this->arrMaxColumnList[$active_list_name] === $row_i){
					break;
				}
				
				$arrExplodeField = explode('_', $arrData[0][$k]);
				$field_name = trim($arrExplodeField[0]);
				$field_lang_name = mb_strtolower(trim($arrExplodeField[1]));
				if($field_name === 'ID'){
					$row_i++;
					continue;
				}
				
				//Тут поля
				if($key === 0){
					if($field_lang_name === 'en'){
						$arrFieldsLanguage[] = $field_name;
					}else{
						$arrFields[] = $field_name;
					}
				}else{
					$value = trim(str_replace(array('"', "'"), '', $value));
					if($field_name === 'Name' && empty($field_lang_name)){
						$item_name = $value;
					}
					if(empty($value) && $value !== 0){
						$value = "NULL";
					}else{
						$value = "'".$db->escape($value)."'";
					}
					
					if($field_lang_name === 'en'){
						$arrInsertDataLanguage[] = $value;
					}else{
						$arrInsertData[] = $value;
					}
				}
				$row_i++;
			}
			
			if(!empty($arrInsertData)){
				$key_search = array_search(mb_strtolower($item_name), array_column($allDataDB, 'NameLower'), true);
				if($key_search !== false){
					$main_id = $allDataDB[$key_search]['ID'];
					$arrCheckedIds[] = $main_id;
					$arrUpdate = array();
					foreach($arrFields as $kf => $f_name){
						if($f_name === 'Name'){
							$arrUpdate[] = $f_name.' = '.$arrInsertData[$kf];
						}else{
							$arrUpdate[] = $f_name.' = '.(!empty($arrInsertData[$kf]) && $arrInsertData[$kf] !== 'NULL' ? $arrInsertData[$kf] : $f_name);
						}
					}
					$arrUpdate[] = 'Checked = 1';
					$db->query("UPDATE ".$table." SET ".implode(', ', $arrUpdate)." WHERE Message_ID = '".$main_id."'");
					//$db->debug();
					//echo "UPDATE ".$table." SET ".implode(', ', $arrUpdate)." WHERE Message_ID = '".$main_id."'";
					//echo '<br/>';
					
					//Английская
					$main_lang_id = $db->get_var("SELECT Message_ID FROM ".$table." 
												  WHERE Language_Parent = '".$main_id."' 
												  ORDER BY Message_ID
												  LIMIT 1");
					if(!empty($main_lang_id)){
						$arrUpdateLang = array();
						unset($kf, $f_name);
						foreach($arrFieldsLanguage as $kf => $f_name){
							if($f_name === 'Name'){
								$arrUpdateLang[] = $f_name.' = '.$arrInsertDataLanguage[$kf];
							}else{
								$arrUpdateLang[] = $f_name.' = '.(!empty($arrInsertDataLanguage[$kf]) && $arrInsertDataLanguage[$kf] !== 'NULL' ? $arrInsertDataLanguage[$kf] : $f_name);
							}
						}
						$arrUpdateLang[] = 'Checked = 1';
						$db->query("UPDATE ".$table." SET ".implode(', ', $arrUpdateLang)." WHERE Message_ID = '".$main_lang_id."'");
						//$db->debug();
						//echo "UPDATE ".$table." SET ".implode(', ', $arrUpdateLang)." WHERE Message_ID = '".$main_lang_id."'";
						//echo '<br/>';
					}
				}else{
					$arrFieldsInsert = $arrFields;
					$arrFieldsInsert[] = 'Subdivision_ID';
					$arrFieldsInsert[] = 'Sub_Class_ID';
					$arrFieldsInsert[] = 'Checked';
					$arrFieldsInsert[] = 'Language_ID';
					$arrFieldsInsert[] = 'Language_Parent';
					
					$arrFieldsInsertLanguage = $arrFieldsLanguage;
					$arrFieldsInsertLanguage[] = 'Subdivision_ID';
					$arrFieldsInsertLanguage[] = 'Sub_Class_ID';
					$arrFieldsInsertLanguage[] = 'Checked';
					$arrFieldsInsertLanguage[] = 'Language_ID';
					$arrFieldsInsertLanguage[] = 'Language_Parent';
					
					
					
					$arrInsertData[] = $arrListData['sub'];
					$arrInsertData[] = $arrListData['cc'];
					$arrInsertData[] = 1;
					$arrInsertData[] = 1;
					$arrInsertData[] = 'NULL';
					
					$insert_data = '('.implode(',', $arrInsertData).')';
					$db->query("INSERT INTO ".$table." (".implode(',', $arrFieldsInsert).") VALUES ".$insert_data);
					//$db->debug();
					//echo "INSERT INTO ".$table." (".implode(',', $arrFieldsInsert).") VALUES ".$insert_data;
					//echo '<br/>';
					$main_id = $db->insert_id;
					$arrInsertDataLanguage[] = 0;
					$arrInsertDataLanguage[] = 0;
					$arrInsertDataLanguage[] = 1;
					$arrInsertDataLanguage[] = 2;
					$arrInsertDataLanguage[] = $main_id;
					
					$insert_data_language = '('.implode(',', $arrInsertDataLanguage).')';
					$db->query("INSERT INTO ".$table." (".implode(',', $arrFieldsInsertLanguage).") VALUES ".$insert_data_language);
					//$db->debug();
					//echo "INSERT INTO ".$table." (".implode(',', $arrFieldsInsertLanguage).") VALUES ".$insert_data_language;
					//echo '<br/>';
				}
			}
		}
		
		//Выключаем объекты, которые есть в базе, но отсутствует в файле
		$arrDiff = array_diff(array_column($allDataDB, 'ID'), $arrCheckedIds);
		if(!empty($arrDiff)){
			foreach($arrDiff as $id){
				$db->query("UPDATE ".$table." SET Checked = 0 
							WHERE Message_ID = '".$id."' OR Language_Parent = '".$id."'");
			}
		}
	}
	
	protected function eachListData($active_list_name = NULL){
		$objPHPExcel = $this->objPHPExcel;
		$this->setDataForFields($active_list_name);
		
		//Получаем лист
		$sheet = $objPHPExcel->getSheetByName($active_list_name);
		
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		
		$arrSettings = $db->get_row("SELECT NameTemplate,WordTemplate,DescTemplate FROM Message".Constants::$manualImportTableId."",ARRAY_A);
		$name_template = $arrSettings["NameTemplate"];
		$keywords_template = $arrSettings["WordTemplate"];
		$desc_template = $arrSettings["DescTemplate"];
		
		$arrData = $sheet->toArray();
		$arrInsertField = array();
		$arrFieldsLanguage = array();
		$arrDataInsert = array();
		$arrDataInsertLanguage = array();
		$old_main_name = NULL;
		
		//Получаем все необходимые поля
		$arrFields = array();
		$arrFieldsAll = $arrData[0];
		$row_i_field = 1;
		foreach($arrFieldsAll as $kf => $fv){
			//Если полей вдруг больше, чем положено
			if(!empty($this->arrMaxColumnList[$active_list_name]) && $this->arrMaxColumnList[$active_list_name] === $row_i_field){
				break;
			}
			$arrFields[$kf] = $fv;
			
			$row_i_field++;
		}
		
		unset($arrData[0]);
		//Получаем ключ, где храниться Имя товара
		$key_name = NULL;
		//Получаем ключ, где храниться ID Товара
		$key_id = NULL;
		//Получаем ключ, где храниться Название Коллекции
		$key_collection_name = NULL;
		$row_i_field = 1;
		foreach($arrFields as $f_key => $f_name){
			if($f_name === 'Name'){
				$key_name = $f_key;
			}
			if($f_name === 'ID'){
				$key_id = $f_key;
			}
			if($f_name === 'Collection'){
				$key_collection_name = $f_key;
			}
			//Меняем название поля, т.к. в базе у нас оно подругому названо
			if($f_name === 'OnRequest'){
				$f_name = 'isUnderOrder';
			}
			if($f_name === 'Serificates'){
				continue;
			}
			if($f_name === 'Easyclean'){
				continue;
			}
			
			//Если полей вдруг больше, чем положено
			if(!empty($this->arrMaxColumnList[$active_list_name]) && $this->arrMaxColumnList[$active_list_name] === $row_i){
				break;
			}
			
			$arrExplodeField = explode('_', $f_name);
			$field_lang_name = mb_strtolower(trim($arrExplodeField[1]));
			if($field_lang_name === 'en'){
				$arrFieldsLanguage[] = trim($arrExplodeField[0]);
			}
			
			$row_i_field++;
		}
		unset($field_lang_name,$arrExplodeField);
		
		$arrDataNoSortBy = $arrData;
		usort($arrData, function($a, $b) use ($key_name){
			return $a[$key_name] <=> $b[$key_name];
		});
		
		$arrCollectionNames = array();
		foreach($arrData as $key => $arrRow){
			//в 0 храняться название полей
			
			if(empty($arrRow)){
				continue;
			}
			$arrDataRow = array();
			$arrDataRowLanguage = array();
			$item_id = NULL;
			$arrSeoFields = array();
			$get_pat_name = NULL;
			$row_i = 1;
			foreach($arrRow as $k => $value){
				$field_name = $arrFields[$k];
				$value = trim($value);
				
				//Если пустое поле, значит что-то лишнее
				if(empty($field_name)){
					continue;
				}
				
				//Если полей вдруг больше, чем положено
				if(!empty($this->arrMaxColumnList[$active_list_name]) && $this->arrMaxColumnList[$active_list_name] === $row_i){
					break;
				}
				
				if(in_array($field_name, $this->arrFieldsIsToInt)){
					$value = (int)$value;
				}
				if(in_array($field_name, $this->arrFieldsIsToFloat)){
					$value = (float)$value;
				}
				if(in_array($field_name, $this->arrFieldsIsToDate)){
					$is_time = strtotime($value);
					if(!empty($is_time)){
						$value = date('Y-m-d H:i:s', $is_time);
					}
				}
				
				if($field_name === "ID"){
					$item_id = $value;
				}
				
				if($field_name === 'Name'){
					$item_name = str_replace(array('"', "'"), '', $value);
					$value = $item_name;
				}
				
				if($field_name === 'Yandex'){
					if(!empty($value)){
						$this->arrItemsToUpdateImages[] = $arrRow[$key_id];
					}
					continue;
				}
				if($field_name === 'Available'){
					$this->arrItemsToChecked[$arrRow[$key_id]] = $value;
					continue;
				}
				if($field_name === 'Easyclean'){
					continue;
				}
				if($field_name === 'ItemTypeIcon'){
					if(mb_strtolower($value) === 'easy clean'){
						$value = 1;
					}else if(mb_strtolower($value) === 'fireretardant'){
						$value = 2;
					}else{
						$value = NULL;
					}
				}
				
				//Поля для которые требуется связь с другим объектом или множественная поля или классификаторы
				if($field_name === 'Collection'){
					$row_key_name = str_replace(array('"', "'"), '', $arrRow[$key_name]);
					if(empty($arrCollectionNames[$value]) && !empty($value)){
						$arrCollectionNames[$value] = $row_key_name;
						$old_main_name = $row_key_name;
					}else{
						$this->arrParentItems[$old_main_name][] = $row_key_name;
					}
					$value = str_replace(array('"', "'"), '', $value);
					$value = $this->getDataFielByBindObject('collection', $value, 'Name', 'Message_ID', true);
				}
				if($field_name === 'CollectionDescription'){
					$collection_name = $arrRow[$key_collection_name];
					$this->arrCollectionData[$collection_name]['name'] = $collection_name;
					$this->arrCollectionData[$collection_name]['desc'] = $value;
					if(empty($this->arrCollectionData[$collection_name]['cert'])){
						$this->arrCollectionData[$collection_name]['cert'] = array();
					}
					continue;
				}
				if($field_name === 'Serificates'){
					$collection_name = $arrRow[$key_collection_name];
					$arrValues = explode(';', $value);
					$this->arrCollectionData[$collection_name]['name'] = $collection_name;
					$this->arrCollectionData[$collection_name]['cert'] = $arrValues;
					continue;
				}
				if($field_name === 'Design'){
					$value = $this->getDataFielByBindObject('design', $value, 'Name', 'Message_ID');
				}
				if($field_name === 'Cloth'){
					$value = $this->getDataFielByBindObject('cloth', $value, 'Name', 'Message_ID');
				}
				if($field_name === 'DirectionRoll'){
					$value = $this->getDataFielByBindObject('direction_roll', $value, 'Name', 'Message_ID');
				}
				if($field_name === 'CountryVendor'){
					$value = $this->getDataFielByBindObject('countries', $value, 'Countries_Name', 'Countries_ID');
				}
				if($field_name === 'SimilarCloth'){
					$this->saveBindTable('similar_cloth', $value, $item_name, 'Message31', 'Name', 'Message182', 'Item_Main_ID', 'Item_Parent_ID');
					continue;
				}
				if($field_name === 'TypeItem'){
					$arrResultIds = $this->getDataFielByMoreObject('type_item', $value, 'Name', 'Message_ID');
					$value = $this->getValueMoreObjectToSaveDB($arrResultIds);
				}
				if($field_name === 'Color'){
					$arrResultIds = $this->getDataFielByMoreObject('color', $value, 'Name', 'Message_ID');
					$value = $this->getValueMoreObjectToSaveDB($arrResultIds);
				}
				if($field_name === 'AppArea'){
					$arrResultIds = $this->getDataFielByMoreObject('app_area', $value, 'Name', 'Message_ID');
					$value = $this->getValueMoreObjectToSaveDB($arrResultIds);
				}
				if($field_name === 'Technologies'){
					$arrResultIds = $this->getDataFielByMoreObject('technology', $value, 'Name', 'Message_ID');
					$value = $this->getValueMoreObjectToSaveDB($arrResultIds);
				}
				if($field_name === 'Care'){
					$arrResultIds = $this->getDataFielByMoreObject('care', $value, 'Name', 'Message_ID');
					$value = $this->getValueMoreObjectToSaveDB($arrResultIds);
				}
				
				
				
				if(empty($value) && $value !== 0){
					$value = "NULL";
				}else{
					$value = "'".$db->escape($value)."'";
				}
				
				$arrExplodeField = explode('_', $field_name);
				$field_lang_name = mb_strtolower(trim($arrExplodeField[1]));
				if($field_lang_name === 'en'){
					$arrDataRowLanguage[] = $value;
				}else{
					$arrDataRow[] = $value;
				}
				
				$row_i++;
			}
			
			if(!empty($arrDataRow) && !empty($arrDataRowLanguage)){
				//Получаем оригинальный номер строки, из массива без сортирвки, иначе если делать свою переменную инкрементную, то данные будут расходиться
				$key_original = array_search($item_id, array_column($arrDataNoSortBy, 0));
				//Т.к. у нас начиются данные со второй строки
				$key_original += 2;
				
				//Ключевое слово
				$get_pat_name = patternColumn($sheet, $key_original, $name_template);
				//$arrSeoFields['Keyword'] = nc_transliterate($get_pat_name, true);
				
				//Ключевые слова
				$get_pat_keyw = patternColumn($sheet, $key_original, $keywords_template, 1);
				$arrSeoFields['ncKeywords'] = $get_pat_keyw ? $get_pat_keyw : '';
				
				//Описание
				$get_pat_desc = patternColumn($sheet, $key_original, $desc_template, 1);
				$arrSeoFields['ncDescription'] = $get_pat_desc ? $get_pat_desc : '';
				
				$arrDataRow[] = $this->arrListIndexToTable[$active_list_name]['sub'];
				$arrDataRow[] = $this->arrListIndexToTable[$active_list_name]['cc'];
				//Создаем кейворд
				$add_keyword = nc_transliterate($get_pat_name, true); 
				//Проверяем кейворд, если такой уже есть, то вернет уже новый 
				//$add_keyword = nc_check_keyword_name(0, $add_keyword, $this->arrListIndexToTable[$active_list_name]['class_id'], $this->arrListIndexToTable[$active_list_name]['sub']);
				//Добавляем кейворд в добавление товара
				$arrDataRow[] = "'".$db->escape($add_keyword)."'";
				$arrDataRow[] = 1;
				$arrDataRow[] = 'NULL';
				
				//Поля SEO
				foreach($arrSeoFields as $value_seo){
					$arrDataRow[] = "'".$db->escape(strip_tags($value_seo))."'";
				}
				
				$arrDataInsert[] = '('.implode(',', $arrDataRow).')';
				
				$arrDataRowLanguage[$key_name] = "'".$db->escape(str_replace(array('"', "'"), '', $arrRow[$key_name]))."'";
				$arrDataRowLanguage[] = 0;
				$arrDataRowLanguage[] = 0;
				$arrDataRowLanguage[] = 'NULL';
				$arrDataRowLanguage[] = 2;
				$arrDataRowLanguage[] = $item_id;
				$arrDataInsertLanguage[] = '('.implode(',', $arrDataRowLanguage).')';
			}
		}
		
		//Поля, которые стоит внест и в базу
		$arrFields[$key_id] = 'Message_ID';
		$arrFields[] = 'Subdivision_ID';
		$arrFields[] = 'Sub_Class_ID';
		$arrFields[] = 'Keyword';
		$arrFields[] = 'Language_ID';
		$arrFields[] = 'Language_Parent';
		
		//Поля SEO
		foreach($arrSeoFields as $field_name_seo => $value_seo){
			$arrFields[] = $field_name_seo;
		}
		
		$arrInsertField = $arrFields;
		
		//Удаляем не нужны поля или меняем название полей
		$arrDeleteFieldInsert = array('SimilarCloth', 'Yandex', 'Available', 'Serificates', 'Easyclean');
		foreach($arrInsertField as $key_field => $insert_field_name){
			if(in_array($insert_field_name, $arrDeleteFieldInsert) || mb_stripos($insert_field_name, '_en') !== false){
				unset($arrInsertField[$key_field]);
			}
			if($insert_field_name === 'OnRequest'){
				$arrInsertField[$key_field] = 'isUnderOrder';
			}
		}
		
		$arrFieldsLanguage[] = 'Name';
		$arrFieldsLanguage[] = 'Subdivision_ID';
		$arrFieldsLanguage[] = 'Sub_Class_ID';
		$arrFieldsLanguage[] = 'Keyword';
		$arrFieldsLanguage[] = 'Language_ID';
		$arrFieldsLanguage[] = 'Language_Parent';
		
		$this->saveTempTable($arrDataInsert, $arrInsertField, $arrDataInsertLanguage, $arrFieldsLanguage, $active_list_name);
	}
	
	/*
		Связь с другим объектом
		$key_data_field - ключ из функции setDataForFields()
	*/
	protected function getDataFielByBindObject($key_data_field = NULL, $value = NULL, $key_search_name = NULL, $get_key_search_name = NULL, $search_is_strict = false){
		if(empty($key_data_field) || empty($value) || empty($key_search_name) || empty($get_key_search_name)){
			return NULL;
		}
		
		$arrDataField = $this->arrDataForFields[$key_data_field];
		$key_search_data = array_search($value, array_column($arrDataField, $key_search_name), $search_is_strict);
		$result = NULL;
		if($key_search_data !== false){
			$result = $arrDataField[$key_search_data][$get_key_search_name];
		}
		
		return $result;
	}
	
	/*
		Сохраняем данные в связанную таблицу
		Например Похожие ткани
	*/
	protected function saveBindTable($key_data_field = NULL, $value = NULL, $name_item = NULL, $table_data = NULL, $table_data_field_search = NULL, $table_save = NULL, $main_field_name = NULL, $parent_field_name = NULL){
		if(empty($key_data_field) || empty($value) || empty($name_item) || empty($table_data) || empty($table_save) || empty($main_field_name) || empty($parent_field_name)){
			return NULL;
		}
		
		$arrClothNames = explode(';', $value);
		$arrClothNames = array_map('trim', $arrClothNames);
		$arrClothNames = array_filter($arrClothNames);
		$arrClothNames = array_values($arrClothNames);
		
		if(!empty($arrClothNames)){
			$this->arrDataBingToTable[$name_item]['data'] = $arrClothNames;
			$this->arrDataBingToTable[$name_item]['table_save'] = $table_save;
			$this->arrDataBingToTable[$name_item]['table_data'] = $table_data;
			$this->arrDataBingToTable[$name_item]['table_data_field'] = $table_data_field_search;
			$this->arrDataBingToTable[$name_item]['main_field_name'] = $main_field_name;
			$this->arrDataBingToTable[$name_item]['parent_field_name'] = $parent_field_name;
		}
		
		return true;
	}
	
	/*
		Множественная связь
	*/
	protected function getDataFielByMoreObject($key_data_field = NULL, $value = NULL, $key_search_name = NULL, $get_key_search_name = NULL){
		if(empty($key_data_field) || empty($value) || empty($key_search_name) || empty($get_key_search_name)){
			return NULL;
		}
		
		$arrValues = explode(';', $value);
		$arrValues = array_map(function($value){
			return trim($value);
		}, $arrValues);
		$arrValues = array_filter($arrValues);
		if(empty($arrValues)){
			return NULL;
		}
		
		$arrColumn = array();
		$arrDataField = $this->arrDataForFields[$key_data_field];
		$arrColumnDataField = array_column($arrDataField, $key_search_name);
		foreach($arrColumnDataField as $kf => $data_value){
			$arrColumn[$kf] = mb_strtolower(trim($data_value));
		}
		$arrResult = array();
		
		foreach($arrValues as $name){
			$name = mb_strtolower(trim($name));
			$key_search = array_search($name, $arrColumn, true);
			if($key_search !== false){
				$arrResult[] = $arrDataField[$key_search][$get_key_search_name];
			}
		}
		
		return $arrResult;
	}
	
	/*
		Множественная связь - формирования для сохранение в базу
	*/
	protected function getValueMoreObjectToSaveDB($arrValues = array()){
		if(empty($arrValues)){
			return NULL;
		}
		
		$result = ','.implode(',', $arrValues).',';
		
		return $result;
	}
	
	protected function saveTempTable($arrDataInsert = array(), $arrInsertField = array(), $arrDataInsertLanguage = array(), $arrFieldsLanguage = array(), $active_list_name = NULL){
		if(empty($arrDataInsert) || empty($arrInsertField) || empty($arrDataInsertLanguage) || empty($arrFieldsLanguage) || empty($active_list_name) ){
			return NULL;
		}
		
		$table_name = $this->arrListIndexToTable[$active_list_name]['table'];
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		
		$table_name_temp = $table_name.'_temp';
		
		$db->query("CREATE TEMPORARY TABLE IF NOT EXISTS ".$table_name_temp." AS (SELECT * FROM ".$table_name." WHERE Language_Parent IS NULL)");
		$db->query("TRUNCATE TABLE ".$table_name_temp);

		$db->query("INSERT INTO ".$table_name_temp." (".implode(',', $arrInsertField).") VALUES ".implode(',', $arrDataInsert));
		$db->query("INSERT INTO ".$table_name_temp." (".implode(',', $arrFieldsLanguage).") VALUES ".implode(',', $arrDataInsertLanguage));
		
		$arrFieldsUpdate = array();
		$arrFieldsNotUpdate = array('Message_ID', 'Language_ID', 'Language_Parent');
		foreach($arrInsertField as $field_name){
			if(!in_array($field_name, $arrFieldsNotUpdate)){
				$arrFieldsUpdate[] = $field_name.' = VALUES('.$field_name.')';
			}
		}
		$arrFieldsUpdate[] = 'Checked = 1';
		
		//Добавляем русские объекты и обновляем старые
		$db->query("INSERT INTO ".$table_name." (".implode(',', $arrInsertField).") 
						SELECT ".implode(',', $arrInsertField)." FROM ".$table_name_temp."
						WHERE Language_Parent IS NULL
					ON DUPLICATE KEY UPDATE 
						".implode(',', $arrFieldsUpdate));
		
		
		//Добавляем английские объекты
		$arrFieldsLanguageWrapSelect = array_map(function($value){
			return 't.'.$value;
		}, $arrFieldsLanguage);
		$db->query("INSERT INTO ".$table_name." (".implode(',', $arrFieldsLanguage).") 
			(
			SELECT ".implode(',', $arrFieldsLanguageWrapSelect)." FROM ".$table_name_temp." as t
				LEFT JOIN ".$table_name." as a ON t.Language_Parent = a.Message_ID
				LEFT JOIN ".$table_name." as lang ON a.Message_ID = lang.Language_Parent
			WHERE lang.Message_ID IS NULL AND t.Language_Parent IS NOT NULL
			)");
		//Обновляем английские объекты
		$arrFieldsUpdateLanguage = array();
		unset($field_name);
		$arrFieldsNotUpdateLanguage = array('Message_ID', 'Language_ID', 'Language_Parent', 'Keyword');
		foreach($arrFieldsLanguage as $field_name){
			if(!in_array($field_name, $arrFieldsNotUpdateLanguage)){
				$arrFieldsUpdateLanguage[] = 'a.'.$field_name.' = t.'.$field_name;
			}
		}
		$arrFieldsUpdateLanguage[] = 'a.Checked = 1';
		$db->query("UPDATE ".$table_name." as a
						INNER JOIN ".$table_name_temp." as t ON a.Language_Parent = t.Language_Parent
					SET ".implode(',', $arrFieldsUpdateLanguage));
		//И обновляем, все остальные поля, кроме английских полей
		unset($field_name);
		$arrFieldsUpdateLanguageOther = array();
		$arrFieldsNotUpdateFromMain = array('Message_ID', 'Subdivision_ID', 'Sub_Class_ID', 'Keyword', 'Language_ID', 'Language_Parent');
		foreach($arrInsertField as $field_name){
			if(!in_array($field_name, $arrFieldsLanguage) && !in_array($field_name, $arrFieldsNotUpdateFromMain)){
				$arrFieldsUpdateLanguageOther[] = 'a.'.$field_name.' = m.'.$field_name;
			}
		}
		if(!empty($arrFieldsUpdateLanguageOther)){
			$db->query("UPDATE ".$table_name." as a
							INNER JOIN ".$table_name." as m ON a.Language_Parent = m.Message_ID
						SET ".implode(',', $arrFieldsUpdateLanguageOther));
		}
		
		
		
		
		
		//Выключаем объекты, которых не нашлось в импорте и только русские объекты
		$db->query("UPDATE ".$table_name." as a
						LEFT JOIN ".$table_name_temp." as t ON a.Message_ID = t.Message_ID AND t.Language_Parent IS NULL
					SET a.Checked = 0
					WHERE t.Message_ID IS NULL AND a.Language_Parent IS NULL");
		//Включаем товары, у которы Available 1 или выключаем если 0
		$arrItemsToChecked = array();
		$arrItemsToNotChecked = array();
		foreach($this->arrItemsToChecked as $i_id => $is_checked){
			if(!empty($is_checked)){
				$arrItemsToChecked[] = $i_id;
			}else{
				$arrItemsToNotChecked[] = $i_id;
			}
		}
		if(!empty($arrItemsToChecked)){
			$db->query("UPDATE ".$table_name." as a SET a.Checked = 1 WHERE a.Message_ID IN (".implode(',', $arrItemsToChecked).")");
		}
		if(!empty($arrItemsToNotChecked)){
			$db->query("UPDATE ".$table_name." as a SET a.Checked = 0 WHERE a.Message_ID IN (".implode(',', $arrItemsToNotChecked).")");
		}
		
		//Удаляем временную таблицу
		$db->query("DROP TABLE ".$table_name_temp);
		
		
		$this->setTextStep('Шаг 4: загрузка данных из листа "'.$active_list_name.'" - Выполнено');
		//Сохраняем связи для связи в другой таблице по имени
		$this->saveBindTableToDB();
		//Делаем привязку вариантов товаров, так же по имени
		$this->saveBindVariantItemsDB($active_list_name);
		//Сохраняеим данные по коллекции из данных пришедших во вкладе Товары
		$this->saveCollectionDataFromItems();
		
		//Обновляем фото
		if(!empty($this->arrItemsToUpdateImages) || !empty($is_download_images)){
			DownloadPhotosFromYaDisk::downloadAllPhotosFromYaDisk($this->arrItemsToUpdateImages);
		}

		//Данная штука выполяется еще в self::loadStockUnits()
		if($table_name === 'Message31'){
            $db->query("UPDATE " . $table_name . " SET Checked = 0 
                        WHERE StockUnits = 0 AND isSale = 1");
        }

		self::loadStockUnits();
		
		
		unlink($this->file_path);
		
		return $this;
	}
	
	protected function saveBindTableToDB(){
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		$arrDataBingToTable = $this->arrDataBingToTable;
		
		if(!empty($arrDataBingToTable)){
			foreach($arrDataBingToTable as $item_name => $arrRow){
				$arrNames = $arrRow['data'];
				$table_data = $arrRow['table_data'];
				$table_data_field = $arrRow['table_data_field'];
				$table_save = $arrRow['table_save'];
				$main_field_name = $arrRow['main_field_name'];
				$parent_field_name = $arrRow['parent_field_name'];
				
				$get_message_id = (int)$db->get_var("SELECT Message_ID FROM ".$table_data." 
													 WHERE LOWER(".$table_data_field.") = '".mb_strtolower(trim($item_name))."' AND Language_ID = 1");
				if(!empty($get_message_id)){
					$arrInsert = array();
					foreach($arrNames as $bind_name){
						$bind_id = (int)$db->get_var("SELECT Message_ID FROM ".$table_data." 
													  WHERE Name = '".$db->escape($bind_name)."' AND Language_ID = 1");
						if(!empty($bind_id)){
							$arrInsert[] = "('".$get_message_id."', '".$bind_id."')";
						}
					}
					$db->query("DELETE FROM ".$table_save." 
								WHERE ".$main_field_name." = '".$get_message_id."'");
					if(!empty($arrInsert)){
						$db->query("INSERT INTO ".$table_save." (".$main_field_name.",".$parent_field_name.") VALUES ".implode(',', $arrInsert));
					}
				}
			}
		}
		
		return $this;
	}
	
	protected function saveBindVariantItemsDB($active_list_name = NULL){
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		$arrParentItems = $this->arrParentItems;
		$table_name = $this->arrListIndexToTable[$active_list_name]['table'];
		
		if(!empty($arrParentItems) && !empty($table_name)){
			foreach($arrParentItems as $main_name => $arrVariantsName){
				$get_main_id = (int)$db->get_var("SELECT Message_ID FROM ".$table_name." 
												  WHERE LOWER(Name) = '".mb_strtolower(trim($db->escape($main_name)))."' AND Language_Parent IS NULL");
				if(!empty($get_main_id)){
					$arrVariantsName = array_map(function($value){
						$nc_core = nc_Core::get_object();
						$db = $nc_core->db;
						
						return "'".$db->escape($value)."'";
					}, $arrVariantsName);
					
					$db->query("UPDATE ".$table_name." SET Parent_Message_ID = '".$get_main_id."' 
								WHERE Name IN (".implode(',', $arrVariantsName).") AND Language_Parent IS NULL");
				}
			}
		}
		
		return $this;
	}
	
	protected function saveCollectionDataFromItems(){
		$arrCollectionData = $this->arrCollectionData;
		
		if(!empty($arrCollectionData)){
			$nc_core = nc_Core::get_object();
			$db = $nc_core->db;
			$allCerts = $this->getDataForField('Message199', array('Message_ID', 'Name'));
			
			$arrCerts = array();
			foreach($arrCollectionData as $arrRow){
				$arrCert = array_filter($arrRow['cert']);
				if(!empty($arrCert)){
					foreach($arrCert as $cert_name){
						$arrCerts[] = $cert_name;
					}
				}
			}
			
			//Добавление сертификатов
			$arrCerts = array_unique($arrCerts);
			if(!empty($arrCerts)){
				unset($cert_name);
				foreach($arrCerts as $cert_name){
					$key_search = array_search($cert_name, array_column($allCerts, 'Name'), true);
					if($key_search === false){
						//Почему сделано в цикле, т.к. данных мало, и + нам нужно знать ID уже добавлено объекта, для английской версии
						$db->query("INSERT INTO Message199 (Subdivision_ID, Sub_Class_ID, Checked, Created, LastUpdated, Name, Language_ID, Language_Parent) 
									VALUES (7, 55, 1, NOW(), NOW(), '".$db->escape($cert_name)."', 1, NULL)");
						$cert_id = (int)$db->insert_id;
						if(!empty($cert_id)){
							$db->query("INSERT INTO Message199 (Subdivision_ID, Sub_Class_ID, Checked, Created, LastUpdated, Name, Language_ID, Language_Parent) 
										VALUES (0, 0, 1, NOW(), NOW(), '".$db->escape($cert_name)."', 2, '".$cert_id."')");
						}
					}
				}
			}
			
			//Заново получаем сертификаты, если вдруг были добавлены новые
			$allCerts = $this->getDataForField('Message199', array('Message_ID', 'Name'));
			//Данные по коллекциям, которые хранятся в базе
			$allCollections = $this->getDataForField('Message160', array('Message_ID', 'Name'));
			unset($arrRow, $arrCert, $cert_name, $key_search);
			foreach($arrCollectionData as $arrRow){
				$collection_name = $arrRow['name'];
				$collection_desc = $arrRow['desc'];
				$arrCert = array_filter($arrRow['cert']);
				$arrCertIds = array();
				foreach($arrCert as $cert_name){
					$key_search = array_search($cert_name, array_column($allCerts, 'Name'), true);
					if($key_search !== false){
						$arrCertIds[] = (int)$allCerts[$key_search]['Message_ID'];
					}
				}
				
				unset($key_search);
				$key_search = array_search($collection_name, array_column($allCollections, 'Name'));
				if($key_search !== false){
					$collection_id = (int)$allCollections[$key_search]['Message_ID'];
					$db->query("UPDATE Message160 
									SET Text = ".(!empty($collection_desc) ? "'".$db->escape($collection_desc)."'" : 'Text').", Certificates = '".(!empty($arrCertIds) ? ','.implode(',', $arrCertIds).',' : NULL)."'
								WHERE Message_ID = '".$collection_id."' OR Language_Parent = '".$collection_id."'");
				}
			}
		}
		
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static function loadStockUnits($file_path_full = NULL){
		global $FILES_FOLDER, $DOCUMENT_ROOT;
		
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		$no_param_file = false;
		if(empty($file_path_full)){
            $no_param_file = true;
			$file_path_full = $FILES_FOLDER.'loadExcel/stock.csv';
		}
		
		if(!file_exists($file_path_full)){
			return NULL;
		}
		
		$h = fopen($file_path_full, 'r');
		$rows = array();
		while($row = fgetcsv($h, 0, ';')){
			$rows[] = $row;
		}
		
		
		
		$allItems = $db->get_results("SELECT Message_ID,isUnderOrder,isSale,Checked FROM Message".Constants::$tkaniTableId." 
									  WHERE Language_ID = 1", ARRAY_A);
		
		$arrFileItemID = array();
		foreach ($rows as $row){
			$id = (int)$row[0];
			$name = (int)$row[1];
			$is_available = (int)$row[2];
			$price = (float)$row[3];
			$time_exp_of_rec = strtotime($row[4]);
			$date_exp_of_rec = NULL;
			if(!empty($time_exp_of_rec)){
				$date_exp_of_rec = date('Y-m-d H:i:s', $time_exp_of_rec);
			}
			$stock = (int)$row[5];
			
			if(empty($stock)){
				$stock = 10000;
			}
			if(empty($is_available) && empty($date_exp_of_rec)){
                $stock = 0;
            }
            if(empty($is_available) && !empty($date_exp_of_rec)){
                $stock = 0;
            }
			
			$key_search = array_search($id, array_column($allItems, 'Message_ID'));
			if($key_search !== false){
                $isSale = (int)$allItems[$key_search]['isSale'];
                $is_available_db = (int)$allItems[$key_search]['Checked'];
                $isUnderOrder = (int)$allItems[$key_search]['isUnderOrder'];
                //Данная штука еще выполняется в saveTempTable перед выполнением self::loadStockUnits()
                if(empty($is_available) && !empty($isSale)){
                    $is_available = 0;
                }else if(!empty($date_exp_of_rec)){
                    $is_available = 1;
                }else if(!empty($is_available_db)){
                    $is_available = 1;
                }else if(!empty($isUnderOrder)){
                    $is_available = 1;
                }else if(empty($is_available_db)){
                    $is_available = 1;
                }
			}else{
                if(!empty($date_exp_of_rec)){
                    $is_available = 1;
                }
            }

			$db->query('update Message'.Constants::$tkaniTableId.' 
							set StockUnits = '.$stock.', 
								Price = '.$price.', 
								Checked = '.$is_available.', 
								DateExpectedOfReceipt = "'.$date_exp_of_rec.'" 
						where Message_ID = "'.$id.'"');
			$arrFileItemID[] = $id;
		}

		//Получаем список всех тканей, которые включены
		if(!empty($arrFileItemID)){
			$allTkani = $db->get_results("SELECT Message_ID,isUnderOrder FROM Message".Constants::$tkaniTableId." 
										  WHERE Checked = 1",ARRAY_A);
			
			foreach($allTkani as $arrVal){
				$item_id = $arrVal["Message_ID"];
				$isUnderOrder = (int)$arrVal["isUnderOrder"];
				//Если в файле не существует текущего Message_ID
				//значит надо ставить наличие в 0 и add 18.03.16 выключать товар
				if(!in_array($item_id,$arrFileItemID) && empty($isUnderOrder)){
					$db->query('update Message'.Constants::$tkaniTableId.' 
									set Checked = 0
								where Message_ID = "'.$item_id.'"');
				}
			}
		}

		if($no_param_file){
		    unlink($file_path_full);
        }
	}
	
	
	
	
	
	public function setDownloadImages(){
		$this->is_download_images = true;
	}
	
	
	
	
	
	
	
	
	
	
	
	public function setTextStep($text = NULL){
		if(!empty($text)){
			$this->arrTextSteps[] = $text;
		}
		
		return $this;
	}
	
	public function getTextSteps(){
		return $this->arrTextSteps;
	}
}
?>