<?php

class Constants{
	public static $photosImportSettings = 240;
	public static $collectionsTableId = 160;
	public static $tkaniTableId = 31;
	public static $personaTableId = 246;
	public static $manualImportTableId = 238;
	
	public static $collectionsImagesBaseFolderName = 'Collection';
	public static $tkaniBaseImagesFolderName = 'Item';
	public static $tkaniRapportsBaseImagesFolderName = 'ItemRapports';
	public static $collectionsMaterialCollectionBaseFolderName = 'MaterialCollection';
	public static $tkaniPersonaBaseImagesFolderName = 'Persona';
}