<?php

class CommonFunctions{
	/**
	 *  из имени файла делает строчку вида
	 *  collection 01 01.jpg:image/jpeg:40502:370/599/collection_01_01.jpg
	 */
	public static function makeDbStringFromFilePath($filePath){
		return FishlabFunctions::makeDbStringFromFilePath($filePath);
	}
	
	
	/**
	 *  Из строчки из БД делаем массив с файлом
	 */
	public static function makeFileArrayFromDbString($dbString){
		return FishlabFunctions::makeFileArrayFromDbString($dbString);
	}
	
	/**
	 *  
	 */
	public static function makePreviewForFile($filePath, $params){
		if (!file_exists($filePath)){
			return $arrSizes;
		}
		$pathInfo = pathinfo($filePath);
		$result = $params;
		foreach($result as $key => $arrSize){
			$previewDir = $pathInfo['dirname'].'/';
			$previewName = $pathInfo['filename'].'_'.$arrSize['width'].'x'.$arrSize['height'].'.'.$pathInfo['extension'];
			/**
			 *  Если надо сделать квадрат из порямоугольника, то:
			 *  вначале уменьшаем изображение так, чтобы оно было по МЕНЬШЕЙ стороне как наш квадрат
			 *  (большая соответвенно будет больше требуемого размера, но это лучше чем если будет меньше требуемого)
			 *  вырезаем из уменьшенного прямоугольника квадрат (обычно из центра)
			 */
			if (isset($arrSize['makeRectangle'])){
				$imageSize = getimagesize($filePath);
				$imageWidth = $imageSize[0];
				$imageHeight = $imageSize[1];
				if (empty($imageWidth)){
					continue;
				}
				$resizedWidth = null;
				$resizedHeight = null;
				if ($imageWidth >= $imageHeight){
					$resizedHeight = $arrSize['height'];
					$resizedWidth = ((int)$resizedHeight*$imageWidth/$imageHeight)+1;
				} elseif ($imageWidth < $imageHeight){
					$resizedWidth = $arrSize['width'];
					$resizedHeight = ((int)$resizedWidth*$imageHeight/$imageWidth)+1;
				}
				
				$newImagePath = nc_ImageTransform::imgResize($filePath, $previewDir.$previewName, $resizedWidth, $resizedHeight, 0, null, 100);
				if (empty($newImagePath)){
					$result[$key]['path'] = $newImagePath;
					continue;
				}
				$resizedImageInfo = getimagesize($newImagePath);
				$resizedImageWidth = $resizedImageInfo[0];
				$resizedImageHeight = $resizedImageInfo[1];
				
				
				if ($imageWidth == $imageHeight){
					$result[$key]['path'] = $newImagePath;
					continue;
				}
				$marginLeft = 0;
				$marginTop = 0;
				switch($arrSize['makeRectangle']){
					case 'center':
						if ($resizedImageWidth > $resizedImageHeight){
							$marginLeft = ((int)($resizedImageWidth - $resizedImageHeight) / 2);
							$marginTop = 0;
						} elseif ($resizedImageWidth < $resizedImageHeight){
							$marginLeft = 0;
							$marginTop = (int)(($resizedImageHeight - $resizedImageWidth)/2);
						}
						break;
						}
				
				switch($resizedImageInfo['mime']){
					case 'image/jpeg':
						$src = ImageCreateFromJpeg($newImagePath);
						$dst = ImageCreateTrueColor($arrSize['width'], $arrSize['height']);
						imagecopyresampled($dst, $src, 0, 0, $marginLeft, $marginTop, $arrSize['width'], $arrSize['height'], $arrSize['width'], $arrSize['height']);
						imageinterlace( $dst, true);
						ImageJpeg($dst, $newImagePath, 100);
						$result[$key]['path'] = $newImagePath;
						break;
					case 'image/png':
						$src = ImageCreateFrompng($newImagePath);
						$dst = ImageCreateTrueColor($arrSize['width'], $arrSize['height']);
						imagecopyresampled($dst, $src, 0, 0, $marginLeft, $marginTop, $arrSize['width'], $arrSize['height'], $arrSize['width'], $arrSize['height']);
						Imagepng($dst, $newImagePath);
						$result[$key]['path'] = $newImagePath;
						break;
					case 'image/gif':
						$src = ImageCreateFromGif($newImagePath);
						$dst = ImageCreateTrueColor($arrSize['width'], $arrSize['height']);
						imagecopyresampled($dst, $src, 0, 0, $marginLeft, $marginTop, $arrSize['width'], $arrSize['height'], $arrSize['width'], $arrSize['height']);
						imagegif($dst, $newImagePath);
						$result[$key]['path'] = $newImagePath;
						break;
					default:
						$result[$key]['path'] = $newImagePath;
						break;
				}
				continue;
				
			} else {
				$result[$key]['path'] = nc_ImageTransform::imgResize($filePath, $previewDir.$previewName, $arrSize['width'], $arrSize['height'], 0, null, 100);
			}
		}
		return $result;
		
	}
	
	public static function priceNumberFormat($number){
		if ((int)$number == $number){
			return number_format($number, 0, '.', ' ');
		} else {
			return number_format($number, 2, '.', ' ');
		}
	}
	
	public static function getPreviewForFile($file, $width, $height){
		if (empty($file)){
			return '';
		}
		$imageName = basename($file);
		$imageFolder = dirname($file);
		$dot = strrpos($imageName, '.');
		$nameOnly = substr($imageName, 0, $dot);
		$extension = substr($imageName, $dot);
		if (file_exists($_SERVER['DOCUMENT_ROOT'].$imageFolder.'/'.$nameOnly.'_'.$width.'x'.$height.$extension)){
			return $imageFolder.'/'.$nameOnly.'_'.$width.'x'.$height.$extension;
		} else {
			return null;
		}
	}
	
	public static function getPreviewNameForFile($file, $width, $height){
		$imageName = basename($file);
		$imageFolder = dirname($file);
		$dot = strrpos($imageName, '.');
		$nameOnly = substr($imageName, 0, $dot);
		$extension = substr($imageName, $dot);
		return $imageFolder.'/'.$nameOnly.'_'.$width.'x'.$height.$extension;
	}
	
	public static function isLastSymbolEnding($str){
		$lastSymbol = mb_substr($str, mb_strlen($str, 'UTF-8')-1, 1);
		if ($lastSymbol == '.' || $lastSymbol == '?' || $lastSymbol == '!'){
			return true;
		} else {
			return false;
		}
	}
}