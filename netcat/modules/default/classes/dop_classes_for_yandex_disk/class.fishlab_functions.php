<?php

class FishlabFunctions{
	public static function makeDbStringFromFilePath($filePath){
		global $FILES_FOLDER;
		if (!$filePath || !file_exists($filePath)){
			return '';
		}
		$pathInfo = pathinfo($filePath);
		$imageMime = image_type_to_mime_type(exif_imagetype($filePath));
		$fileSize = filesize($filePath);
		$dbFilePath = str_ireplace($FILES_FOLDER, '', $filePath);
		$dbString = $pathInfo['basename'].':'.$imageMime.':'.$fileSize.':'.$dbFilePath;
		return $dbString;
	}
	
	
	public static function makeFileArrayFromDbString($dbString){
		$fileArray = array(
			'name' => '',
			'mime' => '',
			'size' => '',
			'path' => '',
		);
		if (empty($dbString)){
			return $fileArray;
		}
		$dbArray = explode(':', $dbString);
		$fileArray['name'] = $dbArray[0];
		$fileArray['mime'] = $dbArray[1];
		$fileArray['size'] = $dbArray[2];
		$fileArray['path'] = $dbArray[3];
		return $fileArray;
	}
	
}