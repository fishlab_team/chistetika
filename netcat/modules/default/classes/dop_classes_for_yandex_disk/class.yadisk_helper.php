<?php

class YaDiskHelper{
	public static function makeDirsPretty($dirs){
		foreach($dirs as $key => $oneDir){
			$dirs[$key] = self::prettifyDirProperties($dirs[$key]);
		}
		return $dirs;
	}
	
	public static function prettifyDirProperties($dir){
		$dir['href'] = rawurldecode($dir['href']);
		return $dir;
	}
}