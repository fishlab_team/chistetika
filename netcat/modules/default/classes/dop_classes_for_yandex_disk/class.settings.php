<?php

class Settings{
	public static function getPreviewSettingsForCollectionParsed(){
		//$collectionSettings = self::getPreviewSettingsFromDb(Constants::$collectionPreviewSettingsSubClassId);
		$collectionSettings = self::getPreviewSettingsFromDb();
		$results = self::makeParsedSettingsForPreview($collectionSettings);
		return $results;
	}
	
	public static function getPreviewSettingsForTkaniParsed(){
		//$tkaniSettings = self::getPreviewSettingsFromDb(Constants::$tkaniPreviewSettingsSubClassId);
		$tkaniSettings = self::getPreviewSettingsFromDb();
		$results = self::makeParsedSettingsForPreview($tkaniSettings);
		return $results;
	}
	
	public static function getPreviewSettingsForRapportsTkaniParsed(){
		//$tkaniSettings = self::getPreviewSettingsFromDb(Constants::$tkaniRapportsPreviewSettingsSubClassId);
		$tkaniSettings = self::getPreviewSettingsFromDb();
		$results = self::makeParsedSettingsForPreview($tkaniSettings);
		return $results;
	}
	
	public static function getPreviewSettingsForPersonaParsed(){
		//$tkaniSettings = self::getPreviewSettingsFromDb(Constants::$divaniPreviewSettingsSubClassId);
		$tkaniSettings = self::getPreviewSettingsFromDb();
		$results = self::makeParsedSettingsForPreview($tkaniSettings);
		return $results;
	}
	
	public static function makeParsedSettingsForPreview($collectionSettings){
		$results = array();
		foreach($collectionSettings as $setting){
			$currentResult = array(
				'width' => $setting['Width'],
				'height' => $setting['Height'],
				'CropImage' => $setting['CropImage'],
			);
			$results[] = $currentResult;
		}
		return $results;
	}
	
	public static function getPreviewSettingsFromDb($subClassId = NULL){
		global $db;
		//$sql = 'select * from Message'.Constants::$previewSettingsTableId.' where Sub_Class_ID = '.$db->escape($subClassId).' and Checked = 1';
		//$results = (array)$db->get_results($sql, ARRAY_A);
		
		$results = array(
			array(
				'Width' => 1900,
				'Height' => 900,
				'CropImage' => 1,
			),
		);
		
		return $results;
	}
}