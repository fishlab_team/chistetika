<?php
class sct{
	
	//Subdivision
	public static function sub($sub_name = NULL){
		if(empty($sub_name)){
			return NULL;
		}
		
		$arrData = array(
					'main' => 2,
					'collection' => 7,
					'item' => 7,
					'easy_clean' => 5,
					'contract_cloth' => 6,
					'catalog' => 7,
					'innovation' => 13,
					'care' => 14,
					'news' => 16,
					'media' => 17,
					'bracing' => 18,
					'app_area' => 15,
					'payment' => 22,
					'partners' => 20,
					'delivery' => 21,
					'contacts' => 23,
					'about' => 19,
					'favorites' => 24,
					'search' => 25,
					'contact_form' => 23,
					'review_bracing' => 18,
					'review_bracing' => 18,
					'site_holding' => 26,
					'persona_images' => 7,
					'privacy_policy' => 27,
					'cert' => 7,
					'lk_personal' => 31,
					'cart' => 8,
					'order' => 9,
					'auth' => 28,
		);
		
		return $arrData[$sub_name];
	}
	
	
	
	//SubClass
	public static function cc($cc_name = NULL){
		if(empty($cc_name)){
			return NULL;
		}
		
		$arrData = array(
					'slider' => 3,
					'main_about' => 4,
					'collection' => 6,
					'item' => 5,
					'favorites' => 56,
					'contact_form' => 59,
					'review_bracing' => 62,
					'care' => 63,
					'media' => 65,
					'site_holding' => 70,
					'persona_images' => 81,
					'privacy_policy' => 75,
					'cert' => 55,
					'app_area' => 68,
					'auth' => 82,
		);
		
		return $arrData[$cc_name];
	}
	
	
	
	//tpl
	public static function tpl($tpl_name = NULL){
		if(empty($tpl_name)){
			return NULL;
		}
		
		$arrData = array(
					'slider' => 155,
					'main_about' => 158,
					'collection_image_photos' => 183,
					'item_image_photos' => 184,
					'item' => 159,
					'favorites_small' => 203,
					'contact_form' => 210,
					'review_bracing_form' => 217,
					'review_bracing_list' => 220,
					'care_ajax_by_filter' => 225,
					'media_fresh_articles' => 230,
					'site_holding_footer' => 237,
					'persona_images_collection' => 248,
					'persona_images_item' => 249,
		);
		
		return $arrData[$tpl_name];
	}
}
?>