<?php
use Yandex\Disk\DiskClient;

class DownloadPhotosFromYaDisk{
	public static function downloadAllPhotosFromYaDisk($arrUpdateIdsTkani = array(), $arrUpdateIdsCollection = array()){
		global $db, $FILES_FOLDER;
		
		$sql = 'select * from Message'.Constants::$photosImportSettings.' where Checked = 1';
		$settings = (array)$db->get_row($sql, ARRAY_A);
		if (empty($settings)){
			return;
		}
		if (empty($settings['PerRun'])){
			$perRun = 10;
		} else {
			$perRun = $settings['PerRun'];
		}
		
		
		$diskClient = new DiskClient($settings['DiskLogin'], $settings['DiskPassword']);
		$diskClient->setServiceScheme(DiskClient::HTTPS_SCHEME);
		

		//Импорт имиджевых фото
		self::importPersonaImages($diskClient, $settings, $perRun);
		
		
		
		
		//Коллекции
		$collectionsPhotos = $diskClient->directoryContents($settings['CollectionsFolder']);
		foreach($collectionsPhotos as $key => $collectionPhoto){
			$collectionsPhotos[$key]['href'] = rawurldecode($collectionPhoto['href']);
		}
		if (empty($settings['LastCollectionId'])){
			$lastCollectionId = 1;
		} else {
			$lastCollectionId = $settings['LastCollectionId'];
		}
		if(empty($arrUpdateIdsCollection)){
			$sql = 'select * from Message'.$db->escape(Constants::$collectionsTableId).' where Message_ID >= '.$lastCollectionId.' AND Language_ID = 1 limit '.$perRun;
		}else{
			$sql = 'select * from Message'.$db->escape(Constants::$collectionsTableId).' where Message_ID IN ('.implode(",",$arrUpdateIdsCollection).')';
		}
		$collections = (array)$db->get_results($sql, ARRAY_A);
		if (count($collections) < $perRun){
			$lastCollectionId = 1;
		}else {
			if (isset($collections[count($collections)-1]['Message_ID'])){
				$lastCollectionId = $collections[count($collections)-1]['Message_ID']+1;
			} else {
				$lastCollectionId += $perRun;
			}
		}
		self::makePreviewForArray($collections, $collectionsPhotos, 'collection', $diskClient);

		
		//Ткани
		$tkaniPhotos = YaDiskHelper::makeDirsPretty($diskClient->directoryContents($settings['TkanImageFolder']));
		if (empty($settings['LastTkanImageId'])){
			$lastTkanImageId = 1;
		} else {
			$lastTkanImageId = $settings['LastTkanImageId'];
		}
		if(empty($arrUpdateIdsTkani)){
			$sql = 'select * from Message'.$db->escape(Constants::$tkaniTableId).' where Message_ID >= '.$lastTkanImageId.' AND Language_ID = 1 limit '.$perRun;
		}else{
			$sql = 'select * from Message'.$db->escape(Constants::$tkaniTableId).' where Message_ID IN ('.implode(",",$arrUpdateIdsTkani).')';
		}
		$tkani = (array)$db->get_results($sql, ARRAY_A);
		if (count($tkani) < $perRun){
			$lastTkanImageId = 1;
		} else {
			if (isset($tkani[count($tkani)-1]['Message_ID'])){
				$lastTkanImageId = $tkani[count($tkani)-1]['Message_ID']+1;
			} else {
				$lastTkanImageId += $perRun;
			}
		}
		self::makePreviewForArray($tkani, $tkaniPhotos, 'tkani', $diskClient);
		
		
		
		
		
		//Раппорты
		$tkaniPhotos = YaDiskHelper::makeDirsPretty($diskClient->directoryContents($settings['RapportsFolder']));
		if (empty($settings['LastRapportId'])){
			$lastRapportId = 1;
		} else {
			$lastRapportId = $settings['LastRapportId'];
		}
		if(empty($arrUpdateIdsTkani)){
			$sql = 'select * from Message'.$db->escape(Constants::$tkaniTableId).' where Message_ID >= '.$lastRapportId.' AND Language_ID = 1 limit '.$perRun;
		}else{
			$sql = 'select * from Message'.$db->escape(Constants::$tkaniTableId).' where Message_ID IN ('.implode(",",$arrUpdateIdsTkani).')';
		}
		$tkani = (array)$db->get_results($sql, ARRAY_A);
		if (count($tkani) < $perRun){
			$lastRapportId = 1;
		} else {
			if (isset($tkani[count($tkani)-1]['Message_ID'])){
				$lastRapportId = $tkani[count($tkani)-1]['Message_ID']+1;
			} else {
				$lastRapportId += $perRun;
			}
		}
		self::makePreviewForArray($tkani, $tkaniPhotos, 'rapports', $diskClient);
		
		
		
		
		
		
		//Файлы для коллекции
		$downloadMaterialCollection = YaDiskHelper::makeDirsPretty($diskClient->directoryContents($settings['DownloadMaterialCollectionFolder']));
		$sql = 'select * from Message'.$db->escape(Constants::$collectionsTableId).' where Message_ID >= 1';
		$downloadMaterialCollection_data = (array)$db->get_results($sql, ARRAY_A);
		self::makePreviewForArray($downloadMaterialCollection_data, $downloadMaterialCollection, 'download_material_collection_pdf', $diskClient);
		self::makePreviewForArray($downloadMaterialCollection_data, $downloadMaterialCollection, 'download_material_collection_xls', $diskClient);


		//Файлы коллекции, отдельно все делаем, т.к. это множественное поле со своими нюансами
        //Поэтому все делаем здесь
        global $DOCUMENT_ROOT;
        $downloadMaterialCollection = YaDiskHelper::makeDirsPretty($diskClient->directoryContents($settings['DownloadMaterialCollectionFolder']));
        unset($downloadMaterialCollection[0]);//Сама папка
        $downloadMaterialCollection = array_values($downloadMaterialCollection);
        $collection_files_field = 786;
        $collection_files_field_name = 'Files';
        $table_collection = $db->escape(Constants::$collectionsTableId);
        $allCollections = $db->get_results("SELECT a.Message_ID as CollectionID,a.Name as CollectionName,LOWER(a.Name) as CollectionNameLower,m.ID as FileID,m.Name as FileName,m.Path as FilePath FROM Message".$table_collection." as a
                                                LEFT JOIN Multifield as m ON a.Message_ID = m.Message_ID AND m.Field_ID = '".$collection_files_field."'
                                            WHERE a.Language_ID = 1
                                            ORDER BY a.Name", ARRAY_A);
        $path_save_file_db = '/netcat_files/multifile/'.$collection_files_field.'/';
        $delimiter = '_';
        $arrFilesExists = array();
        foreach($downloadMaterialCollection as $oneYaDiskElement){
            $displayName = $oneYaDiskElement['displayName'];
            $href = $oneYaDiskElement['href'];
            $lastModified = $oneYaDiskElement['lastModified'];
            $arrPathInfo = pathinfo($displayName);
            $filename = $arrPathInfo['filename'];//Allure
            $basename = $arrPathInfo['basename'];//Allure.pdf
            $arrExpl = explode($delimiter, $filename);

            $file_save_db_name = NULL;
            $file_save_db_name_to_path = NULL;
            $file_save_db_size = $oneYaDiskElement['contentLength'];
            $file_collection_name = NULL;
            $file_collection_id = NULL;
            $file_row_id = NULL;
            if(count($arrExpl) === 1){
                $file_save_db_name = NULL;//Allure
                $file_save_db_name_to_path = $basename;//Allure.pdf
                $file_collection_name = $filename;
            }else{
                //НазваниеКоллекции_ПроизвольноеНазвание_НазваниеВыносимоеВЗаголовок.(jpg|pdf)
                $file_save_db_name = !empty($arrExpl[2]) ? $arrExpl[2] : NULL;//НазваниеВыносимоеВЗаголовок
                $file_save_db_name_to_path = $arrExpl[0].$delimiter.$arrExpl[1].'.'.$arrPathInfo['extension'];//Allure_ПроизвольноеНазвание.pdf
                $file_collection_name = $arrExpl[0];//Allure
            }
            $file_collection_name = mb_strtolower($file_collection_name);
            $file_collection_name = str_ireplace(array("'",' '), array(''), $file_collection_name);
            $is_save_file = false;
            $is_exists_files = false;
            foreach($allCollections as $arrRow){
                $collection_db_name_lower = str_ireplace(array("'",' '), array(''), $arrRow['CollectionNameLower']);
                $FileNameDB = $arrRow['FileName'];//Allure ИЛИ НазваниеВыносимоеВЗаголовок
                $FilePath = $arrRow['FilePath'];//Путь к файлу /netcat_files/multifile/786/1/Alluer.pdf
                $arrPathInfoDB = pathinfo($FilePath);
                //Если название файла совпадает и совпадает имя файла скаченное и название коллекции
                if(
                    mb_strtolower($file_collection_name) === mb_strtolower($collection_db_name_lower) &&
                    //mb_strtolower($FileNameDB) === mb_strtolower($file_save_db_name) &&
                    mb_strtolower($file_save_db_name_to_path) === mb_strtolower($arrPathInfoDB['basename'])
                ){
                    $arrFilesExists[] = $FilePath;
                    $fileMTime = filemtime($DOCUMENT_ROOT.$FilePath);
                    $diskLastModifiedTime = new DateTime($lastModified);
                    $ftpLastModifiedTime = DateTime::createFromFormat('U', $fileMTime);
                    if ($ftpLastModifiedTime <= $diskLastModifiedTime){
                        $is_save_file = true;
                    }
                    $is_exists_files = true;
                    //Запись которую следует обновить
                    $file_row_id = $arrRow['FileID'];
                }
                if(mb_strtolower($file_collection_name) === mb_strtolower($collection_db_name_lower)){
                    $file_collection_id = $arrRow['CollectionID'];
                }
            }

            //Если определилась коллекциия ID
            //Если нашли файл и дата изменения изменилась
            //ИЛИ если файл такой не нашли - НАДО сохранить
            if(
                !empty($file_collection_id)
                &&
                (
                    ($is_save_file && $is_exists_files)
                    ||
                    (!$is_save_file && !$is_exists_files)
                )
            ){
                $path_folder_save = $path_save_file_db.$file_collection_id.'/';
                $full_path_folder_save = $DOCUMENT_ROOT.$path_folder_save;
                //Проверяем создана ли папка с ID Коллекции
                if (!is_dir($full_path_folder_save)){
                    mkdir($full_path_folder_save, 0777, true);
                }
                //Сохраняем файл в папку
                $full_path_save_file = $full_path_folder_save;
                $diskClient->downloadFile($href, $full_path_save_file, $file_save_db_name_to_path);
                //Если нашли такой файл, то данные надо лишь обновить, иначе добавить
                if($is_save_file && $is_exists_files && !empty($file_row_id)){
                    $db->query("UPDATE Multifield
                                  SET Name = '".$db->escape($file_save_db_name)."', Size = '".$db->escape($file_save_db_size)."' 
                                WHERE ID = '".$file_row_id."'");
                }else{
                    $db->query("INSERT INTO Multifield 
                                (Field_ID,Message_ID,Priority,Name,Size,Path)
                                  VALUES 
                                ('".$collection_files_field."','".$file_collection_id."',0,'".$db->escape($file_save_db_name)."','".$db->escape($file_save_db_size)."','".$db->escape($path_folder_save.$file_save_db_name_to_path)."')");
                }
            }
        }
        unset($arrRow);
        $arrDeleteFiles = array();
        foreach($allCollections as $arrRow){
            if(!in_array($arrRow['FilePath'], $arrFilesExists) && !empty($arrRow['FilePath'])){
                $arrDeleteFiles[] = array(
                    'id' => $arrRow['FileID'],
                    'path' => $arrRow['FilePath'],
                );
            }
        }
        if(!empty($arrDeleteFiles)){
            unset($arrRow);
            foreach($arrDeleteFiles as $arrRow){
                $db->query("DELETE FROM Multifield WHERE ID = '".$arrRow['id']."'");
                unlink($DOCUMENT_ROOT.$arrRow['path']);
            }
        }
		
		
		
		
		$sql = 'update Message'.Constants::$photosImportSettings.' set LastCollectionId = '.$lastCollectionId.', LastRapportId = '.$lastRapportId.', LastTkanImageId = '.$lastTkanImageId.', LastPersonaImportPhotoId = '.$settings["LastPersonaImportPhotoId"].' where Message_ID = '.$db->escape($settings['Message_ID']);
		$db->query($sql);
		return;
	}
	
	public static function makePreviewForArray(&$dbArray, $yaDiskArray, $type, $diskClient){
		global $db, $FILES_FOLDER;
		
		if ($type == 'collection'){
			$previewsParams = Settings::getPreviewSettingsForCollectionParsed();
			$tableId = Constants::$collectionsTableId;
			$fieldName = 'Image';
		} elseif ($type == 'tkani') {
			$previewsParams = Settings::getPreviewSettingsForTkaniParsed();
			$tableId = Constants::$tkaniTableId;
			$fieldName = 'Image';
		} elseif ($type == 'rapports'){
			$previewsParams = Settings::getPreviewSettingsForRapportsTkaniParsed();
			$tableId = Constants::$tkaniTableId;
			$fieldName = 'RapportImage';
		}  else if ($type == 'download_material_collection_pdf') {
			$previewsParams = array();
			$tableId = Constants::$collectionsTableId;
			$fieldName = 'FileMaterialPDF';
		} else if ($type == 'download_material_collection_xls') {
			$previewsParams = array();
			$tableId = Constants::$collectionsTableId;
			$fieldName = 'FileMaterialXLS';
		} else {
			return;
		}
		
		foreach($dbArray as $dbKey => $oneDbElement){
			foreach($yaDiskArray as $oneYaDiskElement){
				$photoName = explode('.', $oneYaDiskElement['displayName']);
				$shouldDownloadFile = false;
				if ($type == 'collection'){
					$photoName[0] = preg_replace('/_cover/', '', $photoName[0]);
					$dbName = preg_replace('/\s/', '', $oneDbElement['Name']);
				} elseif ($type == 'rapports'){
					$photoName[0] = preg_replace('/_rep/', '', $photoName[0]);
					$dbName = preg_replace('/\s/', '', $oneDbElement['Name']);
				} elseif ($type == 'tkani'){
					$dbName = preg_replace('/\s/', '', $oneDbElement['Name']);
				} elseif ($type == 'download_material_collection_pdf'){
					$dbName = preg_replace('/\s/', '', $oneDbElement['Name']);
				} elseif ($type == 'download_material_collection_xls'){
					$dbName = preg_replace('/\s/', '', $oneDbElement['Name']);
				}  else {
					continue;
				}
				
				$dbName = str_replace(array('"', "'"), "", $dbName);
				$photoName[0] = str_replace(array('"', "'"), "", $photoName[0]);
				
				if (mb_strtolower($photoName[0], 'UTF-8') == mb_strtolower($dbName, 'UTF-8')){
					self::makePreviewForElement($oneYaDiskElement, $oneDbElement, $type, $fieldName, $previewsParams, $dbArray, $dbKey, $diskClient, $tableId);
				}
			}
		}
	}
	
	
	public static function makePreviewForElement($oneYaDiskElement, $oneDbElement, $type, $fieldName, $previewsParams, &$dbArray, $dbKey, $diskClient, $tableId){
		global $db, $FILES_FOLDER;
		$dbFileArray = CommonFunctions::makeFileArrayFromDbString($oneDbElement[$fieldName]);
		$currentPreviewParams = $previewsParams;
		$fileOnFtp = '';
		if (!empty($dbFileArray['path'])){
			$fileOnFtp = $FILES_FOLDER.$dbFileArray['path'];
			
			if (!file_exists($fileOnFtp)){
				$shouldDownloadFile = true;
				goto shouldMakePreviewForCollection;
			}
			$fileMTime = filemtime($fileOnFtp);
			$diskLastModifiedTime = new DateTime($oneYaDiskElement['lastModified']);
			$ftpLastModifiedTime = DateTime::createFromFormat('U', $fileMTime);
			if ($ftpLastModifiedTime <= $diskLastModifiedTime){
				$shouldDownloadFile = true;
				goto shouldMakePreviewForCollection;
			}
			$pathInfo = pathinfo($fileOnFtp);
			foreach($currentPreviewParams as $key => $previewParam){
				$currentPreviewFile = $pathInfo['dirname'].'/'.$pathInfo['filename'].'_'.$previewParam['width'].'x'.$previewParam['height'].'.'.$pathInfo['extension'];
				if (!file_exists($currentPreviewFile)){
					continue;
				}
				$currentPreviewFileMTime = filemtime($currentPreviewFile);
				$currentPreviewFileModifiedTime = DateTime::createFromFormat('U', $currentPreviewFileMTime);
				if ($currentPreviewFileModifiedTime <= $diskLastModifiedTime){
					continue;
				}
				unset($currentPreviewParams[$key]);
			}
			$currentPreviewParams = array_values($currentPreviewParams);
			if (!empty($currentPreviewParams)){
				goto shouldMakePreviewForCollection;
			}
			return;
		} else {
			$shouldDownloadFile = true;
		}
shouldMakePreviewForCollection:	
		if ($type == 'collection'){
			if (!is_dir($FILES_FOLDER.Constants::$collectionsImagesBaseFolderName.'/'.$oneDbElement['Message_ID'].'/')){
				mkdir($FILES_FOLDER.Constants::$collectionsImagesBaseFolderName.'/'.$oneDbElement['Message_ID'].'/', 0777, true);
			}
			$resultFolder = $FILES_FOLDER.Constants::$collectionsImagesBaseFolderName.'/'.$oneDbElement['Message_ID'].'/';
		} elseif ($type == 'tkani') {
			if (!is_dir($FILES_FOLDER.Constants::$tkaniBaseImagesFolderName.'/'.$oneDbElement['Message_ID'].'/')){
				mkdir($FILES_FOLDER.Constants::$tkaniBaseImagesFolderName.'/'.$oneDbElement['Message_ID'].'/', 0777, true);
			}
			$resultFolder = $FILES_FOLDER.Constants::$tkaniBaseImagesFolderName.'/'.$oneDbElement['Message_ID'].'/';
		} elseif ($type == 'rapports'){
			if (!is_dir($FILES_FOLDER.Constants::$tkaniRapportsBaseImagesFolderName.'/'.$oneDbElement['Message_ID'].'/')){
				mkdir($FILES_FOLDER.Constants::$tkaniRapportsBaseImagesFolderName.'/'.$oneDbElement['Message_ID'].'/', 0777, true);
			}
			$resultFolder = $FILES_FOLDER.Constants::$tkaniRapportsBaseImagesFolderName.'/'.$oneDbElement['Message_ID'].'/';
		} elseif ($type == 'download_material_collection_pdf'){
			if (!is_dir($FILES_FOLDER.Constants::$collectionsMaterialCollectionBaseFolderName.'/'.$oneDbElement['Message_ID'].'/')){
				mkdir($FILES_FOLDER.Constants::$collectionsMaterialCollectionBaseFolderName.'/'.$oneDbElement['Message_ID'].'/', 0777, true);
			}
			$resultFolder = $FILES_FOLDER.Constants::$collectionsMaterialCollectionBaseFolderName.'/'.$oneDbElement['Message_ID'].'/';
		} elseif ($type == 'download_material_collection_xls'){
			if (!is_dir($FILES_FOLDER.Constants::$collectionsMaterialCollectionBaseFolderName.'/'.$oneDbElement['Message_ID'].'/')){
				mkdir($FILES_FOLDER.Constants::$collectionsMaterialCollectionBaseFolderName.'/'.$oneDbElement['Message_ID'].'/', 0777, true);
			}
			$resultFolder = $FILES_FOLDER.Constants::$collectionsMaterialCollectionBaseFolderName.'/'.$oneDbElement['Message_ID'].'/';
		} elseif ($type == 'persona') {
			if (!is_dir($FILES_FOLDER.Constants::$tkaniPersonaBaseImagesFolderName.'/'.$oneDbElement['Message_ID'].'/')){
				mkdir($FILES_FOLDER.Constants::$tkaniPersonaBaseImagesFolderName.'/'.$oneDbElement['Message_ID'].'/', 0777, true);
			}
			$resultFolder = $FILES_FOLDER.Constants::$tkaniPersonaBaseImagesFolderName.'/'.$oneDbElement['Message_ID'].'/';
		} else {
			//continue;
		}
		if ($shouldDownloadFile){
			
			//Материалы
			$file_ext = pathinfo($oneYaDiskElement['displayName'], PATHINFO_EXTENSION);
			if ($type == 'download_material_collection_pdf' && $file_ext !== 'pdf') {
				return;
			} elseif ($type == 'download_material_collection_xls' && ($file_ext !== 'xlsx' || $file_ext !== 'xls')) {
				return;
			}
			
			$diskClient->downloadFile($oneYaDiskElement['href'], $resultFolder, $oneYaDiskElement['displayName']);
			$imageFile = $resultFolder.$oneYaDiskElement['displayName'];
		} else {
			$imageFile = $fileOnFtp;
		}
		
		$pathInfo = pathinfo($imageFile);
		foreach($currentPreviewParams as $oneCurrentViewParam){
			$previewFilePath = CommonFunctions::getPreviewNameForFile($imageFile, $oneCurrentViewParam['width'], $oneCurrentViewParam['height']);
			if ($oneCurrentViewParam['CropImage']){
				nc_ImageTransform::imgResize($imageFile, $previewFilePath, $oneCurrentViewParam['width'], $oneCurrentViewParam['height'], 1, null, 95);
			} else {
				nc_ImageTransform::imgResize($imageFile, $previewFilePath, $oneCurrentViewParam['width'], $oneCurrentViewParam['height'], 0, null, 95);
			}
		}
		$dbFileString = CommonFunctions::makeDbStringFromFilePath($imageFile);
		
		$sql = 'update Message'.$db->escape($tableId).' set '.$db->escape($fieldName).' = "'.$db->escape($dbFileString).'" where Message_ID = '.$db->escape($oneDbElement['Message_ID']);
		$db->query($sql);
		$dbArray[$dbKey][$fieldName] = $dbFileString;
		sleep(1);
	}
	
	
	
	
	
	
	
	
	
	
	public static function importPersonaImages($diskClient, &$settings, $perRun){
		// 
		global $db, $FILES_FOLDER;
		$personaPhotos = YaDiskHelper::makeDirsPretty($diskClient->directoryContents($settings["PersonaImageFolder"]));
		$sql = 'select * from Message'.$db->escape(Constants::$collectionsTableId);
		$collections = (array)$db->get_results($sql, ARRAY_A);
		$sql = 'select * from Message'.$db->escape(Constants::$tkaniTableId);
		$tkani = (array)$db->get_results($sql, ARRAY_A);
		$sql = 'select * from Message'.Constants::$personaTableId;
		$persona = (array)$db->get_results($sql, ARRAY_A);
		$sql = 'select max(Priority) from Message'.Constants::$personaTableId;
		$priority = (int)$db->get_var($sql);
		$previewsParams = Settings::getPreviewSettingsForPersonaParsed();
		$tableId = Constants::$personaTableId;
		$fieldName = 'Image';
		
		if (empty($settings['LastPersonaImportPhotoId'])){
			$settings['LastPersonaImportPhotoId'] = 1;
		}
		$currentLastKeyId = $settings['LastPersonaImportPhotoId'];
		$nextLastKeyId = (int)$currentLastKeyId + (int)$perRun;
		$settings['LastPersonaImportPhotoId'] = $nextLastKeyId;
		
		if ($nextLastKeyId > count($personaPhotos)){
			 $settings['LastPersonaImportPhotoId'] = 1;
		}
		// var_dump($personaPhotos);
		foreach($personaPhotos as $oneYaDiskKey => $oneYaDiskElement){
			if ($oneYaDiskKey <= $currentLastKeyId){
				continue;
			}
			if ($oneYaDiskKey > $nextLastKeyId){
				return;
			}
			$diskName = $oneYaDiskElement['displayName'];
			$diskTkanDbName = preg_replace('/_gallery(.*)+/', '', $diskName);
			$diskCollectionDbName = preg_replace('/\d*_gallery(.*)+/', '', $diskName);
			
			$dbTkan = array();
			foreach($tkani as $oneTkan){
				$tkanName = preg_replace('/\s+/', '', $oneTkan['Name']);
				if (mb_strtolower($diskTkanDbName, 'UTF-8') == mb_strtolower($tkanName, 'UTF-8')){
					$dbTkan = $oneTkan;
					break;
				}
			}
			$dbCollection = array();
			foreach($collections as $oneCollection){
				$collectionName = preg_replace('/\s+/', '', $oneCollection['Name']);
				if (mb_strtolower($diskCollectionDbName, 'UTF-8') == mb_strtolower($collectionName, 'UTF-8')){
					$dbCollection = $oneCollection;
					break;
				}
			}
			if (empty($dbCollection) && empty($dbTkan)){
				continue;
			}
			$dbPersona = array();
			foreach($persona as $onePersona){
				if (stripos($onePersona['Image'], $diskName.':') !== false){
					$dbPersona = $onePersona;
					break;
				}
			}
			
			if (empty($dbPersona)){
				$priority++;
				$sql = 'insert into Message'.Constants::$personaTableId.'(Subdivision_ID, Sub_Class_ID, Priority, Checked, Image, Collection_ID, Item_ID) values (7, 81, '.$priority.', 1, \'\',';
				if (!empty($dbCollection)){
					$sql .= $dbCollection['Message_ID'];
				} else {
					$sql .= 'null';
				}
				$sql .= ',';
				if (!empty($dbTkan)){
					$sql .= $dbTkan['Message_ID'];
				} else {
					$sql .= 'null';
				}
				$sql .= ')';
				$db->query($sql);
				$personaId = $db->insert_id;
			} else {
				$personaId = $dbPersona['Message_ID'];
			}
			$sql = 'select * from Message'.Constants::$personaTableId.' where Message_ID = '.$db->escape($personaId);
			$dbArray = (array)$db->get_results($sql, ARRAY_A);
			foreach($dbArray as $dbKey => $oneDbElement){
				self::makePreviewForElement($oneYaDiskElement, $oneDbElement, 'persona', $fieldName, $previewsParams, $dbArray, $dbKey, $diskClient, Constants::$personaTableId);
			}
		}
	}
	
	
	
	
}