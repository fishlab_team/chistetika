<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Response\QrCodeResponse;

global $DOCUMENT_ROOT,$HTTP_FILES_PATH,$AUTH_USER_ID,$FILES_FOLDER,$system_env,$current_catalogue;

$is_download = (int)$_GET['is_download'];
$is_email = (int)$_GET['is_email'];
$system_email = $system_env["SpamFromEmail"];

$nc_core = nc_Core::get_object();
$db = $nc_core->db;
$template_path = helperData::getTemplatePath();
$full_site_url = helperData::getFullUrlSite();
$netshop = nc_netshop::get_instance();
$allFavorites = $netshop->goodslist_favorite->get_all('DESC', 9999);

$full_path_image_qr_code_site = $DOCUMENT_ROOT.$template_path.'img/pdf/qr-code.png';
if(!file_exists($full_path_image_qr_code_site)){
	$qrCode = new QrCode($full_site_url);
	$qrCode->setSize(100);
	$qrCode->setMargin(0);
	$qrCode->setEncoding('UTF-8');
	$qrCode->setForegroundColor(['r' => 51, 'g' => 177, 'b' => 160, 'a' => 1]);
	$qrCode->writeFile($full_path_image_qr_code_site);
}
$path_image_qr_code_site = $template_path.'img/pdf/qr-code.png';

$html = '<!DOCTYPE html>
			<html lang="ru">

			<head>
				<meta charset="UTF-8">
				<title>Title</title>
			</head>
			<style>
				@import url("https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=cyrillic-ext");
				body {
					width: 210mm;
					margin: 0 auto;
					padding: 0;
					background: #fff;
					font-family: "PT Sans", sans-serif;
					font-size: 14px;
				}

				.favorites-pdf {
					padding: 40px;
				}

				.header__code {
					padding-right: 30px;
					float: left;
				}

				.header__contacts {
					line-height: 1.4;
					float: left;
				}

				.header {
					color: #33b1a0;
					height: 40mm;
					margin-top:25mm;
				}

				.header:after,
				.item:after {
					content: \'\';
					display: table;
					clear: both;
				}

				.header__contacts a {
					text-decoration: none;
					color: #33b1a0;
				}

				.header__logo {
					margin: -30px 0 -30px auto;
					float: right;
				}

				.item__name {
					color: #006884;
					text-transform: uppercase;
					font-size: 28px;
					font-weight: normal;
					display: block;
					margin-bottom: 15px;
					margin-top: -5px;
				}

				.item {
					min-height: 80mm;
					margin-bottom: 10mm;
					clear: both;
					position: relative;
					padding: 20px 100px 20px 0;
					border-bottom: 1px solid #33b1a0;
				}

				.item:last-child {
					border-bottom: none;
				}

				.item:last-child:after {
					display: none;
				}

				.item:after {
					position: absolute;
					right: 0;
					bottom: -5px;
					background: #006884;
					width: 9px;
					height: 9px;
					content: \'\';
					display: block;
				}

				.item__pic {
					width: 240px;
					float: left;
					margin-right: -100%;
					position: relative;
				}

				.item__info {
					margin-left: 270px;
				}

				.item__pic img {
					display: block;
				}

				.item__pic .ico {
					position: absolute;
					right: 20px;
					top: 20px;
					z-index: 3;
				}

				.item-params {
					font-size: 12px;
					margin: 0;
					padding-bottom: 10px;
				}

				.item-params dt {
					color: #006884;
					width: 140px;
					float: left;
					margin-right: -100%;
				}

				.item-params dd {
					padding: 0;
					margin: 0 0 0 160px;
				}

				.param-ico {
					display: inline-block;
					vertical-align: middle;
					margin-right: 10px;
				}

				.item__code {
					position: absolute;
					top: 20px;
					right: 0;
				}
				
				.item.is_break{
					page-break-after:always;
				}
				.item.is_break + .item{
					margin-top:40mm;
				}
			</style>

			<body>

				<div class="favorites-pdf">

					<div class="header">
						<div class="header__code">
							<img src="'.$full_site_url.$path_image_qr_code_site.'" width="100" height="100" alt="">
						</div>
						<div class="header__contacts">
							143050, Московская область,<br> Одинцовский район, Малые Вязёмы,<br> Петровский проезд, владение 2, строение 2<br> Телефон: +7 (495) 637-90-79 доб.246<br>
							<a href="http://www.chistetika.ru" target="_blank">www.chistetika.ru</a>
						</div>
						<div class="header__logo">
							<img src="'.$full_site_url.$template_path.'img/pdf/logo.svg" width="140" height="137" alt="">

						</div>
					</div>';
//

$row_break_i_first_three = 1;
$row_break_i_by_four_not_first_three = 1;
$total = count($allFavorites);
foreach($allFavorites as $arrRowItem){
	if((int)$arrRowItem['Class_ID'] !== 31){
		$allItems = $db->get_results("SELECT Message_ID FROM Message31 
									  WHERE Collection = '".(int)$arrRowItem['Item_ID']."' AND 
									  Parent_Message_ID = 0 AND 
									  Language_Parent IS NULL",ARRAY_A);
		if(!empty($allItems)){
			$arrItemsCol = array();
			foreach($allItems as $arrRowI){
				$arrItemsCol[] = array(
					'id' => $arrRowI['Message_ID'],
				);
			}
			$html .= s_list_class(sct::sub('item'), sct::cc('item'), "nc_ctpl=".sct::tpl('item')."&isNaked=1&json_favorites_pdf=".json_encode($arrItemsCol)."");
		}
	}else{
		$arrItemToJson = array(
			array(
				'id' => $arrRowItem['Item_ID'],
				'show_by_article' => true,
			)
		);
		$html .= s_list_class(sct::sub('item'), sct::cc('item'), "nc_ctpl=".sct::tpl('item')."&isNaked=1&json_favorites_pdf=".json_encode($arrItemToJson));
	}
	
	$row_break_i_first_three++;
	$row_break_i_by_four_not_first_three++;
}

$html .= '</div>
	</body>
</html>';


$field_user_value = $AUTH_USER_ID;
if(empty($AUTH_USER_ID)){
	$cookie_key = ini_get('session.name');
	$field_user_value = $nc_core->input->fetch_cookie($cookie_key);
}

$file_html_pdf = $FILES_FOLDER.'favorites_pdf/favorites_pdf_'.$field_user_value.'.html';
$file_pdf = $FILES_FOLDER.'favorites_pdf/favorites_pdf_'.$field_user_value.'.pdf';

$testDev = file_put_contents($file_html_pdf, $html);
exec('xvfb-run /usr/bin/wkhtmltopdf '.$file_html_pdf.' '.$file_pdf);


if($is_email === 1){
	$arrResult = array(
		'status' => 0,
		'header' => Langs::TEXT_ERROR,
		'text' => Langs::TEXT_ERROR,
	);
	
	$email_send = $_POST['email'];
	$agreement = trim($_POST['agreement']);
	
	if($agreement === 'on' && !empty($email_send) && filter_var($email_send, FILTER_VALIDATE_EMAIL)){
		$arrResult['status'] = 1;
		$arrResult['header'] = Langs::TEXT_SEND_BY;
		$arrResult['text'] = Langs::TEXT_LETTER_PDF_SENT_EMAIL;
		
		$mailer = new CMIMEMail();
		$htmlText = "Здравствуйте вы запросили PDF-файл с избранными товарами.";
		$mailer->mailbody(strip_tags($htmlText),$htmlText);
		$subject = "Запрос PDF-файла с избранным с сайта ".$current_catalogue['Catalogue_Name'];
		$mailer->attachFile($file_pdf, 'favorites_pdf_'.$field_user_value.'.pdf', "application/octet-stream");
		$mailer->send($email_send, $system_email, $system_email, $subject, $current_catalogue["Catalogue_Name"]);
	}
	
	ob_end_clean();
	echo json_encode($arrResult);
	exit();
}else{
	header("Content-type:application/pdf");
	// It will be called downloaded.pdf
	header("Content-Disposition:".($is_download ? 'attachment' : 'inline').";filename='favorites_pdf_".$field_user_value.".pdf'");
	// The PDF source is in original.pdf
	readfile($file_pdf);
}
?>



        