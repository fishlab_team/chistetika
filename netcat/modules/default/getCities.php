<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

$city_name = $db->escape(mb_strtolower($_POST['city']));

$arrResult = array(
	'status' => 0,
	'html' => NULL,
	'city' => NULL,
);

if(!empty($city_name)){
	$arrLang = getLanguage(false, true);
	$lang_id = $arrLang['id'];
	$lang_prefix_field = $arrLang['prefix_field'];
	$table_name = 'Cities';
	if($lang_id !== 1){
		$table_name .= $lang_prefix_field;
	}
	
	$allCities = $db->get_results("SELECT a.".$table_name."_Name as Name,
										  a.".$table_name."_ID as ID 
								   FROM Classificator_".$table_name." as a 
										INNER JOIN Message204 as c ON a.".$table_name."_ID = c.Cities
								   WHERE a.".$table_name."_Name LIKE '%".$city_name."%' AND c.TypeContact <> 4
								   GROUP BY a.".$table_name."_Name
								   ORDER BY a.".$table_name."_Priority", ARRAY_A);
	
	$arrResult['status'] = 1;
	$cookie_city_id = (int)trim($nc_core->input->fetch_cookie('city_id'));
	foreach($allCities as $arrRow){
		$is_active = false;
		if($cookie_city_id === (int)$arrRow['ID']){
			$is_active = true;
		}
		$arrResult['html'] .= '<li class="city-select-list__item '.($is_active ? 'city-select-list__item_active' : NULL).'" data-id="'.$arrRow['ID'].'">'.$arrRow['Name'].'</li>';
		$arrResult['city'][] = $arrRow['Name'];
	}
}

ob_end_clean();
echo json_encode($arrResult);
exit();
?>