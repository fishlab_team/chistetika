<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

global $AUTH_USER_ID;

$like_type = (int)$_POST['type'];
$comment_id = (int)$_POST['id'];
$company_id = (int)$_POST['company_id'];

$arrResult = array(
	'status' => 0,
	'text' => 'error',
);

if(!empty($like_type) && !empty($comment_id) && !empty($company_id)){
	if(empty($AUTH_USER_ID)){
		$cookie_key = ini_get('session.name');
		$cookie_value = $nc_core->input->fetch_cookie($cookie_key);
		if(!empty($cookie_value)){
			$exists_vote = (int)$db->get_var("SELECT Message_ID FROM Message218 
											  WHERE SID = '".$cookie_value."' AND 
													Comment_ID = '".$comment_id."' AND 
													Company_ID = '".$company_id."'");
			if(!empty($exists_vote)){
				$arrResult['text'] = 'Вы уже голосовали!';
			}else{
				$arrResult['status'] = 1;
				$arrResult['text'] = 'OK!';
				
				$db->query("INSERT INTO Message218 (User_ID, Checked, Created, LastUpdated, Comment_ID, LikeComment, SID, Company_ID) 
							VALUES (0, 1, NOW(), NOW(), '".$comment_id."', '".$like_type."', '".$cookie_value."', '".$company_id."')");
			}
		}
	}else{
		$exists_vote = (int)$db->get_var("SELECT Message_ID FROM Message218 
										  WHERE User_ID = '".$AUTH_USER_ID."' AND 
												Comment_ID = '".$comment_id."' AND 
												Company_ID = '".$company_id."'");
		if(!empty($exists_vote)){
			$arrResult['text'] = 'Вы уже голосовали!';
		}else{
			$arrResult['status'] = 1;
			$arrResult['text'] = 'OK!';
			
			$db->query("INSERT INTO Message218 (User_ID, Checked, Created, LastUpdated, Comment_ID, LikeComment, SID) 
						VALUES ('".$AUTH_USER_ID."', 1, NOW(), NOW(), '".$comment_id."', '".$like_type."', NULL, '".$company_id."')");
		}
	}
}

ob_end_clean();
echo json_encode($arrResult);
exit();
?>