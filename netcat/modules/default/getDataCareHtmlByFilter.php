<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

$spot_ids = $_POST['spot_ids'];
$type_cloth_ids = $_POST['type_cloth_ids'];

$arrSpotIds = explode(',', $spot_ids);
$arrSpotIds = array_map(function($value){
	return (int)$value;
}, $arrSpotIds);
$arrSpotIds = array_filter($arrSpotIds);


$arrTypeClothIds = explode(',', $type_cloth_ids);
$arrTypeClothIds = array_map(function($value){
	return (int)$value;
}, $arrTypeClothIds);
$arrTypeClothIds = array_filter($arrTypeClothIds);

if(!empty($arrSpotIds) && !empty($arrTypeClothIds)){
	echo s_list_class(sct::sub('care'), sct::cc('care'), "nc_ctpl=".sct::tpl('care_ajax_by_filter')."&isNaked=1&spot_ids=".implode(',', $arrSpotIds)."&type_cloth_ids=".implode(',', $arrTypeClothIds)."");
}
exit();
?>