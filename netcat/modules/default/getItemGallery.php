<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

$template_path = helperData::getTemplatePath();

$arrLang = getLanguage(false, true);
$lang_prefix = $arrLang['prefix'];
$active_id = (int)$_GET['active_id'];
$active_class_id = (int)$_GET['active_class_id'];
$item_id = (int)$_GET['item_id'];
$netshop = nc_netshop::get_instance();
//$show_catalog_by_article = (bool)$_GET['show_catalog_by_article'];

$html = '';
$item = new nc_netshop_item(array("Class_ID" => 31, "Message_ID" => $item_id));
$item = itemQueryLand::getItemField($item);
$hide_data_info_collection = false;
$hide_data_info_child = false;
if($show_catalog_by_article){
	$hide_data_info_collection = true;
	$hide_data_info_child = true;
}
$arrDataItems = getDataItemsCollectionChild($item, $hide_data_info_collection, $hide_data_info_child, true);

//По умолчанию делаем, что была передана коллекция
$item_collection_name = $item['AdditionalData']['Collection_Name'];
$class_id = 160;

$row_i = 0;
$row_i_active = 0;
$is_active_global = false;

$html_active = '';
//collection_id если не пустое, значит это коллекция
foreach($arrDataItems as $arrRowDataItem){
	$row_id = $arrRowDataItem['id'];
	if(!empty($arrRowDataItem['collection_id'])){
		$row_id = $arrRowDataItem['collection_id'];
	}
	
	$is_active = false;
	
	if((int)$row_id === $active_id && (int)$arrRowDataItem['class_id'] === $active_class_id && $is_active_global){
		continue;
	}
	
	//Ставим активный индекс row_i_active для слайдера
	//Чтобы показать выбранный объект
	//И ставим активное имя item_collection_name и класс id class_id
	if((int)$row_id === $active_id && (int)$arrRowDataItem['class_id'] === $active_class_id && !$is_active_global){
		$row_i_active = $row_i;
		$is_active = true;
		$is_active_global = true;
		$item_collection_name = !empty($arrRowDataItem['collection_id']) ? $arrRowDataItem['item_data']['AdditionalData']['Collection_Name'] : $arrRowDataItem['item_data']['Name'];
		$class_id = $arrRowDataItem['class_id'];
	}
	
	//Чтобы когда слайдер переключали, и снова переключались на первое изображение, становилось имя коллекции
	$arrJsonItem = array(
		'name' => !empty($arrRowDataItem['collection_id']) ? $arrRowDataItem['item_data']['AdditionalData']['Collection_Name'] : $arrRowDataItem['item_data']['Name'],
		'id' => $row_id,
		'class_id' => $arrRowDataItem['class_id'],
		'is_favorite' => $netshop->goodslist_favorite->check($row_id, $arrRowDataItem['class_id']),
	);
	if(empty($arrRowDataItem['no_photo'])){
		if(!empty($is_active)){
			$html_active = '<div class="modal-gallery-slider__item '.($arrRowDataItem['no_photo'] ? 'item-tile-slider__link_no-pic' : NULL).'" data-json=\''.json_encode($arrJsonItem).'\'>
								<img src="'.$arrRowDataItem['modal'].'" alt="">
							</div>';
		}else{
			$html .= '<div class="modal-gallery-slider__item '.($arrRowDataItem['no_photo'] ? 'item-tile-slider__link_no-pic' : NULL).'" data-json=\''.json_encode($arrJsonItem).'\'>
						'.($row_i === 0 && $show_catalog_by_article ? '<img src="'.$arrRowDataItem['modal'].'" alt="">' : '<img data-lazy="'.$arrRowDataItem['modal'].'" alt="">').'
					  </div>';
		}
		$row_i++;
	}
}


$is_favorite = $netshop->goodslist_favorite->check($active_id, $class_id);
?>
<div class="modal-gallery">

    <div class="modal-gallery__title">
        <?=$item_collection_name?>
    </div>

    <a href="#" class="favorites-ico js-add-favorites is_modal_gallery <?=($is_favorite ? 'active' : NULL)?>" data-url="<?=$lang_prefix?>/netcat/modules/default/addFavorite.php" data-class-id="<?=$class_id?>" data-id="<?=$active_id?>"></a>

    <div class="modal-gallery-slider" data-active-index="<?=$row_i_active?>">
        <?=$html_active.$html?>
    </div>
</div>