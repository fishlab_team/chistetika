<?php

// пример поискового item-а (лишнее выкинуто)
// productID и price - наши поля с html-области индексирования

/*
nc_search_result Object
(
    [items:protected] => Array
        (
            [0] => nc_search_result_document Object
                (
                    [table_name:protected] => Search_Document
                    [properties:protected] => Array
                        (
                            [id] => 296
                            [contenttype] => text/html
                            [site_id] => 1
                            [path] => /catalog/braslet-granitnaya-nezhnost-163.html
                            [url] => /catalog/braslet-granitnaya-nezhnost-163.html
                            [language] => ru
                            [title] => GL Jewelry / Каталог / <strong class='matched'>Браслет</strong> "Гранитная нежность"
                            [context] => <strong class='matched'>Браслет</strong> <span class='skipped'>&hellip;</span> <strong class='matched'>Браслет</strong>
                            [content] => full text
                            [lastmodified] => 2016-10-25 15:30:11
                            [meta] => Array
                                (
                                    [productID] => 163
                                    [price] => 31 000 
                                )

                        )
				)
		)
)
*/

// расширение функционала стандартного поиска NetCat-а
namespace SearchExtensions;

/**
 * Быстрый заказ (заказ в один клик)
 *
 */
class Search
{
	//private $nc_core;
	private $nc_search;
	
	function __construct($nc_search = NULL) 
	{
		//$this->nc_core = $nc_core;
		$this->nc_search = $nc_search;
	}
	
	// доступ protected-свойствам
	private function accessProtected($obj = NULL, $prop = NULL) 
	{
		$reflection = new \ReflectionClass($obj);
		$property = $reflection->getProperty($prop);
		$property->setAccessible(true);
		return $property->getValue($obj);
	}
	
	private function getMetaData($result = NULL)
	{
		$resultArray = array();
		
		if ($result)
		{
			$items = $this->accessProtected($result, "items");
			
			if ($items)
			{
				foreach ($items as $item)
				{
					$properties = $this->accessProtected($item, "properties");
					
					if ($properties)
					{
						if (is_array($properties['meta']) && $properties['meta'])
						{
							$resultArray[] = $properties['meta'];
						}
					}
				}
			}
		}
		
		return $resultArray;
	}
	
	// $search_query - поисковая фраза
	// $area - область поиска
	// $params - параметры поиска
	public function query($search_query = "", $area = "", $params = "")
	{
		$results = "";

        $arrLang = getLanguage(false, true);
        $lang_id = (int)$arrLang['id'];

        if($lang_id === 1){
            $search_area = 'sub7';
        }else{
            $search_area = '-sub5 -sub6 -sub13 -sub14 -sub15 -sub16 -sub17 -sub18 -sub19 -sub23 -sub27';
        }
		
		$metaData = array();
		
		if ($search_query)
		{
			$search_query_original = $search_query;
			//$search_query = '"%'.$search_query.'%"';
			//if(is_numeric(mb_substr($search_query,0,1))){
			if(!empty(preg_match_all("/[\d]/i",$search_query,$matches))){
				$search_query = '"'.$search_query.'*"';
			}else{
				$search_query = $search_query.'*';
			}
			$search_query = $this->nc_search->make_query_string($search_query);
			
			if ($search_query)
			{
				$search_query = \nc_search_util::convert($search_query, 1);
				$results = $this->nc_search->get_results($search_query, $search_area, $params);
			}
		}
		
		if ($results) 
		{
			$metaData = $this->getMetaData($results);
		}
		
		if(empty($metaData)){
			$search_query = $search_query_original.'*';
			$search_query = $this->nc_search->make_query_string($search_query);
			
			if ($search_query)
			{
				$search_query = \nc_search_util::convert($search_query, 1);
				$results = $this->nc_search->get_results($search_query, $area, $params);
			}
			if ($results) 
			{
				$metaData = $this->getMetaData($results);
			}
		}
		
		if(empty($metaData)){
			$search_query = $search_query_original;
			$search_query = $this->nc_search->make_query_string($search_query);
			
			if ($search_query)
			{
				$search_query = \nc_search_util::convert($search_query, 1);
				$results = $this->nc_search->get_results($search_query, $area, $params);
			}
			if ($results) 
			{
				$metaData = $this->getMetaData($results);
			}
		}
		
		//print_r($metaData);
		
		return $metaData;
	}
}

?>