<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

$city_name = $db->escape(mb_strtolower($_POST['city']));

$arrResult = array(
	'status' => 0,
	'city' => NULL,
	'city_id' => NULL,
);

if(!empty($city_name)){
	$arrLang = getLanguage(false, true);
	$lang_id = $arrLang['id'];
	$lang_prefix = $arrLang['prefix'];
	$table_name = 'Cities';
	if($lang_id !== 1){
		$table_name .= $lang_prefix;
	}
	
	$arrCity = $db->get_row("SELECT ".$table_name."_Name as Name,".$table_name."_ID as ID FROM Classificator_".$table_name." 
							 WHERE LOWER(".$table_name."_Name) LIKE '%".$city_name."%'
							 GROUP BY ".$table_name."_Name
							 ORDER BY ".$table_name."_Priority
							 LIMIT 1", ARRAY_A);
	$arrResult['status'] = 1;
	if(!empty($arrCity)){
		$arrResult['city'] = $arrCity['Name'];
		$arrResult['city_id'] = $arrCity['ID'];
	}else{
		$arrResult['city'] = getDefaultCityName();
		$arrResult['city_id'] = getDefaultCityName(true);
	}
	
	$arrResult['phone'] = getPhoneHeaderByCityID($arrResult['city_id']);
	$arrResult['phone_clear'] = clearPhoneForTel($arrResult['phone']);
}

ob_end_clean();
echo json_encode($arrResult);
exit();
?>