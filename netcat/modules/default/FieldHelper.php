<?php

class FieldHelper
{
    public static function renderStringField($langAlias, $defaultValue, $fieldName, $fieldSettings, $classId, $renderCaption = 1)
    {
        $ncField = nc_string_field($fieldName, $fieldSettings, $classId, $renderCaption);

        if ($langAlias) {
            $ncField = str_replace("'f_{$fieldName}'", "'f_{$fieldName}_{$langAlias}'", $ncField);
            $ncField = str_replace("\"f_{$fieldName}\"", "\"f_{$fieldName}_{$langAlias}\"", $ncField);
        }
        
        if ($defaultValue) {
            $defaultValue = htmlspecialchars($defaultValue, ENT_QUOTES);

            $ncField = preg_replace("/value=('|\").*('|\")/", "value='{$defaultValue}'", $ncField);
        }

        return $ncField;
    }

    public static function renderTextField($langAlias, $defaultValue, $fieldName, $fieldSettings, $classId, $renderCaption = 1)
    {
        $ncField = nc_text_field($fieldName, $fieldSettings, $classId, $renderCaption);

        if ($langAlias) {
            $ncField = str_replace("'f_{$fieldName}'", "'f_{$fieldName}_{$langAlias}'", $ncField);
            $ncField = str_replace("\"f_{$fieldName}\"", "\"f_{$fieldName}_{$langAlias}\"", $ncField);
        }

        if ($defaultValue) {
            $defaultValue = htmlspecialchars($defaultValue, ENT_QUOTES);
            $matches = false;
            $replaceProcess = preg_match("/(<textarea .*>).*(<\/textarea>)/s", $ncField, $matches);
            $ncField = str_replace($matches[0], $matches[1].$defaultValue.$matches[2], $ncField);
            //$ncField = preg_replace("/(<textarea .*>).*(<\/textarea>)/s", "$1{$defaultValue}$2", $ncField);
        }

        return $ncField;
    }

    public static function dbSelectField($cc, $fieldName, $onlyMainLang = false, $noValue = '', $value = '0', $asName = 'Name', $asID = 'Message_ID', $join = '', $where = '', $addText = '')
    {
        //вывод списка
        if (strpos($fieldName, "[") === false) {
            $fieldName = "f_" . $fieldName;
        }

        $nc_cc2 = (string) ($cc * 1);
        if ($nc_cc2 == $cc) {
            $tablet = "Message{$nc_cc2}";
        } else {
            $tablet = (string) $cc;
        }

        $result = "<select name='" . $fieldName . "'" . $addText . ">" .
            "<option value='{$noValue}'" . ($value ? "" : " selected='selected'") . ">-- выбрать --</option>" .
            listQuery(
                "SELECT DISTINCT `a`.`{$asID}`, `a`.`{$asName}` 
                FROM `{$tablet}` `a` 
                {$join} 
                WHERE 1 AND `a`.`Checked` = 1" . ($where ? " AND " . $where : "") . ($onlyMainLang ? " AND `a`.`Language_ID` = '1'" : "") . " 
                ORDER BY `a`.`{$asID}`",
                "<option value='\$data[" . $asID . "]'" . ($value ? "\".(\$data[" . $asID . "]==" . $value . " ? \" selected='selected'\" : \"\").\"" : "") . ">\".strip_tags(\$data[" . $asName . "]).\"</option>") .
            "</select>";

        return $result;
    }
	
	public static function renderTabSystemLang($class_id = NULL, $edit_message_id = NULL){
		$html = '<div data-tab-caption="Дополнительно (RU)" data-tab-id="data-system-ru">
					'.self::renderTabSystemLangFields("ru", 1, $class_id, $edit_message_id).'
				 </div>
				 
				 <div data-tab-caption="Дополнительно (EN)" data-tab-id="data-system-en">
					'.self::renderTabSystemLangFields("en", 2, $class_id, $edit_message_id).'
				 </div>';
		
		return $html;
	}
	
	public static function getFieldsSystem(){
		$arrResult = array(
						"Created",
						"LastUpdated",
						"User_ID",
						"LastUser_ID",
						"IP",
						"LastIP",
						"Checked",
						"Priority",
						"Keyword",
						"ncTitle",
						"ncKeywords",
						"ncDescription",
						"ncSMO_Title",
						"ncSMO_Description",
						"ncSMO_Image",
		);
		
		return $arrResult;
	}
	
	public static function renderTabSystemLangFields($lang_prefix = NULL, $lang_id = NULL, $class_id = NULL, $edit_message_id = NULL){
		if(empty($lang_prefix) || empty($lang_id) || empty($class_id)){
			return NULL;
		}
		
		$nc_core = nc_Core::get_object();
		$db = $nc_core->db;
		$user = $nc_core->user;
		
		$html = '';
		$arrFields = self::getFieldsSystem();
		$where = "1";
		$where_add = NULL;
		
		if($lang_id == 1 && !empty($edit_message_id)){
			$where_add .= " AND a.Message_ID = '".(int)$edit_message_id."'";
		}
		if($lang_id != 1 && !empty($edit_message_id)){
			$where_add .= " AND a.Language_ID = '".(int)$lang_id."'  AND a.Language_Parent = '".(int)$edit_message_id."'";
		}
		
		if(!empty($where_add)){
			$where .= $where_add;
			$arrDataSystem = $db->get_row("SELECT ".implode(",",$arrFields)." FROM Message".(int)$class_id." as a
										   WHERE ".$where."
										   ORDER BY Message_ID DESC
										   LIMIT 1",ARRAY_A);
		}
		
		
		
		//Дата добавление или изменение объекта
		if(!empty($edit_message_id) && !empty($arrDataSystem)){
			$html .= '<div class="nc_admin_settings_info nc_seo_edit_info">
						<div class="nc_admin_settings_info_actions">
							<div>
								<span>Добавление:</span> '.date("m.d.Y H:i:s",strtotime($arrDataSystem["Created"])).' '.(!empty($arrDataSystem["User_ID"]) ? $user->get_by_id($arrDataSystem["User_ID"],"Email") : NULL).' '.(!empty($arrDataSystem["IP"]) ? "(".$arrDataSystem["IP"].")" : NULL).'
							</div>   <div>
								<span>Изменение:</span> '.date("m.d.Y H:i:s",strtotime($arrDataSystem["LastUpdated"])).' '.(!empty($arrDataSystem["LastUser_ID"]) ? $user->get_by_id($arrDataSystem["LastUser_ID"],"Email") : NULL).' '.(!empty($arrDataSystem["LastIP"]) ? "(".$arrDataSystem["LastIP"].")" : NULL).'
							</div>
						</div>
					  </div>';
		}
		//Включение или выключение обьъекта
		$is_checked = true;
		if(empty($arrDataSystem["Checked"]) && !empty($arrDataSystem)){
			$is_checked = false;
		}
		$html .= '<div class="nc_admin_settings_info_checked">
					<input id="chk_'.$lang_prefix.'" name="f_Checked_'.$lang_prefix.'" type="checkbox" value="1" '.($is_checked ? 'checked="checked"' : NULL).' onchange="jQuery.modal.nc_modal_confirm=true;undefined">
					<label for="chk_'.$lang_prefix.'">включить</label>
				  </div>';
		//Приоритет
		$priority = 0;
		if(!empty($arrDataSystem)){
			$priority = $arrDataSystem["Priority"];
		}
		$html .= '<div class="nc_admin_settings_info_priority">
					<div>Приоритет:</div>
					<div><input name="f_Priority_'.$lang_prefix.'" type="text" size="3" maxlength="10" value="'.$priority.'" onchange="jQuery.modal.nc_modal_confirm=true;undefined"></div>
				  </div>';
		//Ключевое слово
		$keyword = NULL;
		if(!empty($arrDataSystem)){
			$keyword = $arrDataSystem["Keyword"];
		}
		$html .= '<div>
					<div>Ключевое слово:</div>
					<div><input type="text" name="f_Keyword_'.$lang_prefix.'" style="width: 100%" maxlength="255" value="'.$keyword.'" onchange="jQuery.modal.nc_modal_confirm=true;undefined"></div>
				  </div>';
		//Заголовок страницы (Title)
		$title = NULL;
		if(!empty($arrDataSystem)){
			$title = $arrDataSystem["ncTitle"];
		}
		$html .= '<div>
					<div>Заголовок страницы (Title):</div>
					<div><input type="text" name="f_ncTitle_'.$lang_prefix.'" style="width: 100%" value="'.$title.'" onchange="jQuery.modal.nc_modal_confirm=true;undefined"></div>
				  </div>';
		//Ключевые слова для поисковиков
		$keywords = NULL;
		if(!empty($arrDataSystem)){
			$keywords = $arrDataSystem["ncKeywords"];
		}
		$html .= '<div>
					<div>Ключевые слова для поисковиков:</div>
					<div><textarea name="f_ncKeywords_'.$lang_prefix.'" onchange="jQuery.modal.nc_modal_confirm=true;undefined">'.$keywords.'</textarea></div>
				  </div>';
		//Описание страницы для поисковиков
		$description = NULL;
		if(!empty($arrDataSystem)){
			$description = $arrDataSystem["ncDescription"];
		}
		$html .= '<div>
					<div>Описание страницы для поисковиков:</div>
					<div><textarea name="f_ncDescription_'.$lang_prefix.'" onchange="jQuery.modal.nc_modal_confirm=true;undefined">'.$description.'</textarea></div>
				  </div>';
		//Заголовок для социальных сетей
		$title_smo = NULL;
		if(!empty($arrDataSystem)){
			$title_smo = $arrDataSystem["ncSMO_Title"];
		}
		$html .= '<div>
					<div>Заголовок для социальных сетей:</div>
					<div><input type="text" name="f_ncSMO_Title_'.$lang_prefix.'" style="width: 100%" value="'.$title_smo.'" onchange="jQuery.modal.nc_modal_confirm=true;undefined"></div>
					<div class="nc-field-hint-line"> Станет заголовком статьи при размещении ссылки на страницу в фейсбуке или вконтакте</div>
				  </div>';
		//Описание для социальных сетей
		$description_smo = NULL;
		if(!empty($arrDataSystem)){
			$description_smo = $arrDataSystem["ncSMO_Description"];
		}
		$html .= '<div>
					<div>Описание для социальных сетей:</div>
					<div><input type="text" name="f_ncSMO_Description_'.$lang_prefix.'" style="width: 100%" value="'.$description_smo.'" onchange="jQuery.modal.nc_modal_confirm=true;undefined"></div>
					<div class="nc-field-hint-line"> Станет текстом статьи при размещении ссылки на страницу в фейсбуке или вконтакте</div>
				  </div>';
		//Изображение для социальных сетей
		$image_smo = NULL;
		if(!empty($arrDataSystem)){
			global $HTTP_FILES_PATH;
			$image_smo = $arrDataSystem["ncSMO_Image"];
			$arrFile = explode(":",$image_smo);
			$file_name = $arrFile[0];
			$file_path = $HTTP_FILES_PATH.$arrFile[3];
		}
		$html .= '<div>
					<div>Изображение для социальных сетей:</div>
					<div>
					  <div class="nc-upload nc-upload-applied">
						<div class="nc-upload-files">
						  '.(!empty($image_smo) ? '
							<div class="nc-upload-file" data-type="image/jpeg">
								<div class="nc-upload-file-preview nc-upload-file-drag-handle nc-upload-file-preview-image" style="display:block;max-width:100px;text-align:center;">
								  <img src="'.$file_path.'" style="max-width:100%;max-height:100%;">
								  <a class="nc-upload-file-name" href="'.$file_path.'?'.time().'" target="_blank">'.$file_name.'</a>
								</div>
								<div class="nc-upload-file-info">
								  <label for="delete_old_image_smo_'.$lang_prefix.'">Удалить старый файл?</label>
								  <input type="checkbox" id="delete_old_image_smo_'.$lang_prefix.'" name="delete_old_image_smo_'.$lang_prefix.'" />
								</div>
							</div>
						  ' : NULL).'
						</div>
						<input class="nc-upload-input" name="f_ncSMO_Image_'.$lang_prefix.'" size="50" type="file" accept="image/*" onchange="jQuery.modal.nc_modal_confirm=true;undefined">
					  </div>
					</div>
				  </div>';
		
		
		return $html;
	}
}