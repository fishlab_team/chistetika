<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

$type = $_GET['type'];

if($type === 'manual'){
	global $DOCUMENT_ROOT;
	
	$arrRowSettingsManual = $db->get_row("SELECT Message_ID,DownloadImages,NameTemplate,WordTemplate,DescTemplate FROM Message238
										  ORDER BY Message_ID DESC
										  LIMIT 1", ARRAY_A);

	$is_load_items = false;
	$file_char = nc_file_path(238, $arrRowSettingsManual['Message_ID'], 'FileCharacteristics');
	if(!empty($file_char)){
		$file_char = $DOCUMENT_ROOT.$file_char;
		$loadExcelItems = new loadExcelItems($file_char, $arrRowSettingsManual['DownloadImages']);
		$is_load_items = true;
	}
	
	
	$file_stock = nc_file_path(238, $arrRowSettingsManual['Message_ID'], 'FileNalichie');
	if(!empty($file_stock)){
		$file_stock = $DOCUMENT_ROOT.$file_stock;
		loadExcelItems::loadStockUnits($file_stock);
	}

	if($is_load_items || file_exists($file_stock)){
		$db->query("UPDATE Message238 SET Status = 'Импорт завершен', LastCheckpointTime = NOW(), FileCharacteristics = '', FileNalichie = ''  WHERE Message_ID = '".$arrRowSettingsManual['Message_ID']."'");
        unlink($file_stock);
	}
}else if($type === 'auto'){
	try{
		$loadExcelItems = new loadExcelItems();
		/*
		$arrLoadSteps = $loadExcelItems->getTextSteps();
		$arrLoadSteps = array_map(function($value){
			return '<li>'.$value.'</li>';
		}, $arrLoadSteps);
		echo '<h2>Импорит завершен:</h2>
			  <ul>
				'.implode('', $arrLoadSteps).'
			  </ul>';
		*/
	}catch(Exception $e){
		echo 'Выброшено исключение: '.$e->getMessage();
	}
}else if($type === 'images'){
	nc_image_generator::remove_generated_images();
	DownloadPhotosFromYaDisk::downloadAllPhotosFromYaDisk();
}
?>