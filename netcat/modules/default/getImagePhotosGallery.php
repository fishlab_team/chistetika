<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

$item_id = (int)$_GET['item_id'];
$collection_id = (int)$_GET['collection_id'];
$row_active_id = (int)$_GET['row_active_id'];

$html = '';
if(!empty($item_id) && !empty($collection_id)){
	$html = trim(s_list_class(sct::sub('persona_images'), sct::cc('persona_images'), "nc_ctpl=".sct::tpl('persona_images_collection')."&isNaked=1&item_id=".$item_id."&collection_id=".$collection_id."&is_modal=true&row_active_id=".$row_active_id));
	if(mb_stripos($html, '<!--active-->') !== false){
		$html .= trim(s_list_class(sct::sub('persona_images'), sct::cc('persona_images'), "nc_ctpl=".sct::tpl('persona_images_item')."&isNaked=1&item_id=".$item_id."&collection_id=".$collection_id."&is_modal=true&row_active_id=".$row_active_id));
	}else{
		$html = trim(s_list_class(sct::sub('persona_images'), sct::cc('persona_images'), "nc_ctpl=".sct::tpl('persona_images_item')."&isNaked=1&item_id=".$item_id."&collection_id=".$collection_id."&is_modal=true&row_active_id=".$row_active_id)).$html;
	}
}
?>
<div class="modal-gallery">

    <div class="modal-gallery-slider">
        <?=$html?>
    </div>
</div>