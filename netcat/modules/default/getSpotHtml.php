<?php
$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($INCLUDE_FOLDER."index.php");

$ids = $_POST['ids'];
$arrIdsTypeSpot = explode(',', $ids);
$arrIdsTypeSpot = array_map(function($value){
	return (int)$value;
}, $arrIdsTypeSpot);
$arrIdsTypeSpot = array_filter($arrIdsTypeSpot);

$active_ids = $_POST['active_ids'];
$arrActiveIds = explode(',', $active_ids);
$arrActiveIds = array_map(function($value){
	return (int)$value;
}, $arrActiveIds);
$arrActiveIds = array_filter($arrActiveIds);

if(!empty($arrIdsTypeSpot)){
	echo get_checkbox_list(223, 'Spot[%ID%]', $arrActiveIds, NULL, ' AND a.TypeSpot IN ('.implode(',', $arrIdsTypeSpot).')');
}

exit();
?>