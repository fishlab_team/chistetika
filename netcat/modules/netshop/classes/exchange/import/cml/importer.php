<?php

class nc_netshop_exchange_import_cml_importer {
    /* @var nc_netshop_exchange_import_cml $object */
    protected $object = null;

    public function __construct(nc_netshop_exchange_import_cml $object) {
        $this->object = $object;
    }

    public function import_catalog_file($file_name, $file_data) {
        // Папка обмена и путь до файла
        $this->object->folder->create();
        $folder = $this->object->folder->get_path();
        $file_path = $folder . '/' . $file_name;
        // Записываем данные в файл
        $file_handle = fopen($file_path, 'a');
        fwrite($file_handle, $file_data);
        fclose($file_handle);

        return array(
            'path' => $file_path,
            'size' => filesize($file_path)
        );
    }
}